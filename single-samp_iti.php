<?php
/*
This template shows a single Sample Itinerary
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

						<div id="main" class="eightcol first clearfix" role="main">

							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

								<header class="article-header">
									<h1 class="single-title"><?php the_title(); ?></h1>
								</header>

								<section class="entry-content clearfix">
									<?php
										the_content(); 

										$sampiti_data = json_decode( get_post_meta( get_the_ID(),
												'sampiti-data', true ));

										foreach($sampiti_data as $day_index => $places_in_day){
											echo '<div class="sampiti_day_container">';
											echo '<h3>Day '.($day_index + 1).'</h3>';
											foreach($places_in_day as $place_index => $place){
												$place_link = $excerpt_text = $loc = $thumb_url = NULL;
												$is_custompost = false;
												//If ID of the place=0, it is a custom post
												if($place->{'id'} > 0){
													$place_link = get_permalink( $place->{'id'} );
													$excerpt_text = apply_filters('the_excerpt', get_post_field('post_excerpt', $place->{'id'}));
													$loc = wp_get_post_terms( $place->{'id'}, 'Location', array("fields" => "all"));
													$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($place->{'id'}), 'medium' );
													$thumb_url = $thumb[0];
												}
												else {
													$is_custompost = true;
													$thumb_url = get_template_directory_uri().'/library/images/logo-gray-thumb.png';
												}

												//echo "<p>Place ".($place_index+1)." ".$place->{'label'}." [ID=".$place->{'id'}."]</p>";
											?> 
												<div class="sampiti_place">
													<div class="sampiti_media"
														onclick="location.href='<?php echo $place_link ?>';"
														style="background-image: url(<?php echo $thumb_url ?>)">
														&nbsp;
													</div>
													<div class="sampiti_content">
														<?php if($is_custompost) { ?>
														<p class="sampiti-place-title"><?php echo $place->{'label'}; ?></p>
														<?php } else { ?>
														<div class="alignright tag-loc-<?php echo $loc[0]->slug ?>"><?php echo $loc[0]->name ?></div>
														<p class="sampiti-place-title"><a href="<?php echo $place_link ?>"><?php echo $place->{'label'}; ?></a></p>
														<?php } ?>

														<?php echo $excerpt_text; ?>
													</div>
													<div class="clear"></div>
												</div>
											<?php

											}
											//Close the day <div>
											echo "</div>";
										}


										echo do_shortcode('[shareaholic app="share_buttons" id="6579957"]');
									?>
								</section>

								<footer class="article-footer">
									<p class="tags"><?php echo get_the_term_list( get_the_ID(), 'post_tag', '<span class="tags-title">' . __( 'Custom Tags:', 'bonestheme' ) . '</span> ', ', ' ) ?></p>

								</footer>

							</article>

							<?php endwhile; ?>

							<?php else : ?>

									<article id="post-not-found" class="hentry clearfix">
										<header class="article-header">
											<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
										</header>
										<section class="entry-content">
											<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
										</section>
										<footer class="article-footer">
												<p><?php _e( 'This is the error message in the single-samp_iti.php template.', 'bonestheme' ); ?></p>
										</footer>
									</article>

							<?php endif; ?>

						</div>

						<?php get_sidebar(); ?>

				</div>

			</div>

<script type="text/javascript">
    jQuery( document ).ready(function() {
    	
    });
</script>
<?php get_footer(); ?>
