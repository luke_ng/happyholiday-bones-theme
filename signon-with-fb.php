<?php
require_once( '../../../wp-load.php' ); 
require_once( '../../../fb-sdk-php/facebook.php' );
//use Facebook\FacebookSession;
//use Facebook\FacebookRequest;
//use Facebook\GraphUser;
//use Facebook\FacebookRequestException;

//Signs user onto wordpress using the user's FB token
/*
Input POST parameters:

*/

// Redirect to https login if forced to use SSL
if ( force_ssl_admin() && ! is_ssl() ) {
	if ( 0 === strpos($_SERVER['REQUEST_URI'], 'http') ) {
		wp_redirect( set_url_scheme( $_SERVER['REQUEST_URI'], 'https' ) );
		exit();
	} else {
		wp_redirect( 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
		exit();
	}
}

/**
 * Output the login page header.
 *
 * @param string $title    Optional. WordPress Log In Page title to display in <title/> element. Default 'Log In'.
 * @param string $message  Optional. Message to display in header. Default empty.
 * @param string $wp_error Optional. The error to pass. Default empty.
 * @param WP_Error $wp_error Optional. WordPress Error Object
 */
function login_header( $title = 'Log In', $message = '', $wp_error = '' ) {
	global $error, $interim_login, $action;

	// Don't index any of these forms
	add_action( 'login_head', 'wp_no_robots' );

	if ( wp_is_mobile() )
		add_action( 'login_head', 'wp_login_viewport_meta' );

	if ( empty($wp_error) )
		$wp_error = new WP_Error();

	// Shake it!
	$shake_error_codes = array( 'empty_password', 'empty_email', 'invalid_email', 'invalidcombo', 'empty_username', 'invalid_username', 'incorrect_password' );
	/**
	 * Filter the error codes array for shaking the login form.
	 *
	 * @since 3.0.0
	 *
	 * @param array $shake_error_codes Error codes that shake the login form.
	 */
	$shake_error_codes = apply_filters( 'shake_error_codes', $shake_error_codes );

	if ( $shake_error_codes && $wp_error->get_error_code() && in_array( $wp_error->get_error_code(), $shake_error_codes ) )
		add_action( 'login_head', 'wp_shake_js', 12 );

	?><!DOCTYPE html>
	<!--[if IE 8]>
		<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" <?php language_attributes(); ?>>
	<![endif]-->
	<!--[if !(IE 8) ]><!-->
		<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
	<!--<![endif]-->
	<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<title><?php bloginfo('name'); ?> &rsaquo; <?php echo $title; ?></title>
	<?php

	wp_admin_css( 'wp-admin', true );
	wp_admin_css( 'colors-fresh', true );
	wp_admin_css( 'ie', true );

	// Remove all stored post data on logging out.
	// This could be added by add_action('login_head'...) like wp_shake_js()
	// but maybe better if it's not removable by plugins
	if ( 'loggedout' == $wp_error->get_error_code() ) {
		?>
		<script>if("sessionStorage" in window){try{for(var key in sessionStorage){if(key.indexOf("wp-autosave-")!=-1){sessionStorage.removeItem(key)}}}catch(e){}};</script>
		<?php
	}

	/**
	 * Enqueue scripts and styles for the login page.
	 *
	 * @since 3.1.0
	 */
	do_action( 'login_enqueue_scripts' );
	/**
	 * Fires in the login page header after scripts are enqueued.
	 *
	 * @since 2.1.0
	 */
	do_action( 'login_head' );

	if ( is_multisite() ) {
		$login_header_url   = network_home_url();
		$login_header_title = get_current_site()->site_name;
	} else {
		$login_header_url   = __( 'http://wordpress.org/' );
		$login_header_title = __( 'Powered by WordPress' );
	}

	/**
	 * Filter link URL of the header logo above login form.
	 *
	 * @since 2.1.0
	 *
	 * @param string $login_header_url Login header logo URL.
	 */
	$login_header_url = apply_filters( 'login_headerurl', $login_header_url );
	/**
	 * Filter the title attribute of the header logo above login form.
	 *
	 * @since 2.1.0
	 *
	 * @param string $login_header_title Login header logo title attribute.
	 */
	$login_header_title = apply_filters( 'login_headertitle', $login_header_title );

	$classes = array( 'login-action-' . $action, 'wp-core-ui' );
	if ( wp_is_mobile() )
		$classes[] = 'mobile';
	if ( is_rtl() )
		$classes[] = 'rtl';
	if ( $interim_login ) {
		$classes[] = 'interim-login';
		?>
		<style type="text/css">html{background-color: transparent;}</style>
		<?php

		if ( 'success' ===  $interim_login )
			$classes[] = 'interim-login-success';
	}

	/**
	 * Filter the login page body classes.
	 *
	 * @since 3.5.0
	 *
	 * @param array  $classes An array of body classes.
	 * @param string $action  The action that brought the visitor to the login page.
	 */
	$classes = apply_filters( 'login_body_class', $classes, $action );

	?>
	</head>
	<body class="login <?php echo esc_attr( implode( ' ', $classes ) ); ?>">
	<div id="login">
		<h1><a href="<?php echo esc_url( $login_header_url ); ?>" title="<?php echo esc_attr( $login_header_title ); ?>"><?php bloginfo( 'name' ); ?></a></h1>
	<?php

	unset( $login_header_url, $login_header_title );

	/**
	 * Filter the message to display above the login form.
	 *
	 * @since 2.1.0
	 *
	 * @param string $message Login message text.
	 */
	//$message = apply_filters( 'login_message', $message );
	if ( !empty( $message ) )
		echo $message . "\n";

	// In case a plugin uses $error rather than the $wp_errors object
	if ( !empty( $error ) ) {
		$wp_error->add('error', $error);
		unset($error);
	}

	if ( $wp_error->get_error_code() ) {
		$errors = '';
		$messages = '';
		foreach ( $wp_error->get_error_codes() as $code ) {
			$severity = $wp_error->get_error_data($code);
			foreach ( $wp_error->get_error_messages($code) as $error ) {
				if ( 'message' == $severity )
					$messages .= '	' . $error . "<br />\n";
				else
					$errors .= '	' . $error . "<br />\n";
			}
		}
		if ( ! empty( $errors ) ) {
			/**
			 * Filter the error messages displayed above the login form.
			 *
			 * @since 2.1.0
			 *
			 * @param string $errors Login error message.
			 */
			echo '<div id="login_error">' . apply_filters( 'login_errors', $errors ) . "</div>\n";
		}
		if ( ! empty( $messages ) ) {
			/**
			 * Filter instructional messages displayed above the login form.
			 *
			 * @since 2.5.0
			 *
			 * @param string $messages Login messages.
			 */
			echo '<p class="message">' . apply_filters( 'login_messages', $messages ) . "</p>\n";
		}
	}
} // End of login_header()


/**
 * Outputs the footer for the login page.
 *
 * @param string $input_id Which input to auto-focus
 */
function login_footer($input_id = '') {
	global $interim_login;

	// Don't allow interim logins to navigate away from the page.
	if ( ! $interim_login ): ?>
	<p id="backtoblog"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php esc_attr_e( 'Are you lost?' ); ?>"><?php printf( __( '&larr; Back to %s' ), get_bloginfo( 'title', 'display' ) ); ?></a></p>
	<?php endif; ?>

	</div>

	<?php if ( !empty($input_id) ) : ?>
	<script type="text/javascript">
	try{document.getElementById('<?php echo $input_id; ?>').focus();}catch(e){}
	if(typeof wpOnload=='function')wpOnload();
	</script>
	<?php endif; ?>

	<?php
	/**
	 * Fires in the login page footer.
	 *
	 * @since 3.1.0
	 */
	do_action( 'login_footer' ); ?>
	<div class="clear"></div>
	</body>
	</html>
	<?php
}


function wp_shake_js() {
	if ( wp_is_mobile() )
		return;
?>
<script type="text/javascript">
addLoadEvent = function(func){if(typeof jQuery!="undefined")jQuery(document).ready(func);else if(typeof wpOnload!='function'){wpOnload=func;}else{var oldonload=wpOnload;wpOnload=function(){oldonload();func();}}};
function s(id,pos){g(id).left=pos+'px';}
function g(id){return document.getElementById(id).style;}
function shake(id,a,d){c=a.shift();s(id,c);if(a.length>0){setTimeout(function(){shake(id,a,d);},d);}else{try{g(id).position='static';wp_attempt_focus();}catch(e){}}}
addLoadEvent(function(){ var p=new Array(15,30,15,0,-15,-30,-15,0);p=p.concat(p.concat(p));var i=document.forms[0].id;g(i).position='relative';shake(i,p,20);});
</script>
<?php
}

function wp_login_viewport_meta() {
	?>
	<meta name="viewport" content="width=device-width" />
	<?php
}

/**
 * Handles registering a new user using facebook account.
 *
 * @param string $user_login User's username for logging in
 * @param string $user_email User's email address to send password and add
 * @return int|WP_Error Either user's ID or error on failure.
 */
function hhp_register_new_user_fb( $first_name, $last_name, $user_email, $b_day, $b_month, $b_year ) {

	$errors = new WP_Error();
	
	$user_login = 'usr_' . date("YmdGis") . rand(1,100);
	$sanitized_user_login = sanitize_user( $user_login );
	$user_email = apply_filters( 'user_registration_email', $user_email );

	// Check the username
	while ( username_exists( $sanitized_user_login ) ) {
		$user_login .= rand(1,100);
		$sanitized_user_login = sanitize_user( $user_login );
	}

	// Check the e-mail address
	if ( $user_email == '' ) {
		$errors->add( 'empty_email', __( '<strong>ERROR</strong>: Please type your e-mail address.' ) );
	} elseif ( ! is_email( $user_email ) ) {
		$errors->add( 'invalid_email', __( '<strong>ERROR</strong>: The email address isn&#8217;t correct.' ) );
		$user_email = '';
	} elseif ( email_exists( $user_email ) ) {
		//User already has normal account, so just update the facebook id to this user
	}

	// Check the name
	if ( $first_name == '' || $last_name == '') {
		$errors->add( 'empty_name', __( '<strong>ERROR</strong>: Please type your name.' ) );
	}

	$errors = apply_filters( 'registration_errors', $errors, $sanitized_user_login, $user_email );

	if ( $errors->get_error_code() )
		return $errors;

	$user_pass = wp_generate_password( 12, false );
	$user_id = wp_create_user( $sanitized_user_login, $user_pass, $user_email );
	if ( ! $user_id || is_wp_error( $user_id ) ) {
		$errors->add( 'registerfail', sprintf( __( '<strong>ERROR</strong>: Couldn&#8217;t register you&hellip; please contact the <a href="mailto:%s">webmaster</a> !' ), get_option( 'admin_email' ) ) );
		return $errors;
	}

	//Add the user's name to the database
	$user_name = $first_name . ' ' . $last_name;
	$url_safe_name = str_replace(' ', '-', $user_name);
	$user_id = wp_update_user( 
		array(  'ID' => $user_id,
				'first_name' => $first_name,
				'last_name' => $last_name, 
				'display_name' => $user_name,
				'user_nicename' => $url_safe_name,
				'nickname' => $url_safe_name ) 
	);

	//Add the user's date of birth
	update_user_meta( $user_id, 'user_birthday', $b_day );
	update_user_meta( $user_id, 'user_birthmonth', $b_month ); 
	update_user_meta( $user_id, 'user_birthyear', $b_year ); 

	//TODO use some function to send a welcome email
	
	return $user_id; //returns the ID
}

//Returns (integer/boolean) 
//Primary key id for success. False for failure.
function hhp_add_fb_meta($user_id, $fb_id){
	$meta_key = 'fb_id';
	$unique = true;

	return add_user_meta( $user_id, $meta_key, $fb_id, $unique ); 
}

// Main //
//
// Get the fb ID first.
//
// Using this id, map to the correct wordpress user and set his auth cookie
//
// If user is not mapped, redirect to registration

$action = isset($_REQUEST['action']) ? $_REQUEST['action'] : 'verify';
$errors = new WP_Error();

if(isset($_GET['checkdob'])){
	$errors->add('checkdob', "Please ensure your birth date is in MM/DD/YYYY format!");
	$action = 'verify';
}
else if(isset($_GET['checkfields'])){
	$errors->add('checkfields', "All fields are compulsory, please do not leave any blanks!");
	$action = 'verify';	
}


$config = array(
  'appId' => '573561169383977',
  'secret' => '70c0564873a36b857e20ccac344f5faa',
  'fileUpload' => false, // optional
  'allowSignedRequest' => false, // optional, but should be set to false for non-canvas apps
);

$facebook = new Facebook($config);
$fb_id = $facebook->getUser(); //This is the unique ID of the facebook user

if($fb_id) {
    //echo "<p>fb_id: " . $fb_id . "</p>";
    //echo "<p>Name: " . $user_profile['name'] . "</p>";
    //echo "<p>Email: " . $user_profile['email'] . "</p>";
    
    // We have a user ID, so probably a logged in user.
    // If not, we'll get an exception, which we handle below.
    try {

    	$user_profile = $facebook->api('/me','GET');
	}
	catch(FacebookApiException $e) {
    	// If the user is logged out, you can have a 
    	// user ID even though the access token is invalid.
    	// In this case, we'll get an exception, so we'll
    	// just ask the user to login again here.
    	$action = 'fb_error';
  	}
}
else {
	$action = 'retry_signon';
}


/*
FacebookSession::setDefaultApplication('573561169383977','70c0564873a36b857e20ccac344f5faa');

$helper = new FacebookJavaScriptLoginHelper();
try {
    $session = $helper->getSession();
    $user_profile = (new FacebookRequest($session, 'GET', '/me'));
} catch(FacebookRequestException $ex) {
  	// When Facebook returns an error
	$action = 'fb_error';
} catch(\Exception $ex) {
    // When validation fails or other local issues
    $action = 'retry_signon';
}
if ($session) {
	// Logged in
	// Get the FB id of the user

}
*/


switch ($action) {

case 'retry_signon':
	//redirect to wp-login again
	//echo '<p>retry_signon</p>';
	wp_safe_redirect(esc_url(site_url('wp-login.php?retry_signon=fb')));
break;


case 'fb_error':
	login_header('Facebook connect error', '<p class="form-title">We had some trouble connecting to Facebook</p>', $errors);
	$login_url = $facebook->getLoginUrl(); 
	echo '<p>Please <a href="' . $login_url . '">try to login</a> again.</p>';
	error_log($e->getType());
	error_log($e->getMessage());
	login_footer();
break;


case 'register':
	$first_name = $_POST['first_name'];
	$last_name = $_POST['last_name'];
	$user_email = $_POST['user_email'];
	$user_dob = $_POST['user_dob']; //MM/DD/YYYY 

	$dob_info = explode('/', $user_dob);
	if(count($dob_info) != 3 || $dob_info[0] > 12 || $dob_info[1] > 31 || $dob_info[2] < 1900) {
		wp_safe_redirect(esc_url( content_url('themes/hhp-bones/signon-with-fb.php?checkdob=format')));
		exit();
	}

	//$new_user_id = hhp_register_new_user_fb($user_name, $user_email);
	$new_user_id = hhp_register_new_user_fb( $first_name, $last_name, $user_email, 
		$dob_info[1], $dob_info[0], $dob_info[2] );

	if(is_wp_error($new_user_id)){
		wp_safe_redirect(esc_url( content_url('themes/hhp-bones/signon-with-fb.php?checkfields=1')));
		exit();
	}

	//Add the facebook ID as metadata to the user account
	if(!hhp_add_fb_meta($new_user_id, $fb_id)){
		//Handle link failure
		login_header('Register new account', '<p class="form-title">Facebook account link failed!</p>', $errors);
	}
	else {
		login_header('Register new account', '<p class="form-title">Fantastic! Your new account has been registered!</p>', $errors);
		?>
		<form id="fb_login_form">
			<a href="/wp-content/themes/hhp-bones/signon-with-fb.php">
	    	<div id="btn_login_with_fb"><span class="icon-login-fb"></span>Log In with Facebook</div>
	    	</a>
		</form>
		<?php
	}

	login_footer();
break;

case 'link-fb':
	$user_profile = $facebook->api('/me','GET');
	$existing_user = get_user_by('email', $user_profile['email']);
	if (!hhp_add_fb_meta($existing_user->ID, $fb_id)){
		//Handle link failure
		login_header('Register new account', '<p class="form-title">Facebook account link failed!</p>', $errors);
		//echo '<p>' . var_dump($existing_user) . '</p>';
	}
	else {
		login_header('Register new account', '<p class="form-title">Hooray! Your Facebook account has been linked!</p>', $errors);
		?>
		<form id="fb_login_form">
			<a href="/wp-content/themes/hhp-bones/signon-with-fb.php">
	    	<div id="btn_login_with_fb"><span class="icon-login-fb"></span>Log In with Facebook</div>
	    	</a>
		</form>
		<?php
	}
	login_footer();
break;

case 'verify':
default:

	$user_profile = $facebook->api('/me','GET');

    $search_args = array(
		'meta_key'     => 'fb_id',
		'meta_value'   => $fb_id,
	);
    $user_query = new WP_User_Query( $search_args );
    // User Loop
	if ( ! empty( $user_query->results ) ) {
		$user = $user_query->results[0];
		echo '<p>Found ' . $user->display_name . '</p>';
		wp_set_auth_cookie($user->ID);
		$redirect_to = site_url();
		wp_safe_redirect($redirect_to);
	} else {
		login_header('Register new account', '<p class="form-title">Account Registration</p>', $errors);	
		//If this FB id not found then get user to register
		$existing_user = get_user_by( 'email', $user_profile['email'] );
		//if ( email_exists($user_profile['email']) ){
		if($existing_user){
			//Notify the user that we are going to link his account

?>			<form name="registerform" id="registerform" action="<?php echo get_template_directory_uri(); ?>/signon-with-fb.php?action=link-fb" method="post">
				<p>We found your account!</p>
				<p>Username: <?php echo $existing_user->user_login ?></p>
				<p>Email: <?php echo $existing_user->user_email ?></p>
				<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Link Facebook Account" /></p>
			</form>
<?php
		}
		else { //Show the registration form
?>
			<form name="registerform" id="registerform" action="<?php echo get_template_directory_uri(); ?>/signon-with-fb.php?action=register" method="post">
				<p id="reg_passmail">Verify that your details are correct, and continue with account creation</p>
				<p>
					<label for="first_name">First name<br />
					<input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr(wp_unslash($user_profile['first_name'])); ?>" size="20" /></label>
				</p>
				<p>
					<label for="last_name">Last name<br />
					<input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr(wp_unslash($user_profile['last_name'])); ?>" size="20" /></label>
				</p>
				<p>
					<label for="user_email"><?php _e('E-mail') ?><br />
					<input type="text" name="user_email" id="user_email" class="input" value="<?php echo esc_attr(wp_unslash($user_profile['email'])); ?>" size="25" /></label>
				</p>
				<p>
					<label for="user_dob">Birthdate (MM/DD/YYYY)<br />
					<input type="text" name="user_dob" id="user_dob" class="input" value="<?php echo $user_profile['birthday']; ?>" size="20" /></label>
				</p>
				<br class="clear" />
				<input type="hidden" name="redirect_to" value="<?php echo esc_attr( $redirect_to ); ?>" />
				<p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="<?php esc_attr_e('Register'); ?>" /></p>
			</form>
<?php
		}

		login_footer();
	} //End else

} //End switch



//$username = "luke";
//$user = get_user_by('login', $username );
//wp_set_auth_cookie($user->ID); 

//$redirect_to = admin_url('profile.php');
//wp_safe_redirect($redirect_to);

?>

</body>