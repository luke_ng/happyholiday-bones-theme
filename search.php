<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div class="ninecol first clearfix" role="main">
						<div class="first fourcol">
							<div class="search-taiwan clearfix">
								<p>Search Taiwan</p>
								<img class="alignright" src="<?php echo get_template_directory_uri(); ?>/library/images/search-taiwan-icon.png" />
							</div>
					        <div class="boxed">
					          <?php get_search_form(); ?>
						      <br class="clear"/>
						    </div>
						</div>
						
						<div class="last eightcol boxed results-box">
							
							<div id="search-results">
								<div id="search-meta">
									<p>Search results for
									<?php 
										$sq = get_search_query();
										if($sq == " "){
											echo ' all places in ';
										}
										else {
											echo $sq . ' in ';
										}
										$location = $_GET['Location'];
										if(!empty($location)){
											$loc_obj = get_term_by('slug', $location, 'Location');
											echo $loc_obj->name;
										}
										else {
											echo 'all of Taiwan';

										}
									?></p>
								</div>
								<?php bones_page_navi(); ?>
								<?php if (have_posts()) : while (have_posts()) : the_post(); 
								$loc = wp_get_post_terms( $post->ID, 'Location', array("fields" => "all")); ?>
								<div class="boxed post-card-horizontal">
									<div class="alignright tag-loc-<?php echo $loc[0]->slug ?>"><?php echo $loc[0]->name ?></div>
							        <p class="search-post-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark" ><?php the_title(); ?></a></p>
									<div class="clear"></div>
									<div class="sevencol first clearfix">
										<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
		          						<?php $hhp_attr = array( 'class'	=> "fourcol last search-img" );
								      	  	echo get_the_post_thumbnail( $place->ID, 'featured-thumbnail', $hhp_attr );
										?></a>
							      	</div>
							      	<p><?php the_excerpt(); ?></p>
							      	
							      	<!--<img class="fourcol last" src="http://happyholidayplanner.com/wp-content/uploads/2014/02/Happy-Holiday-Taipei-元定食-Yuan-Ding-Shi-220x143.jpg" class="attachment-featured-thumbnail wp-post-image" alt="Happy Holiday Taipei  元定食 (Yuan Ding Shi)" />-->
							    	<div class="clear"><br></div>
							    	<?php the_terms($post->ID, 'Place-categories', '<div class="tag-category">', 
							          	'</div><div class="tag-category">', '</div>' ); ?>

									<!-- Show this only if its an admin user -->
	                                <?php if (is_user_logged_in() && current_user_can('publish_posts')):
	                                    //Call function from itineraries plugin
	                                    $poiChecked = check_if_current_article_is_already_in_poi(get_the_ID()); ?>
	                                    <a class="button alignright" onclick="add_to_itinerary(<?php echo get_the_ID() ?>, this)">
	                                    	<?php echo $poiChecked ? "Remove from Itinerary":"Add to Itinerary" ?>
	                                    </a>
	                                <?php endif ?>
                                     
							    </div>
						        <?php endwhile; ?>
							        <?php if (function_exists('bones_page_navi')) { ?>
											<?php bones_page_navi(); ?>
									<?php } else { ?>
											<nav class="wp-prev-next">
													<ul class="clearfix">
														<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
														<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
													</ul>
											</nav>
									<?php } ?>

								<?php else : ?>
									<p class="search-fail"><?php _e( 'Sorry, No Results.', 'bonestheme' ); ?></p>
								<?php endif; ?>
							</div> 
						</div>

					</div>

					<?php get_sidebar(); ?>

				</div>

			</div>


<script type='text/javascript'>
	//Another copy of this code in page-destinations
	jQuery( document ).ready(function() {

	  	jQuery( "#searchform" ).on('submit', function( event ) {
	  		show_loader();
			event.preventDefault();
			var s_params = jQuery( this ).serialize();
			jQuery.get( "<?php echo get_template_directory_uri(); ?>/search-pre.php", s_params )
			.done( function( data ) {
				//returns the processed parameters in 'data'
				window.location.href = '<?php echo home_url("/"); ?>' + data;
			});
			//execute_search(jQuery( this ).serialize());
		  	//jQuery("#search-results").empty().append( jQuery( this ).serialize() );
		});

		jQuery( "#sel_all" ).click(function(){
			jQuery('.search-cat').prop("checked", true);
		});

		jQuery( "#sel_none" ).click(function(){
			jQuery('.search-cat').prop("checked", false);
		});
		
		//jQuery( "#adv-search" ).click(function(){
		//	jQuery("#cat-filters").slideToggle();
		//});
		//jQuery( "#cat-filters" ).hide();


		//Scroll to search-results on scroll (Only for mobile)
		if (window.matchMedia('(max-width: 768px)').matches) {
			var target = jQuery('#search-results');
			jQuery('html,body').animate({
				scrollTop: target.offset().top
			}, 1000);
		}
	} );


	function show_loader() {
		jQuery("#search-results").empty()
		.append('<div class="loader"><img src="' + 
			'<?php echo get_template_directory_uri(); ?>/library/images/round-loader.gif" /><p>Searching</p></div>')
		.fadeIn('slow');
	}
</script>  
<?php get_footer(); ?>
