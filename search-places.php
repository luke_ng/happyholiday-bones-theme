<?php
require_once( '../../../wp-load.php' ); 
//Custom search function for places
/*
Input GET parameters:

place_name -> Specifies name of place to search
location -> Specifies the location
category [] -> There may be more than 1 category for each place


*/

echo "<p>Search places</p>";

$place_name = $_GET['place-name'];
$location = $_GET['location'];
$category = $_GET['category'];

//'before' wrapper
//echo '<div id="gpp-payload">';

/*
echo "<p>place-name: " . $place_name . "</p>";
echo "<p>location: " . $location . "</p>";
echo "<p>category: ";
foreach($category as $cat) { echo $cat . ", "; }
echo "</p>";
*/


//If there is no place name, then just return based on the
//other filters

if (!empty($location)) {
	$args = array(
		'post_type' => 'places',
		'post_status' => 'publish',
	);

	if ($location !== "all") {
		$args['Location']=$location;
	}

	if(!empty($category)){
		$cat_str = implode(",", $category);
		$args['Place-categories']=$cat_str;
	}

	//echo var_dump($args);

	$the_query = new WP_Query( $args ); ?>

	<?php if ( $the_query->have_posts() ) : ?>
	  <?php bones_page_navi(); ?>
	  <!-- the loop -->
	  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<h4><?php the_title(); ?></h4>
	  <?php endwhile; ?>
	  <!-- end of the loop -->
	  <?php 
	  $big = 999999999; // need an unlikely integer

		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $the_query->max_num_pages
		) );
	  ?>
	  <?php wp_reset_postdata(); ?>

	<?php else:  ?>
	  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif;
}
else {
	echo "Location parameter not found..";
}
	
		

//'after' wrapper
//echo '</div>';




?>