<?php
/*
Template Name: Itinerary Builder Print Layout 
*/
?>

<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
	<meta charset="utf-8">

    <?php // Google Chrome Frame for IE ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?php wp_title(''); ?></title>

    <?php // mobile meta (hooray!) ?>
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>

	<?php wp_head(); ?>
	<link rel="stylesheet" type="text/css" media="all" 
		href="<?php echo get_template_directory_uri(); ?>/library/css/print.css" />
</head>

<body <?php body_class(); ?>>
	<div id="print-content">
		<div id="cover-page" class="print-page">
			<p id="cover-iti-date">26 - 30th Dec 2014</p>
		</div>
		
		<div class="print-page">
			<div class="page-margin-div"></div>
			<div class="place-header-container rounded-box text-center">
				<h1>九份 – Jiǔ fèn (Jiu Fen)</h1>
			</div>
			<div class="print-feat-img">
				<img src="http://devgz.happyholidayplanner.com/wp-content/uploads/2013/09/jiu-fen.jpg" />
			</div>

			<div class="half-container half-c-left rounded-box gray-gradient box-padding">
				<p>Address:</p>
				<p>台灣新北市瑞芳區九份老街</p>
				<br>
				<p>Operating hours:</p>
				<p>9 am ~ 10 pm</p>
			</div>
			<div class="half-container half-c-right rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
					veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat. Duis aute irure dolor in reprehenderit in voluptate 
					velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat 
					cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id 
					est laborum.
				</p>
			</div>
		</div>
	</div>
</body>

</html>
