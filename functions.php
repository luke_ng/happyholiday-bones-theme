<?php
/*
Author: Eddie Machado
URL: htp://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, ect.
*/

/************* INCLUDE NEEDED FILES ***************/

/*
1. library/bones.php
	- head cleanup (remove rsd, uri links, junk css, ect)
	- enqueueing scripts & styles
	- theme support functions
	- custom menu output & fallbacks
	- related post function
	- page-navi function
	- removing <p> from around images
	- customizing the post excerpt
	- custom google+ integration
	- adding custom fields to user profiles
*/
require_once( 'library/bones.php' ); // if you remove this, bones will break
/*
2. library/custom-post-type.php
	- an example custom post type
	- example custom taxonomy (like categories)
	- example custom taxonomy (like tags)
*/
//require_once( 'library/custom-post-type.php' ); // you can disable this if you like
/*
3. library/admin.php
	- removing some default WordPress dashboard widgets
	- an example custom dashboard widget
	- adding custom login css
	- changing text in footer of admin
*/
// require_once( 'library/admin.php' ); // this comes turned off by default
/*
4. library/translation/translation.php
	- adding support for other languages
*/
// require_once( 'library/translation/translation.php' ); // this comes turned off by default

/*================= HHP Custom scripts registration ================*/
function hhp_register_scripts() {
	//Register scripts
	
	//Register styles
	wp_register_style( 'hhp_jquery_ui_css', get_stylesheet_directory_uri() . '/library/css/jquery-ui-1.10.4.custom.min.css');

}

add_action( 'wp_enqueue_scripts', 'hhp_register_scripts' );

function hhp_register_admin_scripts() {
	//Register scripts
	wp_register_script( 'hhp_custom_post_admin_js', get_stylesheet_directory_uri() . '/library/js/hhp_custom_post.js', array('jquery'));
	wp_register_script( 'hhp_post_admin_js', get_stylesheet_directory_uri() . '/library/js/hhp-admin-posts.js', array('jquery'));
	wp_register_script( 'hhp_samp_iti_admin_js', get_stylesheet_directory_uri() . '/library/js/hhp-admin-samp_iti.js', array('jquery'));
	wp_register_script( 'hhp_itibuilder_admin_js', get_stylesheet_directory_uri() . '/library/js/hhp-itibuilder-admin.js', array('jquery'));
	//Register styles
	wp_register_style( 'hhp_jquery_ui_css', get_stylesheet_directory_uri() . '/library/css/jquery-ui-1.10.4.custom.min.css');
    wp_register_style( 'hhp_custom_post_admin_css', get_stylesheet_directory_uri() . '/library/css/hhp_custom_post.css');
    wp_register_style( 'hhp_post_admin_css', get_stylesheet_directory_uri() . '/library/css/hhp_admin_post.css');
    wp_register_style( 'hhp_samp_iti_admin_css', get_stylesheet_directory_uri() . '/library/css/hhp_admin_samp_iti.css');
    wp_register_style( 'hhp_itibuilder_admin_css', get_stylesheet_directory_uri() . '/library/css/hhp_admin_itibuilder.css');
}

add_action( 'admin_enqueue_scripts', 'hhp_register_admin_scripts' );

/*================= HHP Custom functions ================*/
require_once( 'library/post-extra-meta.php' ); // for additional post meta data entry
require_once( 'library/post-view-count.php' ); // for managing post view counts
require_once( 'library/hhp-recentpost-widget.php' ); // for customizing recent posts widget
require_once( 'library/hhp-recentpromo-widget.php' ); // for customizing recent promos widget
require_once( 'library/places-post-type.php' ); //Defining the 'Places' post type and entry boxes for admin
//require_once( 'library/samp-iti-post-type.php' ); //Defining the 'Sample Itinerary' post type and entry boxes for admin
//require_once( 'library/custom-post-type.php');
require_once( 'library/hhp-user-login.php' ); //Custom login functions
require_once( 'library/itibuilder-admin.php' ); //Custom admin menu item for itinerary builder 

/*================= Custom shortcodes ================*/

//Hack to process shortcodes in excerpts
add_filter('the_excerpt', 'do_shortcode');

// [highlighted text="foo-value"]
function highlighted_func( $atts ) {
	extract( shortcode_atts( array(
		'text' => '-',
	), $atts ) );

	$wrapped = '<span class="dkorange-bg">' . $text . '</span>';
	return $wrapped;
}
add_shortcode( 'highlighted', 'highlighted_func' );


/*====== Hack to remove the empty space created by the admin bar  ===========*/

function remove_admin_bar_space () {
    remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
add_action( 'admin_bar_init', 'remove_admin_bar_space' );

/************* WP CRON Actions *************/

function hhp_rem_expired_promo () {
	global $wpdb;

	$target_date = time() - DAY_IN_SECONDS;

	$posts_to_delete = $wpdb->get_results(
		$wpdb->prepare(
			"SELECT post_id FROM $wpdb->postmeta WHERE meta_key = 'promo-to' AND meta_value < '%d'", $target_date), 
		ARRAY_A);

	foreach ( (array) $posts_to_delete as $post ) {
        $post_id = (int) $post['post_id'];
        if ( !$post_id )
            continue;
	
        //wp_trash_post($post_id);
        //Set the expired post to be under the category 'Expired Promo'
        $uncat = array(get_cat_ID('Expired Promo')); 
        wp_set_post_categories($post_id, $uncat, false);
    }
}
add_action('hhp_detect_expired_promo', 'hhp_rem_expired_promo' );



/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );

/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 300 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 100 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select 
the new images sizes you have just created from within the media manager 
when you add media to your content blocks. If you add more image sizes, 
duplicate one of the lines in the array and name it according to your 
new image size.
*/

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3>',
	));

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!

/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>
		<article id="comment-<?php comment_ID(); ?>" class="clearfix">
			<header class="comment-author vcard">
				<?php
				/*
					this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
					echo get_avatar($comment,$size='32',$default='<path_to_url>' );
				*/
				?>
				<?php // custom gravatar call ?>
				<?php
					// create variable
					$bgauthemail = get_comment_author_email();
				?>
				<img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=32" class="load-gravatar avatar avatar-48 photo" height="32" width="32" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
				<?php // end custom gravatar call ?>
				<?php printf(__( '<cite class="fn">%s</cite>', 'bonestheme' ), get_comment_author_link()) ?>
				<time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>
				<?php edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ?>
			</header>
			<?php if ($comment->comment_approved == '0') : ?>
				<div class="alert alert-info">
					<p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
				</div>
			<?php endif; ?>
			<section class="comment_content clearfix">
				<?php comment_text() ?>
			</section>
			<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
		</article>
	<?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/************* SEARCH FORM LAYOUT *****************/

// Default Search Form
function bones_wpsearch($form) {
	$form = '<form role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
	<label class="screen-reader-text" for="s">' . __( 'Search for:', 'bonestheme' ) . '</label>
	<input type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . esc_attr__( 'Search the Site...', 'bonestheme' ) . '" />
	<input type="submit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" />
	</form>';
	return $form;
} // don't remove this bracket!



// HHP Search Form
function hhp_wpsearch($form) {
	$terms_loc = get_terms("Location", 'orderby=name&hide_empty=1&order=ASC');
	$count = count($terms_loc);
	$citylist = "";
	$param_loc = $_GET['Location']; 
	$param_cat = $_GET['Place-categories'];
	$cat_array;
	if(!empty($param_cat)){
		$cat_array = explode("," , $param_cat);
	}


	if ( $count > 0 ){
	    foreach ( $terms_loc as $term ) {
	    	if(!empty($param_loc) && $term->slug === $param_loc){
	      		$citylist .= '<option value="' . $term->slug . '" selected>' . $term->name . '</option>';
	      	}
	      	else {
	      		$citylist .= '<option value="' . $term->slug . '">' . $term->name . '</option>';
	      	}
	    }
	}

	$terms_placecat = get_terms("Place-categories", 'orderby=name&hide_empty=0&order=ASC');
	$catlist = "";
	if(!empty($param_cat)){
		foreach ( $terms_placecat as $term ) {
			if(in_array($term->slug, $cat_array)){
		    	$catlist .= '<input class="search-cat" type="checkbox" name="category[]" value="' . $term->slug . '" checked><span>' . $term->name . '</span><br>';
		    }
		    else {
		    	$catlist .= '<input class="search-cat" type="checkbox" name="category[]" value="' . $term->slug . '"><span>' . $term->name . '</span><br>';	
		    }
		}
	}
	else {
		foreach ( $terms_placecat as $term ) {
			$catlist .= '<input class="search-cat" type="checkbox" name="category[]" value="' . $term->slug . '" checked><span>' . $term->name . '</span><br>';
		}
	}
	
	$form = '<form role="search" method="get" id="searchform" action="' . get_template_directory_uri() . '/search-pre.php" >
	<input type="hidden" value="places" name="post_type" id="post_type" />
	<div class="search-lefthalf">
		<input class="search-text" type="text" value="' . get_search_query() . '" name="s" id="s" placeholder="' . 'Place Name' . '" />
		<input type="submit" class="searchsubmit-small" value="' . esc_attr__( 'Search' ) .'" />
		<h4 class="location-header">City/Township</h4>
		<select class="loc-select" id="location" name="location"><option value="">All</option>' . $citylist . '</select>
	</div>
	<div class="search-righthalf">
		<div id="adv-search"><img src="' . get_template_directory_uri() . '/library/images/expand_btn.png' . '"/>
		    Advanced Search
		</div>
		<div id="cat-filters">
			<h4 class="placecat-header">Categories</h4><div class="placecat-box">
			<a id="sel_all">Select all</a> <a id="sel_none">Select none</a><br>' . $catlist . '</div>
		</div>
	</div>
		<div class="submit-wrapper"><input type="submit" class="searchsubmit" id="searchsubmit" value="' . esc_attr__( 'Search' ) .'" /></div>
	</form>';

	return $form;
} // don't remove this bracket!

/*
// advanced search functionality for multiple place categories
function advanced_search_query($query) {
 
    if($query->is_search()) {
         
        // multiple taxonomy search (comma separated)
        if (isset($_GET['category']) && is_array($_GET['category'])) {
        	$cat_str = "";
        	$category = $_GET['category'];
        	foreach($category as $cat) { $cat_str .= $cat . ","; }
            $query->set('Place-categories', $cat_str);
        }
     
        return $query;
    }
 
}

add_action('pre_get_posts', 'advanced_search_query');

*/


/**
 * This function will connect wp_mail to your authenticated
 * SMTP server. This improves reliability of wp_mail, and 
 * avoids many potential problems.
 *
 * Author:     Chad Butler
 * Author URI: http://butlerblog.com
 *
 * For more information and instructions, see:
 * http://b.utler.co/Y3
 */
add_action( 'phpmailer_init', 'send_smtp_email' );
function send_smtp_email( $phpmailer ) {

	// Define that we are sending with SMTP
	$phpmailer->isSMTP();

	// The hostname of the mail server
	$phpmailer->Host = "sg241.singhost.net";

	// Use SMTP authentication (true|false)
	$phpmailer->SMTPAuth = true;

	// SMTP port number - likely to be 25, 465 or 587
	$phpmailer->Port = "465";

	// Username to use for SMTP authentication
	$phpmailer->Username = "darius@happyholidayplanner.com";

	// Password to use for SMTP authentication
	$phpmailer->Password = "darius_Gu1zh0ng";

	// Encryption system to use - ssl or tls
	$phpmailer->SMTPSecure = "ssl";

	$phpmailer->From = "darius@happyholidayplanner.com";
	$phpmailer->FromName = "Darius";
}


/************* AJAX FUNCTIONS *****************/
require_once( 'itibuilder-commons.php' );

function get_swap_suggestions() {
	if(isset($_POST['lang_set'])){
		$lang_set = intval($_POST['lang_set']);
	}
	else {
		//Default is 1, (English only)
		$lang_set = 1;
	}

	if(isset($_POST['place_id']))
    {
    	if(isset($_POST['exclude_list'])){
    		$csv = rtrim($_POST['exclude_list'], ",");//trim trailing commas
			$id_arr = str_getcsv($csv);
			//We need the place ids to be the keys of the array
			$exclude_arr = array_flip($id_arr);
			$return_arr = get_nearby_places($_POST['place_id'], 3, $lang_set, $exclude_arr);
		}
		else {
    		$return_arr = get_nearby_places($_POST['place_id'], 3, $lang_set);
    	}
        //Return data to caller
        echo json_encode($return_arr);
        die();
    } // end if
}

add_action('wp_ajax_get_swap_suggestions', 'get_swap_suggestions');
add_action('wp_ajax_nopriv_get_swap_suggestions', 'get_swap_suggestions');//for users that are not logged in.


function get_popular_places() {

	$num_pop_places = 4; //set 4 as the default
	if(isset($_POST['num_pop_places'])){
		$num_pop_places = $_POST['num_pop_places'];
	}

	if(isset($_POST['place_list'])){
		$csv = rtrim($_POST['place_list'], ",");//trim trailing commas
		$places_arr = str_getcsv($csv);

		$args = array(
			'posts_per_page' => $num_pop_places,
			'post_type' => 'places',
			'post__in' => $places_arr,
			'meta_key' => 'hhp_post_views_count', 
			'orderby' => 'meta_value_num', 
			'order' => 'DESC',
			'post_status' => 'publish',
		);
		
		$show_places = get_posts( $args );

		if(!empty($show_places)){
			$return_arr = array();

			foreach($show_places as $place) {
				//for each place return the post_title, featured image, and url
				$image_full = wp_get_attachment_url(
					get_post_thumbnail_id($place->ID));

				preg_match('~\((.*?)\)~', $place->post_title, $output);
		        $place_title_eng = $output[1];

				$place_obj = array( "title" => $place_title_eng,
					//"image_thumb" => $image_thumb[0],
					"image_full" => $image_full,
					"permalink" => get_permalink($place->ID)
					);
				$return_arr[] = $place_obj;
			}

			//Return data to caller
        	echo json_encode($return_arr);
        	die();
		}
	}
}

add_action('wp_ajax_get_popular_places', 'get_popular_places');
add_action('wp_ajax_nopriv_get_popular_places', 'get_popular_places');//for users that are not logged in.

?>
