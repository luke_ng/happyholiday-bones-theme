<?php

//Search preprocessor to generate the search arguments that wordpress understand

//$search_url = $_GET['url'];
$post_type = $_GET['post_type'];
$place_name = $_GET['s'];
$location = $_GET['location'];
$category = $_GET['category'];

$req_url = '?post_type=' . $post_type; 

if(empty($place_name)){
	$place_name = ' ';
}

$req_url .= '&s=' . $place_name;

if(!empty($location)){
	$req_url .= '&Location=' . $location;
} 

if(!empty($category)){
	$cat_list = implode("," , $category);
	$req_url .= '&Place-categories=' . $cat_list;
}
	
echo $req_url;

//Tried but http_get don't work. Need a special package
//echo http_get($req_url);

?>