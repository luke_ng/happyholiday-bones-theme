<?php get_header(); ?>

<!-- Show the promo banners and site features -->
	
	<div id="site-banner" class="clearfix">
		<div id="landing-options">
			<p id="landing-title">How long do you intend to stay in Taiwan?</p>
			<form action='<?php echo esc_url( get_permalink( get_page_by_title( "Itinerary Builder" ) ) ); ?>' 
				method="POST">
				<input id="landing-submit" type="submit" value="Go!"/>
				<select name="num-days">
				<?php for($i=3;$i<21;$i++) {
					echo '<option value="'.$i.'" ';
					if($i == 5){ echo "selected"; }
					echo '>'.$i.' Days</option>';
				} ?>
				</select>
				<div class="clear"></div>

				<input type="hidden" name="prov-sel[]" value="9"/>
				<input type="hidden" name="prov-sel[]" value="24"/>
				<input type="hidden" name="prov-sel[]" value="56"/>
				<input type="hidden" name="arv-airport" value="60"/>
			</form>
		</div>
		<div id="banner-text-mobile">
			<p>New way to travel in <span style="white-space: nowrap; word-break: keep-all;">TAIWAN </span></p>
		</div>
	</div>

	<div id="landing-options-mobile">
		<form action='<?php echo esc_url( get_permalink( get_page_by_title( "Itinerary Builder" ) ) ); ?>' 
				method="POST">
			<span>I want to stay for&nbsp;</span>
			<select name="num-days" class="sel-width-8">
			<?php for($i=3;$i<21;$i++) {
				echo '<option value="'.$i.'" ';
				if($i == 5){ echo "selected"; }
				echo '>'.$i.' Days</option>';
			} ?>
			</select>
			<input id="landing-submit-mobile" type="submit" value="Go!"/>

			<input type="hidden" name="prov-sel[]" value="9"/>
			<input type="hidden" name="prov-sel[]" value="24"/>
			<input type="hidden" name="prov-sel[]" value="56"/>
			<input type="hidden" name="arv-airport" value="60"/>
		</form>
	</div>
	

	<!-- Third row: 3 features -->
    
    <div id="features" class="wrap clearfix">
	    <div class="first fourcol">
	    	<a href='<?php echo esc_url( get_permalink( get_page_by_title( "Itinerary Builder" ) ) ); ?>'>
	      	<div class="features-wrapper">
	      		<img src="<?php echo get_template_directory_uri(); ?>/library/images/KEY-IMG-A.jpg" />
       		</div></a>
	    </div>

	    <div class="fourcol">
	      <a href='<?php echo esc_url( get_permalink( get_page_by_title( "Talk To Us" ) ) ); ?>'>
	      	<div class="features-wrapper">
	      		<img src="<?php echo get_template_directory_uri(); ?>/library/images/KEY-IMG-B.jpg" />
       		</div></a>
	    </div>

	    <div class="last fourcol">
	      <a href='<?php echo esc_url( get_permalink( get_page_by_title( "Travel Resources" ) ) ); ?>'>
	      	<div class="features-wrapper">
	      		<img src="<?php echo get_template_directory_uri(); ?>/library/images/KEY-IMG-C.jpg" />
       		</div></a>
	    </div>
	</div>
	
		
	<!-- The Content -->
	<div id="content">

		<div id="inner-content" class="wrap clearfix">

				<div id="main" class="ninecol first clearfix" role="main">
					<div class="popular-box">
			          <div class="popular-box-h clearfix">
			            <div class="pop-header">Popular Destinations in</div>
			            
			            <div id="social-mobile" class="clearfix">
			              <div class="sm-mobile">
					          <a href="https://www.facebook.com/pages/Happy-Holiday-Planner/1375689759345123" target="_blank">
					          	<img src="<?php echo get_template_directory_uri(); ?>/library/images/fb-icon.png"/>
					          </a>
					      </div>
					      <div class="sm-mobile">
					          <a href="https://twitter.com/HappyHolidayTW" target="_blank">
					          	<img src="<?php echo get_template_directory_uri(); ?>/library/images/tw-icon.png"/>
					          </a>
					      </div>
			            </div>

			            <select id="pop-select" class="loc-select float-left">
			              	<?php 
								$terms = get_terms("Location", 'orderby=count&hide_empty=1&order=DESC');
								$count = count($terms);
								if ( $count > 0 ){
								    foreach ( $terms as $term ) {
								      echo "<option value='" . $term->slug . "''>" . $term->name . "</option>";
								    }
								}
							?>
			            </select>

			            <a href='<?php echo esc_url( get_permalink( get_page_by_title( "Destinations" ) ) ); ?>' class="button alignright">See more</a>
			            <br class="clear" />
			          </div>

			          <div class="hscroll-div"><div class="pop-results clearfix"></div></div>
			        </div>
				</div>

				
				<div id="features-mobile" class="wrap clearfix">
				    <div class="onecol">
				    	<a href='<?php echo esc_url( get_permalink( get_page_by_title( "Itinerary Builder" ) ) ); ?>' class="button">Plan Your Itinerary</a>
					</div>
					<div class="onecol">
				    	<a href='<?php echo esc_url( get_permalink( get_page_by_title( "Talk To Us" ) ) ); ?>' class="button">Get The Best Drivers</a>
				    </div>
					<div class="onecol">
						<a href='<?php echo esc_url( get_permalink( get_page_by_title( "Travel Resources" ) ) ); ?>' class="button">Free Gifts & Promotions</a>
					</div>
				</div>

				<?php get_sidebar(); ?>
		</div>

	</div>

	<script type="text/javascript">
	    jQuery( document ).ready(function() {

	      jQuery( "#pop-select" ).change(show_pop);
	      show_pop();

	    } );

	    function show_pop() {
	      jQuery(".pop-results").fadeOut('fast', function() {
	      	show_loader();
	      
		    var selection = jQuery("#pop-select").val();
		    jQuery.get( "<?php echo get_template_directory_uri(); ?>/get-popular-places.php", { num_posts: "6", location: selection } )
			.done(function( data ) {
			 	jQuery(".pop-results").fadeOut('fast', function() {
			 		jQuery(".pop-results").empty().append(data);
			 		var payload = jQuery("#gpp-payload").html();
			 		jQuery(".pop-results").empty().append(payload).fadeIn('slow');
			 	});
			});
	      });
	      
	    }

	    function show_loader() {
	    	jQuery(".pop-results").empty()
	    	.append('<div class="loader"><img src="' + 
	    		'<?php echo get_template_directory_uri(); ?>/library/images/rect-loader.gif" /><p>Loading</p></div>')
	    	.fadeIn('slow');
	    }
  	</script>
<?php get_footer(); ?>


<!-- 

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?> role="article">

						<header class="article-header">

							<h1 class="h2"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h1>
							<p class="byline vcard"><?php
								printf( __( 'Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time> by <span class="author">%3$s</span> <span class="amp">&</span> filed under %4$s.', 'bonestheme' ), get_the_time('Y-m-j'), get_the_time(get_option('date_format')), bones_get_the_author_posts_link(), get_the_category_list(', '));
							?></p>

						</header>

						<section class="entry-content clearfix">
							<?php the_content(); ?>
						</section>

						<footer class="article-footer">
							<p class="tags"><?php the_tags( '<span class="tags-title">' . __( 'Tags:', 'bonestheme' ) . '</span> ', ', ', '' ); ?></p>

						</footer>

						<?php // comments_template(); // uncomment if you want to use them ?>

					</article>

					<?php endwhile; ?>

							<?php if ( function_exists( 'bones_page_navi' ) ) { ?>
									<?php bones_page_navi(); ?>
							<?php } else { ?>
									<nav class="wp-prev-next">
											<ul class="clearfix">
												<li class="prev-link"><?php next_posts_link( __( '&laquo; Older Entries', 'bonestheme' )) ?></li>
												<li class="next-link"><?php previous_posts_link( __( 'Newer Entries &raquo;', 'bonestheme' )) ?></li>
											</ul>
									</nav>
							<?php } ?>

					<?php else : ?>

							<article id="post-not-found" class="hentry clearfix">
									<header class="article-header">
										<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
								</header>
									<section class="entry-content">
										<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
								</section>
								<footer class="article-footer">
										<p><?php _e( 'This is the error message in the index.php template.', 'bonestheme' ); ?></p>
								</footer>
							</article>

					<?php endif; ?>
-->
