<?php

// $exclude_places will be an array(), where indexes are the place ids
//  e.g. dump exclude_places: a[23]=1, a[533]=1, a[1234]=1 
//
// $title_mode determines how the place title is returned.
// $title_mode = 0 --> Title returned in full (English, Chinese, PinYin)
// $title_mode = 1 --> Title returned in English
//
function get_nearby_places($from_place_id, $get_num_places, $title_mode = 1, $exclude_places = 0, $branch_num = 1) {
	global $wpdb;
	$search_range = $get_num_places + 10;

	$place_list = $wpdb->get_results( "SELECT * FROM samp_iti_dist_table WHERE from_place_id="
		//.$from_place_id. " AND from_place_branch=".$branch_num." ORDER BY dist_meter ASC LIMIT 0,".$search_range);
		.$from_place_id. " ORDER BY dist_meter ASC LIMIT 0,".$search_range);

	//error_log("place ID[0] : ".$place_list[0]->to_place_id,0);
	//error_log("place ID[1] : ".$place_list[1]->to_place_id,0);
	//error_log("place ID[2] : ".$place_list[2]->to_place_id,0);

	$return_arr = array();
	$num_found = 0;
	$loc_terms = wp_get_post_terms( $from_place_id, 'Location', array("fields" => "names"));
	$curr_loc_name = $loc_terms[0];

	//echo('<p>get_nearby_places() place_list</p>');
	//print_r($place_list);
	foreach ($place_list as $place_row) {
		//First, check if the place is in $exclude places
		if(empty($exclude_places)){
			//echo('<p>exclude_places is empty</p>');
			$in_excluded = false;
		}
		else {
			$in_excluded = array_key_exists($place_row->to_place_id, $exclude_places);
			//echo('<p>from_place_id exists in exclude_places?</p>');
			//print_r($in_excluded);
		}

		if(!$in_excluded){
			//Next, check if the location is the same
			$loc = wp_get_post_terms( $place_row->to_place_id, 'Location', array("fields" => "all"));
			if($loc[0]->name === $curr_loc_name){
				//echo('<p>Location is same. Adding to return array</p>');
				//Query the place name and thumbnail using the id
				//$image_thumb = wp_get_attachment_image_src(
					//get_post_thumbnail_id($place_row->to_place_id));
				$image_full = wp_get_attachment_url(
					get_post_thumbnail_id($place_row->to_place_id));
				//Get the excerpt
				$thepost = get_post( $place_row->to_place_id );
				$the_excerpt = $thepost->post_excerpt;
			    //$the_excerpt = apply_filters( 'the_content', $the_excerpt );

				if($title_mode == 0){
					$place_title_eng = get_the_title($place_row->to_place_id);
				}
				else if($title_mode == 1){
					preg_match('~\((.*?)\)~', get_the_title($place_row->to_place_id), $output);
		        	$place_title_eng = $output[1];
				}

		        if(empty($place_title_eng)){
		        	$place_title_eng = get_the_title($place_row->to_place_id);
		        }

				$place_obj = array( "title" => $place_title_eng,
					//"image_thumb" => $image_thumb[0],
					"image_full" => $image_full,
					"permalink" => get_permalink($place_row->to_place_id),
					"excerpt" => $the_excerpt,
					"place_id" => $place_row->to_place_id,
					"prov_slug" => $loc[0]->slug,
					"prov_name" => $loc[0]->name
					);
				$return_arr[] = $place_obj;
				$num_found++;
			}
		}

		if($num_found >= $get_num_places){
			break;
		}
	}

	return $return_arr;
}



//Splits the full name into its chinese, hanyu, and english parts
//Example of place_name: '桃园国际机场 – Táo Yuán Guó Jì Jī Chǎng (Taoyuan International Airport)';
//
//Returns: an array containing the split components:
// e.g array[0] = 桃园国际机场
//     array[1] = Táo Yuán Guó Jì Jī Chǎng
//     array[2] = Taoyuan International Airport
function split_place_name($place_name){
	//Reference for ascii codes https://www.cs.tut.fi/~jkorpela/dashes.html
	$place_name = str_replace( "&#8211;", "-", $place_name );    # en dash 
    $place_name = str_replace( "&#8212;", "-", $place_name );    # em dash 
    
	$keywords = explode("-", $place_name);
	//print_r($keywords);

	$chinese = trim($keywords[0]);

	$keywords2 = explode('(', $keywords[1]);
	//print_r($keywords);

	$hanyu = trim($keywords2[0]);
	$english = trim($keywords2[1], " )");

	//echo $chinese.'\n'.$hanyu.'\n'.$english;
	$comp = array($chinese, $hanyu, $english);

	return $comp;
}

?>