<?php
/*
  This is the custom post type post template.
  If you edit the post type name, you've got
  to change the name of this template to
  reflect that name change.

  i.e. if your custom post type is called
  register_post_type( 'bookmarks',
  then your single template should be
  single-bookmarks.php

 */
?>

<?php get_header(); ?>

<div id="content">

    <div id="inner-content" class="wrap clearfix">
        
        <div style="text-align:center;">
            <a href="/itinerary-builder"><img style="width: 100%;" src="<?php echo wp_get_attachment_url( 3241 ); ?>" /></a>
        </div>
        <div id="main" class="ninecol first clearfix" role="main">

            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

                    <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article">

                        <div id="goto-placeinfo" class="goto-btn"><a href="#place_info">Place Info</a></div>
                        <div id="goto-top" class="goto-btn"><a href="#top"><span class="icon-goup"></span></a></div>

                        <div class="featured-img">
                            <?php the_post_thumbnail('featured'); ?>
                <!--<img src="http://happyholidayplanner.com/wp-content/uploads/2014/02/Happy-Holiday-Taipei-元定食-Yuan-Ding-Shi-700x400.jpg" class="attachment-featured wp-post-image" alt="Happy Holiday Taipei  元定食 (Yuan Ding Shi)" />-->
                        </div>

                        <header id="top" class="article-header">

                            <h1 class="single-title places-title"><?php the_title(); ?></h1>
                            <div class="byline vcard">
                                <?php
                                printf(__('Posted <time class="updated" datetime="%1$s" pubdate>%2$s</time>  ', 'bonestheme'), get_the_time('Y-m-j'), get_the_time(__('F jS, Y', 'bonestheme')));
                                if (is_user_logged_in() && current_user_can('publish_posts')) {
                                    edit_post_link(__('Edit', 'sampression'));
                                }
                                ?>

                                <!-- Show this only if its an admin user -->
                                <?php if (is_user_logged_in() && current_user_can('publish_posts')):
                                    //Call function from itineraries plugin
                                    $poiChecked = check_if_current_article_is_already_in_poi(get_the_ID()); ?>
                                    <a class="button alignright" onclick="add_to_itinerary(<?php echo get_the_ID() ?>, this)">
                                        <?php echo $poiChecked ? "Remove from Itinerary":"Add to Itinerary" ?>
                                    </a>
                                <?php endif ?>
                                
                            </div>

                        </header>

                        <section class="entry-content content-padding clearfix">
                            <!-- Custom text before the contents -->
                            <h2>
                                <?php
                                $place_cat = wp_get_post_terms(get_the_ID(), 'Place-categories', array("fields" => "all"));
                                $has_food = false;
                                $has_poi = false;

                                if (!empty($place_cat)) {
                                    foreach ($place_cat as $pcat) {
                                        //$cat_slug = $place_cat[0]->slug;
                                        if ($pcat->slug == "restaurants-eating-places") {
                                            $has_food = true;
                                        } else {
                                            $has_poi = true;
                                        }
                                    }
                                }

                                if ($has_food && $has_poi) {
                                    echo "Recommended food & places of interest:";
                                } else if ($has_food) {
                                    echo "Recommended food:";
                                } else if ($has_poi) {
                                    echo "Recommended places of interest:";
                                } else {
                                    echo "Our recommendation:";
                                }
                                ?>
                            </h2>
                            <?php
                            the_content();

                            $place_loc = wp_get_post_terms(get_the_ID(), 'Location', array("fields" => "all"));
                            $place_parent = get_term_by('id', $place_loc[0]->parent, 'Location');
                            $loc_str = $place_loc[0]->name;

                            $place_name = esc_html(get_post_meta(get_the_ID(), 'place-name', true));

                            if ($has_food && $has_poi) {
                                ?>		<h2 id="place_info">If you are in <?php echo $loc_str ?>, you must check out 
                                    these attractions and try the food in <?php echo $place_name ?></h2>
                                <?php
                            } else if ($has_food) {
                                ?>		<h2 id="place_info">If you are in <?php echo $loc_str ?>, you must try the food in <?php echo $place_name ?></h2>
                                    <?php
                                } else if ($has_poi) {
                                    ?>		<h2 id="place_info">If you are in <?php echo $loc_str ?>, you must check out these 
                                    attractions in <?php echo $place_name ?></h2>
                            <?php } ?>
                            
                            <!-- Show this only if its an admin user -->
                            <?php if (is_user_logged_in() && current_user_can('publish_posts')): ?>
                                <a class="button alignright" onclick="add_to_itinerary(<?php echo get_the_ID() ?>, this)">
                                    <?php echo $poiChecked ? "Remove from Itinerary":"Add to Itinerary" ?>
                                </a>
                            <?php endif ?>
                            <br>
                            <h4>
                                <?php
                                if (empty($place_name)) {
                                    $place_name = 'Place';
                                }
                                echo $place_name . ' Information:';
                                ?>
                            </h4>

                            <?php
                            $place_num_branch = intval(get_post_meta(get_the_ID(), 'place-num-branch', true));

                            if ($place_num_branch > 1) {
                                ?>
                                <!-- Show the Tabs UI only if more than 1 branch -->
                                <div id="hhp_branch_tabs">
                                    <ul id="hhp_tablist" >
                                        <?php for ($branch_num = 1; $branch_num <= $place_num_branch; $branch_num ++) { ?>
                                            <li><a href="#tabs-<?php echo $branch_num; ?>">Branch <?php echo $branch_num; ?></a></li>
                                        <?php } ?>
                                    </ul>

                                <?php } ?>


                                <?php for ($branch_num = 1; $branch_num <= $place_num_branch; $branch_num ++) { ?>
                                    <div id="tabs-<?php echo $branch_num; ?>">

                                        <h5>Address:</h5>
                                        <p>
                                            <?php
                                            printf('<a href="%s" target="_blank">%s</a>', get_post_meta(get_the_ID(), 'place-map-url-' . $branch_num, true), nl2br(get_post_meta(get_the_ID(), 'place-address-' . $branch_num, true)));

                                            $p_map_iframe = get_post_meta(get_the_ID(), 'place-map-iframe-' . $branch_num, true);
                                            if (!empty($p_map_iframe)) {
                                                printf('<br><div id="p_map_iframe%d" class="Flexible-container">%s</div>', $branch_num, $p_map_iframe);
                                            }
                                            ?>
                                        </p>


                                        <?php
                                        $place_website = esc_html(get_post_meta(get_the_ID(), 'place-webpage', true));
                                        if (!empty($place_website)) {
                                            printf('<h5>Website:</h5><p><a href="%1$s" target="_blank">%1$s</a></p>', $place_website);
                                        }


                                        $p_email = get_post_meta(get_the_ID(), 'place-email', true);
                                        $p_phone = get_post_meta(get_the_ID(), 'place-phone-' . $branch_num, true);
                                        $p_fax = get_post_meta(get_the_ID(), 'place-fax-' . $branch_num, true);

                                        if (!empty($p_email) || !empty($p_phone) || !empty($p_fax)) {

                                            printf('<h5>Contact:</h5><p>');

                                            if (!empty($p_email)) {
                                                printf('Email: <a href="mailto:%1$s" target="_top">%1$s</a><br>', $p_email);
                                            }

                                            if (!empty($p_phone)) {
                                                printf('Phone: %s<br>', nl2br($p_phone));
                                            }

                                            if (!empty($p_fax)) {
                                                printf('Fax: %s<br>', nl2br($p_fax));
                                            }

                                            printf('</p>');
                                        }


                                        $p_food_style = get_post_meta(get_the_ID(), 'place-food-style', true);
                                        if (!empty($p_food_style)) {
                                            printf('<h5>Food Style:</h5><p>%s</p>', $p_food_style);
                                        }


                                        $p_price_range = get_post_meta(get_the_ID(), 'place-price-range', true);
                                        if (!empty($p_price_range)) {
                                            printf('<h5>Price Range:</h5><p>%s</p>', $p_price_range);
                                        }


                                        $p_op_hours = get_post_meta(get_the_ID(), 'place-operating-hours-' . $branch_num, true);
                                        if (!empty($p_op_hours)) {
                                            printf('<h5>Operating Hours:</h5><p>%s</p>', nl2br($p_op_hours));
                                        }
                                        ?>

                                    </div>
                                <?php } ?>

                                <?php if ($place_num_branch > 1) { ?>
                                </div><!-- End hhp_branch_tabs -->
                            <?php } ?>



                        </section>
                        <footer class="article-footer">	
							
						</footer>

                    </article>

                <?php endwhile; ?>

            <?php else : ?>

                <article id="post-not-found" class="hentry clearfix">
                    <header class="article-header">
                        <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
                    </header>
                    <section class="entry-content">
                        <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
                    </section>
                    <footer class="article-footer">
                        <p><?php _e('This is the error message in the single-custom_type.php template.', 'bonestheme'); ?></p>
                        <!-- Show this only if its an admin user -->
                        <?php if (is_user_logged_in() && current_user_can('publish_posts')): ?>
                            <a class="button alignright" onclick="add_to_itinerary(<?php echo get_the_ID() ?>, this)">
                                <?php echo $poiChecked ? "Remove from Itinerary":"Add to Itinerary" ?>
                            </a>
                        <?php endif ?>
                    </footer>
                </article>

            <?php endif; ?>

        </div>

        <?php get_sidebar(); ?>

    </div>

</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

        jQuery("#hhp_branch_tabs").tabs({
            activate: function(event, ui) {
                var active_tab = jQuery("#hhp_branch_tabs").tabs("option", "active");
                active_tab = active_tab + 1;
                var frame = jQuery('#p_map_iframe' + active_tab).contents();
                jQuery('#p_map_iframe' + active_tab).empty().append(frame);

                //jQuery('#p_map_iframe'+active_tab).append('<p>Hahahaha</p>');
            }
        });

        jQuery('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = jQuery(this.hash);
                target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    jQuery('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });

        jQuery('#goto-top').hide();
        //Check to see if the window is top if not then display button
        jQuery(window).scroll(function() {
            if (jQuery(this).scrollTop() > 1600) {
                jQuery('#goto-top').fadeIn();
            } else {
                jQuery('#goto-top').fadeOut();
            }
        });


        position_goto_btns();
        jQuery(window).resize(function() {
            position_goto_btns();
        });

    });

    function position_goto_btns() {
        //Set the positioning of the "View place info" and "go to top" buttons
        //Get the offset and width of the 'entry-content' class
        var content_rightedge = jQuery('.entry-content').offset().left + jQuery('.entry-content').width();
        //window.alert("right edge: " + content_rightedge);
        var goto_pl_newleft = content_rightedge - 40;
        jQuery('#goto-placeinfo').css({"left": goto_pl_newleft});
        var goto_top_newleft = goto_pl_newleft + (jQuery('#goto-placeinfo').width() - jQuery('#goto-top').width());
        jQuery('#goto-top').css({"left": goto_top_newleft});
    }
</script>
<?php get_footer(); ?>