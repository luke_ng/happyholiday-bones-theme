<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

    <head>
        <meta charset="utf-8">

        <?php // Google Chrome Frame for IE ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

        <title><?php wp_title(''); ?></title>

        <?php // mobile meta (hooray!) ?>
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0"/>

        <?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
        <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
        <!--[if IE]>
                <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
        <![endif]-->
        <?php // or, set /favicon.ico for IE10 win ?>
        <meta name="msapplication-TileColor" content="#f01d4f">
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">



        <?php // wordpress head functions ?>
        <?php wp_head(); ?>
        <?php // end of wordpress head ?>

        <?php // drop Google Analytics Here ?>
        <?php // end analytics ?>

        <script type="text/javascript">
            //ipHone does not work well with onClick(), so have to
            //use on 'touchstart' instead

            jQuery(document).ready(function() {

                //activate only on mobile
                if (window.matchMedia('(max-width: 767px)').matches) {
                    jQuery('.icon-menu').on('touchstart', function(event) {
                        event.stopPropagation();
                        event.preventDefault();
                        //jQuery('header > nav').slideToggle();
                        jQuery('#nav-mobile').slideToggle();
                    });

                    jQuery('html').on('touchstart', function() {
                        //Hide the menu if visible
                        //jQuery('header > nav').slideUp();
                        jQuery('#nav-mobile').slideUp();
                        jQuery('#user-nav').slideUp();
                    });

                    jQuery('html').on('touchmove', function() {
                        jQuery('#nav-mobile').slideUp();
                        jQuery('#user-nav').slideUp();
                    });

                    jQuery('#nav-mobile').on('touchstart', function(event) {
                    //jQuery('header > nav').on('touchstart', function(event) {
                        event.stopPropagation();
                    });
                    jQuery('#user-nav').on('touchstart', function(event) {
                        event.stopPropagation();
                    });

                    jQuery('#loggedin_btn').on('touchstart', function(event) {
                        event.stopPropagation();
                        jQuery('#user-nav').slideToggle();
                    });

                    /*
                     jQuery(document).click(function(event) { 
                     if(jQuery(event.target).parents().index(jQuery('header > nav')) == -1) {
                     if(jQuery('header > nav').is(":visible")) {
                     jQuery('header > nav').hide()
                     }
                     }        
                     });
                     */
                }
                else {

                    jQuery('#loggedin_btn').click(function(event) {
                        event.stopPropagation();
                        jQuery('#user-nav').slideToggle();
                    });

                    jQuery('html').click(function(event) {
                        jQuery('#user-nav').slideUp();
                    });

                    jQuery('html').on('touchstart', function() {
                        jQuery('#user-nav').slideUp();
                    });
                }

            });

        </script>
    </head>

    <body <?php body_class(); ?>>

        <div id="top-bar" class="clearfix">
            <div id="hhp-text"><a href='<?php echo home_url(); ?>' >HHP</a></div>

            <div class="icon-menu">
                <span class="hide">Menu</span>
            </div>

            <?php if (is_user_logged_in()): ?>
                <?php
                $current_user = wp_get_current_user();
                $displayname = $current_user->first_name;
                if (empty($displayname)) {
                    $displayname = $current_user->user_login;
                }
                ?>
                <div id="loggedin_btn" class="top-bar-txtbtn"><span class="icon-user"></span><a> Hi <?php echo $displayname; ?>!</a></div>
            <?php else: ?>
                <?php $login_url = (is_home() ? "wp-login.php?redirect_to=/" : "wp-login.php?redirect_to=" . urlencode(get_permalink())); ?>
                <div class="top-bar-txtbtn"><a href='<?php echo home_url($login_url); ?>' >Login/Register</a></div>
            <?php endif ?>

            <!--<div class="top-bar-txtbtn"><a href="/get-freebies-taiwan-trip/" >
                    <span class="icon-freebie"></span> Freebies</a></div>-->
            <form role="search" method="get" id="searchform_top" action="<?php echo home_url('/') ?>" >
                <input type="hidden" value="places" name="post_type" id="post_type" />
                <span class="icon-search"></span><input type="text" value="<?php get_search_query() ?>" name="s" id="s" placeholder="Search places" />
            </form>

            <!-- Show this only if its an admin user -->
            <?php if (is_user_logged_in() && current_user_can('publish_posts')): ?>
            <div class="count-poi">
                <a href="/itineraries">
                    <div class="img-poi">
                        <img src="/wp-content/plugins/itineraries/img/poi.png" />
                    </div>
                    <div class="count"></div>
                </a>
            </div>
            <?php endif ?>
        </div>

        <div id="user-nav"><ul>
                <?php if (is_user_logged_in() && current_user_can('publish_posts')): ?>
                    <li><a href='<?php echo home_url("wp-admin/index.php"); ?>'><span class="icon-admin"></span>  Admin Dashboard</a></li>
                <?php endif ?>
                <li><a href='<?php echo home_url("wp-admin/profile.php"); ?>'><span class="icon-userprofile"></span>  User Profile</a></li>
                <li><a href="<?php echo wp_logout_url('/'); ?>"><span class="icon-userlogout"></span>  Log Out</a></li>
            </ul>
        </div>

        <div id="nav-mobile">
            <?php bones_main_nav(); ?>
        </div>

        <form role="search" method="get" id="searchform_top_m" action="<?php echo home_url('/') ?>" >
            <input type="hidden" value="places" name="post_type" id="post_type" />
            <span class="icon-search"></span><input type="text" value="<?php get_search_query() ?>" name="s" id="s" placeholder="Search places" />
        </form>

        <div id="container">

            <header class="header" role="banner">
                <nav role="navigation">
                    <div id="inner-header" class="wrap clearfix">
                        <!--<div class="sixcol last">-->
                        <?php bones_main_nav(); ?>
                    </div>
                </nav>
            </header>
