<?php
require_once( '../../../wp-load.php' ); 
//Get popular posts
/*
Input GET parameters:

num_posts -> Specifies number of posts to return
location -> 'Location' taxonomy filter
*/

//echo "<p>get popular posts</p>";

$num_posts = $_GET['num_posts'];
$location = $_GET['location'];

echo '<div id="gpp-payload">';

if (empty($num_posts)) {
	//default to 6
	//echo "<p>set to default 6 posts</p>";
	$num_posts = '6';
}


if (!empty($location)) {
	$args = array(
		'posts_per_page' => -1,
		'post_type' => 'places',
		'Location' => $location,
		'meta_key' => 'hhp_post_views_count', 
		'orderby' => 'meta_value_num', 
		'order' => 'DESC',
		'post_status' => 'publish',
	);

	/*
	$show_places = new WP_Query( $args );
	if ( $show_places->have_posts() ) :
		while ( $show_places->have_posts() ) : $show_places->the_post();
		$loc = wp_get_post_terms( get_the_ID(), 'Location', 
		array("fields" => "names"));
		echo "<p>" . $place->post_title . "[" . $loc[0] . "]</p>";
		echo "<p>" . $place->post_excerpt . "</p>";
		endwhile;
	else :
		echo "<p>Sorry no posts match the query</p>";
	endif;
	*/

	$show_places = get_posts( $args );

	if(!empty($show_places)){

		$picked_places = array();
		$spare_places = array();
		foreach($show_places as $place) {
			//$loc = wp_get_post_terms( $place->ID, 'Location', 
				//array("fields" => "names"));

			$place_cat = wp_get_post_terms( $place->ID, 'Place-categories', 
				array("fields" => "all"));
			$has_food = false;
			$has_poi = false;

			if(!empty($place_cat)){
				foreach($place_cat as $pcat){ 
					//$cat_slug = $place_cat[0]->slug;
					if($pcat->slug == "restaurants-eating-places"){
						$has_food = true;
		 			}
					else {
						$has_poi = true;
					}
				}						
			}

			if($has_poi){
				$picked_places[] = $place;
				if(count($picked_places) >= $num_posts){ break; }
			}
			else if (count($spare_places) < $num_posts) {
				$spare_places[] = $place;
			}
		}

		$sp_index = 0;
		while(count($picked_places) < $num_posts && $sp_index <  count($spare_places)){
			$picked_places[] = $spare_places[$sp_index];
			$sp_index++;
		}

		foreach($picked_places as $place){
?>
			<article id="post-<?php echo $place->ID; ?>" class="placecard-mini" role="article">
				<div class="header-wrapper">
				<h3 class="h3"><a href="<?php echo get_permalink( $place->ID ); ?>" title="<?php echo $place->post_title ?>" rel="bookmark" ><?php echo $place->post_title ?></a></h3>
                </div>
                <?php if ( has_post_thumbnail($place->ID) ) { ?>
	            <div class="featured-img">
	              <a href="<?php echo get_permalink( $place->ID ); ?>" title="<?php echo $place->post_title ?>" >
	              <?php echo get_the_post_thumbnail($place->ID, 'featured-thumbnail' ); ?>
	              <!--<img src="http://happyholidayplanner.com/wp-content/uploads/2014/02/Happy-Holiday-Taipei-元定食-Yuan-Ding-Shi-220x143.jpg" class="attachment-featured-thumbnail wp-post-image" />-->
	              </a>
	            </div>
	            <?php } ?>
			
				<div class="post-excerpt"><?php echo $place->post_excerpt ?></div>
			</article>
<?php
		}
	}
	else {
		echo "No matches found";
	}


}
else {
	echo "Location parameter not found..";
}


echo '</div>';

?>