<?php
/*
Template Name: Travel Resources Page Layout 
*/
?>

<?php get_header(); ?>


	<div id="content">
		<div style="text-align:center;">
            <a href="/itinerary-builder"><img style="width: 100%;" src="<?php echo wp_get_attachment_url( 3241 ); ?>" /></a>
        </div>
        
		<h1 class="page-title"><?php echo get_the_title(); ?></h1>

		<div id="inner-content" class="tr-wrap clearfix">
			
			<?php
				$exclude_catid = '-' . get_cat_ID('Blog') . ',-' . get_cat_ID('Uncategorized') .
                	',-' . get_cat_ID('Expired Promo');
				$args = array( 'posts_per_page' => -1, 
					'category' => $exclude_catid,
					'post_status'      => 'publish' );
				$promo_posts = get_posts( $args );
				if ( empty($promo_posts) ) : ?>
					<p class="search-fail"><?php _e( 'No posts found.', 'bonestheme' ); ?></p>
				<?php else : foreach ( $promo_posts as $post ) :
				  	setup_postdata( $post ); ?>
				  	<div class="tr-card">
				  		<div class="tr-card-img"><a href="<?php the_permalink(); ?>">
					  		<?php the_post_thumbnail('featured-thumbnail'); ?>
					  		</a>
					  	</div>
						<div class="tr-card-title"><h3 class="peeker"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3></div>
						<div class="tr-excerpt"><?php the_excerpt(); ?></div>
						<div class="tr-card-footer">
								<?php 
									$promo_from = get_post_meta( get_the_ID(), 'promo-from', true );
									$promo_to = get_post_meta( get_the_ID(), 'promo-to', true );
								    if(!empty($promo_from) && !empty($promo_to)){
								    	$dfromfull = date("j M Y", $promo_from);
								    	$dtofull = date("j M Y", $promo_to);
								    	echo '<div class="tr-promo-period"><p>Promo period:</p>';
								    	echo '<p>'.$dfromfull.' ~ '.$dtofull.'</p></div>';
								    }
								    else if (!empty($promo_from)){
								    	$dfrom = date("j M Y", $promo_from);
								    	echo '<div class="tr-promo-period"><p>Promo period:</p>';
								    	echo '<p>From '.$dfrom.'</p></div>';
								    }
								    else if (!empty($promo_to)){
								    	$dto = date("j M Y", $promo_to);
								    	echo '<div class="tr-promo-period"><p>Promo period:</p>';
								    	echo '<p>Ends '.$dto.'</p></div>';
								    }

								    $buynow_url = get_post_meta( get_the_ID(), 'promo-buynow-url', true);
								    if(!empty($buynow_url)){
								    	echo '<div class="tr-card-buynow"><a href="' .
								    		$buynow_url .'" class="orange-button">Buy Now</a></div>';
								    }
								?>
						</div>
					</div>
				<?php endforeach;
				endif; 
				wp_reset_postdata(); 
			?>

		</div>
	</div>

<script type="text/javascript">
    jQuery( document ).ready(function() {

    	apply_screen_settings();
    	align_pics();

    	jQuery( window ).resize(function() {
    		apply_screen_settings();
    		align_pics();
    	});

    	jQuery('.tr-card-title').hover(
		    function() {
		    	if(jQuery(this).children().height() > jQuery(this).height()){
		        	jQuery(this).animate({height:jQuery(this).children().height()});
		    	}
		    },
		    function() {
		    	if (window.matchMedia('(min-width: 768px)').matches) {
		        	jQuery(this).animate({height:35});
		    	}
		    	else {
		    		jQuery(this).animate({height:40});	
		    	}
		    }
		);
    	
    } );

    function apply_screen_settings() {
    	if (window.matchMedia('(min-width: 1030px)').matches) {
    		jQuery( ".tr-card" ).removeClass().addClass("tr-card one-fifth");
    		jQuery( ".tr-card:nth-child(5n+1)" ).addClass("first");
    		jQuery( ".tr-card:nth-child(5n)" ).addClass("last-tr");
    	}
    	else if (window.matchMedia('(min-width: 768px)').matches) {
    		jQuery( ".tr-card" ).removeClass().addClass("tr-card threecol");
    		jQuery( ".tr-card:nth-child(4n+1)" ).addClass("first");
    		jQuery( ".tr-card:nth-child(4n)" ).addClass("last-tr");
    	}
    	else if (window.matchMedia('(min-width: 481px)').matches) {
    		jQuery( ".tr-card" ).removeClass().addClass("tr-card halfwidth");
    		jQuery( ".tr-card:nth-child(2n+1)" ).addClass("first");
    		jQuery( ".tr-card:nth-child(2n)" ).addClass("last-tr");
    	}
    }

	//Resize and align pictures according to their ratios.
	//Similar to background-size:cover
	//Scale the background image to be as large as possible so that the 
	//background area is completely covered by the background image. 
	//Some parts of the background image may not be in view within 
	//the background positioning area
    function align_pics() {
    	jQuery(".tr-card-img img").each(function(){
    		// reset image (in case we're calling this a second time, for example on resize)
      		jQuery(this).css({'width': '', 'height': '', 'margin-left': '', 'margin-top': ''});

    		// dimensions of the parent
	      	var parentWidth = jQuery(".tr-card-img").width();
	      	var parentHeight = jQuery(".tr-card-img").height();
	 
	      	// dimensions of the image
	      	var imageWidth = this.width;
	      	var imageHeight = this.height;
	 
	      	// step 1 - calculate the percentage difference between image width and container width
	      	var diff = imageWidth / parentWidth;
	 
	      	// step 2 - if height divided by difference is smaller than container height, resize by height. otherwise resize by width
	      	if ((imageHeight / diff) < parentHeight) {
	       		jQuery(this).css({'width': 'auto', 'height': parentHeight});
	 
	       		// set image variables to new dimensions
	       		imageWidth = imageWidth / (imageHeight / parentHeight);
	       		imageHeight = parentHeight;
	      	}
	      	else {
	       		jQuery(this).css({'height': 'auto', 'width': parentWidth});
	 
	       		// set image variables to new dimensions
	       		imageWidth = parentWidth;
	       		imageHeight = imageHeight / diff;
      		}

      		// step 3 - center image in container
      		var leftOffset = (imageWidth - parentWidth) / -2;
      		var topOffset = (imageHeight - parentHeight) / -2;

      		jQuery(this).css({'margin-left': leftOffset, 'margin-top': topOffset});
    	});
    }
</script>
<?php get_footer(); ?>