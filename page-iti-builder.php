<?php
/*
Template Name: Itinerary Builder Page Layout 
*/

require_once( 'itibuilder-commons.php' );

function debugPrint($output_str){
	$VERBOSE = false;
	if($VERBOSE){
		if(is_string($output_str)){
			echo $output_str;
		}
		else{
			var_dump($output_str);
		}
	}
}

function getNextHops($prov_id){
	global $wpdb;
    $prov_data = $wpdb->get_row( "SELECT nexthop_prov FROM samp_iti_provinces WHERE prov_id=".$prov_id );
    $prov_nexthop_data = json_decode($prov_data->nexthop_prov, true);
    //Use empty() to check if there is any data returned
    //If row not found, NULL is returned
    //If row found but next_hop prov has no data, an empty array is returned
    return $prov_nexthop_data;
}


function getProvData($prov_id){
	global $wpdb;
    $prov_data = $wpdb->get_row( "SELECT * FROM samp_iti_provinces WHERE prov_id=".$prov_id );

    return $prov_data;
}

// $prov_array should be an array of prov IDs
// e.g. [2,34,31,4,5]
function getProvDataMulti($prov_array){
	global $wpdb;
	$usr_prov_data = $wpdb->get_results( "SELECT * FROM samp_iti_provinces WHERE prov_id IN (".
		implode(",", $prov_array).
		")");
	return $usr_prov_data;
}


// $prov_array should be an array of prov IDs
// e.g. [2,34,31,4,5]
//
// Return array is indexed by the province ID
// e.g. [ 2 => provData, 34 => provData, 31 => provData, ... ]
function getProvDataMultiIndexedByProvId($prov_array){
	global $wpdb;
	$usr_prov_data = $wpdb->get_results( "SELECT * FROM samp_iti_provinces WHERE prov_id IN (".
		implode(",", $prov_array).
		")");

	$ret_data = array();
	foreach($usr_prov_data as $prov_data){
		$ret_data[intval($prov_data->prov_id)] = $prov_data;
	}
	return $ret_data;
}


// Search for days that match the start and end prov
function getDayItiByProvince($start_prov, $end_prov){
	global $wpdb;
    $prov_data = $wpdb->get_results( "SELECT * FROM samp_iti_day WHERE start_prov=".$start_prov." AND end_prov=".$end_prov );

    return $prov_data;
}



//Recursive algorithm that searches for all possible paths between 
//a specific start node and end node, with a set of nodes
// Each node is the province ID
function getPossiblePaths($start_node, $end_node, $node_pool){
	//debugPrint("<p>start_node:</p>");
	//debugPrint($start_node);
	//debugPrint("<p>end_node:</p>");
	//debugPrint($end_node);
	$paths = array();

 	//Base case:
  	if(empty($node_pool)){
  		$s_canreach_e = false;
  		//Check if start_node can reach end_node
  		$reachable_from_s = getNextHops($start_node);
  		if(!empty($reachable_from_s)){
  			$arr_index = array_search($end_node, $reachable_from_s);
  			if($arr_index !== false){
  				$s_canreach_e = true;
  			}
  		}

        if($s_canreach_e){
        	$paths[] = array(intval($start_node),intval($end_node));
        }
    }
    else {
    	//Normal case:
    	$next_candidates = array_intersect( getNextHops($start_node), $node_pool );
    
      	if( empty($next_candidates) ){
        	$possible_paths = getPossiblePaths($start_node, $end_node, array());
        	if(!empty($possible_paths)){
        		foreach($possible_paths as $pp){
        			$paths[] = $pp;
        		}
        	}
        }
      	else{
      		foreach( $next_candidates as $cand ){
      			//new_pool = node_pool - cand
          		$new_pool = $node_pool;
          		if(($key = array_search($cand, $new_pool)) !== false) {
          			unset($new_pool[$key]);
          			//compact the array
					$new_pool = array_values($new_pool); 
          		}
          		$possible_paths = getPossiblePaths( $cand, $end_node, $new_pool );
          		if(!empty($possible_paths)){
               		foreach ( $possible_paths as $p_path ){
               			//prepend start node to the p_path               			
               			array_unshift($p_path, intval($start_node));
                    	$paths[] = $p_path;
                	}
            	}
            }
        }
    }

    return $paths;
}


function getPrefClosenessScore($arr_place_traits, $arr_user_pref){
	//Compute a score based on current user's preference and the place properties
	//Scoring for Traits closeness
	//within +/- 10 : 2 points
	//within +/- 25 : 1 point
	//else :  0 point

	$trait_score = 0; //init

	foreach($arr_place_traits as $i => $place_trait){
		//Add score
		$pref_diff = abs($place_trait - $arr_user_pref[$i]);
		if($pref_diff <= 10){
			$trait_score += 2;
		}
		else if($pref_diff <= 25){
			$trait_score += 1;
		}
	}

	return $trait_score;
}


//Checks if there is a path that connects nodes Left -> Mid -> Right
function hasPath($left_node, $mid_node, $right_node){
	$reachable_from_L = getNextHops($left_node);	
	//check if mid_node is within the next hops of L
	$arr_index = array_search($mid_node, $reachable_from_L);
	if($arr_index === false){
		return false;
	}

	//If code reach here, L can be connected to M
	$reachable_from_M = getNextHops($mid_node);
	//check if right_node is within the next hops of M
	$arr_index = array_search($right_node, $reachable_from_M);
	if($arr_index === false){
		return false;
	}

	return true;
}

//From an array of possible paths, pick the best one based on user preferences and input
function pickBestPath($paths){

	debugPrint("<p>Inside pickBestPath:</p>");
	debugPrint("<p>Args: paths</p>");
	debugPrint($paths);
	debugPrint($paths[0]);

	$num_days = intval($_POST["num-days"]);
	$arr_user_pref = array();
	$arr_user_pref[] = intval($_POST["pref-shopping"]); //$pref_shopping
	$arr_user_pref[] = intval($_POST["pref-sightsee"]); //$pref_sightsee
	$arr_user_pref[] = intval($_POST["pref-funrelax"]); //$pref_funrelax

	//Get a set of all prov_id that appear in all paths
	$merged = array();
	foreach($paths as $p){
		$merged = array_merge($merged,$p);
	}
	$prov_set = array_unique($merged);//remove duplicate values
	$prov_set = array_values($prov_set);//compact the array

	$prov_data = getProvDataMultiIndexedByProvId($prov_set);
	//prov_data will be indexed by prov_id. So we can random access a prov data using $prov_data[prov_id]

	//1. Remove paths that need a longer duration than specified by user.
	//For each path, add up the total min days (Assume 1 day each for Taipei at start and end)
	for($i=0; $i<count($paths); $i++){
		$path_mindays = 2; //1 day each for 1st n last province
		//For provinces in between, add up the min days
		for($p_index=1; $p_index<count($paths[$i])-1; $p_index++){
			$curr_prov_id = intval($paths[$i][$p_index]);
			$path_mindays += $prov_data[$curr_prov_id]->min_days;
		}
		//If path needs more days, then remove path
		if($path_mindays > $num_days && count($paths) > 1){
			unset($paths[$i]);
		}
	}
	$paths = array_values($paths);//compact the array

	//2. Compute the traits score for each path (exclude start province)
	$trait_scores = array(); //e.g. [5,2,3,3]

	for($i=0; $i<count($paths); $i++){
		//For each province (exclude the first), calculate the avg trait %
		$trait_shopping_total = 0;
		$trait_sightsee_total = 0;
		$trait_funrelax_total = 0;
		for($p_index=1; $p_index<count($paths[$i]); $p_index++){
			$curr_prov_id = intval($paths[$i][$p_index]);
			$trait_shopping_total += intval($prov_data[$curr_prov_id]->prov_trait_shopping);
			$trait_sightsee_total += intval($prov_data[$curr_prov_id]->prov_trait_sightsee);
			$trait_funrelax_total += intval($prov_data[$curr_prov_id]->prov_trait_funrelax);
		}

		$num_p = count($paths[$i])-1;
		$arr_place_traits = array();
		$arr_place_traits[] = $trait_shopping_total/$num_p; //$trait_shopping_avg
		$arr_place_traits[] = $trait_sightsee_total/$num_p; //$trait_sightsee_avg
		$arr_place_traits[] = $trait_funrelax_total/$num_p; //$trait_funrelax_avg

		//Scoring for Traits closeness
		//within +/- 10 : 2 points
		//within +/- 25 : 1 point
		//else :  0 point
		$trait_scores[$i] = getPrefClosenessScore($arr_place_traits, $arr_user_pref);
	}

	//3. Compute Days-Per-Province (DPP) Score
	//DPP = Num_days / province_count (exclude 1st province)

	$dpp_scores = array(); //e.g. [2,1,1,0]
	
	//DPP > 4      -- Score: 0
	//DPP 3.1 - 4  -- Score: 1
	//DPP 2.1 - 3  -- Score: 2
	//DPP 1.1 - 2  -- Score: 1
	//DPP < 1      -- Score: 0

	for($i=0; $i<count($paths); $i++){
		$dpp_scores[$i] = 0; //init
		$curr_dpp = $num_days / (count($paths[$i])-1);
		if($curr_dpp > 1 && $curr_dpp <= 2){
			$dpp_scores[$i] = 1; 	
		}
		else if($curr_dpp > 2 && $curr_dpp <= 3){
			$dpp_scores[$i] = 2;
		}
		else if($curr_dpp > 3 && $curr_dpp <= 4){
			$dpp_scores[$i] = 1;
		}
	}

	//Find path with the highest total scores (trait_scores + dpp_scores)
	$chosen_index = 0;
	$highest_score = $trait_scores[0] + $dpp_scores[0];

	debugPrint("<p>initial highest score: ".$highest_score."</p>");

	for($i=1; $i<count($paths); $i++){
		if(($trait_scores[$i]+$dpp_scores[$i]) > $highest_score){
			$chosen_index = $i;
			$highest_score = $trait_scores[$i] + $dpp_scores[$i];
		}
	}

	debugPrint("<p>Chosen_index: ".$chosen_index."</p>");
	debugPrint("<p>Chosen path:</p>");
	debugPrint($paths[$chosen_index]);

	return $paths[$chosen_index];
}


// $exclude_places will be an array(), where indexes are the place ids
//  e.g. dump exclude_places: a[23]=1, a[533]=1, a[1234]=1 
//
// Returns the row of data the table samp_iti_day of the selected candidate  
function searchDayCandidate2($start_prov, $end_prov, $main_prov, $prevday_endplace, $exclude_places){

	debugPrint("<p>running searchDayCandidate2</p>");
	
	debugPrint("<p>Args: start_prov</p>");
	debugPrint($start_prov);
	debugPrint("<p>Args: end_prov</p>");
	debugPrint($end_prov);
	debugPrint("<p>Args: main_prov</p>");
	debugPrint($main_prov);
	//debugPrint("<p>Args: exclude_places</p>");
	//debugPrint($exclude_places);
	
	$arr_user_pref = array();
	$arr_user_pref[] = intval($_POST["pref-sightsee"]); //$pref_sightsee
	$arr_user_pref[] = intval($_POST["pref-shopping"]); //$pref_shopping
	$arr_user_pref[] = intval($_POST["pref-funrelax"]); //$pref_funrelax

	global $wpdb;
	//We start by querying those entries which match the start and end province. Then we filter and pick from here
	$days_list = $wpdb->get_results( "SELECT * FROM samp_iti_day WHERE start_prov=".$start_prov." AND end_prov=".$end_prov );
	//debugPrint($days_list);

	$bestscore = -1;
	$bestscore_index = -1;
	$parent_list = array();
	$parent_score = array();

	foreach($days_list as $index=>$day_data){

		//debugPrint("<p>Checking candidate </p>");
		//debugPrint($index);

		//From the list, filter out those days which contain places found in the excluded list
		$iti_data = json_decode($day_data->day_itinerary, true);
		$day_places = array();
		$hi_timespent = -1;
		$hi_timespent_index = -1;
		foreach($iti_data as $prov_i=>$prov_data){
			foreach($prov_data["places"] as $place){
				//build the array of place ids to intersect with exclude_places
				$day_places[intval($place["id"])] = 1;
			}

			if($prov_data["timespent"] > $hi_timespent){
				$hi_timespent = $prov_data["timespent"];
				$hi_timespent_index = $prov_i;
			}
		}

		if($main_prov == 0){
			$skip_majority_timespent = true;
		}
		else {
			$skip_majority_timespent = false;	
		}

		//1st requirement to be valid candidate: the greatest timespent must be at the main_prov
		if($skip_majority_timespent || $iti_data[$hi_timespent_index]["province"] == $main_prov){
			//2nd requirement to be valid candidate: places must not be found in the exclude list
			$intersect = array_intersect_key($day_places, $exclude_places);
			if(empty($intersect)){
				//debugPrint("<p>Computing score..</p>");
				//If there are places in the excluded list, don't need to compute its score
				
				//Its a valid candidate, so we compute a score based on its properties
				//Scoring for Traits closeness
				//within +/- 10 : 2 points
				//within +/- 25 : 1 point
				//else :  0 point
				$arr_place_traits = array();
				$arr_place_traits[] = $day_data->day_trait_shopping;
				$arr_place_traits[] = $day_data->day_trait_sightsee;
				$arr_place_traits[] = $day_data->day_trait_funrelax;

				$trait_score = getPrefClosenessScore($arr_place_traits, $arr_user_pref);

				
				//Scoring for location
				// Itineraries with the start place nearer to the end place of 
				// the previous day will have higher points
				//
				//within 2km    : 8 points
				//2km+ to 4km   : 7 points
				//4km+ to 6km   : 6 points
				//6km+ to 8km   : 5 points
				//8km+ to 10km  : 4 points 
				//10km+ to 12km : 3 points  
				//12km+ to 14km : 2 points  
				//14km+ to 16km : 1 points  
				//above 16km    : 0 points
				
				$locality_score = 0;

				if($prevday_endplace > 0){
					$dist_info = $wpdb->get_row("SELECT * FROM samp_iti_dist_table WHERE from_place_id=".$day_data->start_place_id.
						" AND to_place_id=".$prevday_endplace);

					if($dist_info !== null){
						$dist_meter = $dist_info->dist_meter;
						for($limit=16000; $limit>=2000; $limit-=2000){
							if($dist_meter<$limit){
								$locality_score+=1;		
							}
							else{
								break;
							}	
						}
					}
				}

				//Add up the total score
				$day_score = $trait_score + $locality_score;
				//debugPrint("<p>Day score = ".$day_score."</p>");
				if($day_score>$bestscore){
					$bestscore = $day_score;
					$bestscore_index = $index;
				}
			}
			else {
				//If there are repeated places, see how many are repeated
				//If less than half the itinerary repeated, we can consider it as a 'parent' itinerary
				//that can be used to generate a new itinerary if no candidates are found
				debugPrint("<p>Repeated place found</p>");
				//if(count($intersect) <= (count($day_places)/2)){
				if(count($intersect) > 0){
					//Compute the preference score for this parent
					//Scoring for Traits closeness
					//within +/- 10 : 2 points
					//within +/- 25 : 1 point
					//else :  0 point
					$arr_place_traits = array();
					$arr_place_traits[] = $day_data->day_trait_shopping;
					$arr_place_traits[] = $day_data->day_trait_sightsee;
					$arr_place_traits[] = $day_data->day_trait_funrelax;

					$p_score = getPrefClosenessScore($arr_place_traits, $arr_user_pref);
					$parent_score[] = $p_score;
					//Add the parent
					$parent_list[] = $day_data;
				}
			}
		}
	}//end foreach

	if($bestscore_index >= 0){
		debugPrint("<p>searchDayCandidate2 found an existing day:</p>");
		//debugPrint($days_list[$bestscore_index]);
		return $days_list[$bestscore_index];
	}
	else if(!empty($parent_list)){
		debugPrint("<p>searchDayCandidate2 searching new creation:</p>");
		// There were no candidates, do another round of search, which swaps new places
		return searchNewCreations($parent_list, $parent_score, $exclude_places);
	}
	else {
		debugPrint("<p>searchDayCandidate2 found nothing.....</p>");
		return 0;
	}
}


// Returns the row of data the table samp_iti_day of the selected candidate  
//
// Swaps unsuitable places, creating a new entry in the database if necessary
function searchNewCreations($parent_list, $parent_score, $exclude_places){

	$arr_user_pref = array();
	$arr_user_pref[] = intval($_POST["pref-shopping"]); //$pref_shopping
	$arr_user_pref[] = intval($_POST["pref-sightsee"]); //$pref_sightsee
	$arr_user_pref[] = intval($_POST["pref-funrelax"]); //$pref_funrelax

	$chosen_parent_index = -1;
	$iti_data = 0; //used to hold the decoded iti data 

	//Sort the $parent_score from high to low, maintaining the index association
	arsort($parent_score);
	foreach ($parent_score as $p_index => $score) {
		//we iterate from the highest score first
		debugPrint('<p>Looping parent with score '.$score.'</p>');
		$iti_data = json_decode($parent_list[(int)$p_index]->day_itinerary, true);
		$local_exclude = array();
		$day_places = array();
		$go_next_parent = false;

		foreach($iti_data as $prov_data){
			foreach($prov_data["places"] as $place){
				//build the array of place ids that will be excluded from search
				$can_revisit = get_post_meta($place["id"], 'place-can-revisit', true);
				if(empty($can_revisit) && $place["id"]!=0){
					$local_exclude[intval($place["id"])] = 1;
				}
				//build the array of places in the parent itinerary
				$day_places[intval($place["id"])] = 1; 
			}
		}

		//Get the places that we need to swap
		$intersect = array_intersect_key($day_places, $exclude_places);
		debugPrint('<p>Places that need to swap: </p>');
		debugPrint($intersect);

		//For each repeated place, find a suitable place to swap
        foreach($intersect as $repeat_place_id => $dummy){
        	debugPrint('<p>Searching nearby places from place '.$repeat_place_id.' '.get_the_title($repeat_place_id).'</p>');

        	//Combine the exclude places with local_exclude
			$all_exclude = $exclude_places + $local_exclude;
        	debugPrint('<p>Excluding places: </p>');
        	debugPrint($all_exclude);
            $near_places = get_nearby_places($repeat_place_id, 3, 0, $all_exclude);

            if(empty($near_places)){
            	debugPrint('<p>Cannot find any nearby place</p>');
                $go_next_parent = true;
                break;
            }
            //Loop through all near_places, build the array to remove repeats
            // and also compute the preference score
            $swap_candidates_id = array();
            foreach($near_places as $nplace){
                //Get the traits of the place
                $arr_place_traits = array();
                $arr_place_traits[] = get_post_meta($nplace["place_id"], 'place-trait-shopping', true); //$trait_shopping
                $arr_place_traits[] = get_post_meta($nplace["place_id"], 'place-trait-sightsee', true); //$trait_sightsee 
                $arr_place_traits[] = get_post_meta($nplace["place_id"], 'place-trait-funrelax', true); //$trait_funrelax
                
                $p_score = getPrefClosenessScore($arr_place_traits, $arr_user_pref);

                //Store the preference score as the value
                $swap_candidates_id[$nplace["place_id"]] = $p_score;
            }

            //Sort the swap candidates from highest score to lowest.
            arsort($swap_candidates_id);
            //choose the candidate with the highest score, and do the replacement
            reset($swap_candidates_id);//move array pointer to 1st item

            // search $near_places for the place name
            foreach($near_places as $nplace){
                if($nplace["place_id"] == key($swap_candidates_id)){
                    $cand_place_title = $nplace["title"];
                    break;
                }
            }

            $newplace_data = array(
            	'id' => key($swap_candidates_id), 
                'label' => $cand_place_title);
            debugPrint('<p>Found suitable place to swap:</p>');
            debugPrint($newplace_data);

            //We need to search for the index of the place
            $prov_index = -1;
            $place_index = -1;
            foreach($iti_data as $prov_i=>$prov_data){
                foreach($prov_data["places"] as $place_i=>$place){
                    if($place["id"]==$repeat_place_id){
                        $prov_index = $prov_i;
                        $place_index = $place_i;
                    }
                }
            }

            //Add swapped place to the local_exclude
            $local_exclude[(int)$newplace_data["id"]] = 1;
            
            if($prov_index >= 0 && $place_index >= 0){
                $iti_data[$prov_index]["places"][$place_index] = $newplace_data;
            }
            else {
                $go_next_parent = true;
                break;
            }
            
        }

        if(!$go_next_parent){
            $chosen_parent_index = $p_index;
            break;
        }

	}//End trying all parents
	
	if($chosen_parent_index >= 0){
		//We now input the new entry to database..
		$new_entry = $parent_list[$chosen_parent_index];

		//Encode the new day_itinerary in JSON format
		$new_entry->day_itinerary = json_encode($iti_data);

		//Recompute the day traits
		$total_shopping = 0;
		$total_sightsee = 0;
		$total_funrelax = 0;
		$total_num_places = intval($new_entry->num_places);
		foreach($iti_data as $prov_data){
			foreach($prov_data["places"] as $place){
                $total_shopping += intval( get_post_meta( $place["id"], 'place-trait-shopping', true ) );
                $total_sightsee += intval( get_post_meta( $place["id"], 'place-trait-sightsee', true ) );
                $total_funrelax += intval( get_post_meta( $place["id"], 'place-trait-funrelax', true ) );
            }
        }
        $new_entry->day_trait_shopping = (int)($total_shopping/$total_num_places);
        $new_entry->day_trait_sightsee = (int)($total_sightsee/$total_num_places);
        $new_entry->day_trait_funrelax = (int)($total_funrelax/$total_num_places);

		//Update the start_place_id and end_place_id
		$new_entry->start_place_id = $iti_data[0]["places"][0]["id"];

		$last_prov_data = end($iti_data);
		$last_place = end($last_prov_data["places"]);
		$new_entry->end_place_id = $last_place["id"]; 

		global $wpdb;
		$wpdb->insert( 'samp_iti_day', 
            array( 
                'day_itinerary' => $new_entry->day_itinerary,
                'num_places' => $new_entry->num_places,
                'num_prov' => $new_entry->num_prov,
                'day_trait_shopping' => $new_entry->day_trait_shopping,
                'day_trait_sightsee' => $new_entry->day_trait_sightsee,
                'day_trait_funrelax' => $new_entry->day_trait_funrelax,
                'need_driver' => $new_entry->need_driver,
                'start_prov' => $new_entry->start_prov,
                'end_prov' => $new_entry->end_prov,
                'start_place_id' => $new_entry->start_place_id,
                'end_place_id' => $new_entry->end_place_id
            ), 
            array( 
                '%s','%d','%d','%d','%d',
                '%d','%d','%d','%d','%d',
                '%d'
            )
        );

        $new_entry->day_id = $wpdb->insert_id;

        debugPrint('<p>searchNewCreations() returned:</p>');
        debugPrint($new_entry);
        return $new_entry;
	}
	else {
		debugPrint('<p>searchNewCreations() returned nothing.....</p>');
		return 0;
	}
}


//Checks if the user selected provinces are enough for the duration of the trip
function needSuggestProv($total_days, $usr_prov_data){
	$key_prov_needed = (int)($total_days/3);

	//See how many key provinces user selected
	$usr_keyprov_count = 0;
	foreach($usr_prov_data as $p_data){
		if($p_data->is_key_prov){
			$usr_keyprov_count++;
		}
	}
	
	return ($key_prov_needed > $usr_keyprov_count);
}

function getInitialIti(){
	global $wpdb;
	//Get the full data of the provinces that user has selected
	$iti_days = $wpdb->get_results("SELECT * FROM samp_iti_initial, samp_iti_day".
		" WHERE samp_iti_initial.iti_day_id = samp_iti_day.day_id ORDER BY order_index ASC");

	$prov_order = array(); //Per row: {'prov_id'=>?, 'days_spent'=>? }
	$prov_visited = array(); // $prov_visited[prov_id] = 0
	$day_ids = array();

	foreach($iti_days as $dayrow){

		$day_ids[] = $dayrow->day_id;
		$iti_data = json_decode($dayrow->day_itinerary, true);
        $main_prov = 0; //The province where most time of the day is spent
        $main_prov_timespent = -1; //% time spent in main prov
        foreach($iti_data as $prov_data){ 
            $curr_provid = intval($prov_data["province"]);
            //Find the main_prov
            if(intval($prov_data["timespent"]) > $main_prov_timespent){
            	$main_prov = $curr_provid;
            	$main_prov_timespent = intval($prov_data["timespent"]);
            }
        }//end foreach

        //Update prov_visited
        $prov_visited[$main_prov] = 0;

        //Now we update prov_order
        //If the last prov is the same, just increment days spent,
        //if not, add new entry
        if(count($prov_order) > 0 &&
        	$main_prov == $prov_order[count($prov_order)-1]['prov_id']){

        	$prov_order[count($prov_order)-1]['days_spent'] += 1;
        }
        else {
        	$prov_order[] = array('prov_id'=>$main_prov,
        		'days_spent'=>1);
        }
    	
	}//end foreach

	//Return with an array of the provinces visited (array index is the prov index)
	//Array structure: array( 
	//    'day_ids' => $day_ids
	//    'prov_order' => $prov_order,
	//    'prov_visited' => $prov_visited,
	//    'iti_days' => $iti_days )

	return array( 'day_ids' => $day_ids, 
		'prov_order' => $prov_order,
		'prov_visited' => $prov_visited, 
		'iti_days' => $iti_days );
}

//$prov_visited is an array where the index are the id of the provinces visited
//The value is not used currently, so its usually 0
// e.g. [ "23"=>0, "41"=>0, "123"=>0 ]
//
// Returns true/false
function provSetEqual($prov_visited){
	//Compare with the POST data, see if the provinces visited are the same
	$curr_prov = array_flip($_POST["prov-sel"]);

	if(count($prov_visited) == count($curr_prov)){
		foreach($prov_visited as $pv_key => $pv_val){
			if(!array_key_exists($pv_key, $curr_prov)){
				return false;
			}
		}
		return true;
	}
	else {
		return false;
	}
}

//Initialize the days spent in prov_order, allocating the min days for each province
function assignProvMinDays($prov_order) {

	//Retrieve province data from the database
	//Each usr_prov_data element is a row in the DB
	//withe the prov_id used as the index 
	$usr_prov_data = array();
	foreach ($prov_order as $prov) {
		$usr_prov_data[intval($prov['prov_id'])] = getProvData($prov['prov_id']);
	}
	
	//If province 'days_spent' is not set, assign the min_days to all provinces first
	//For first and last province, set to 1
	for($p_index = 0; $p_index < count($prov_order); $p_index++) {
		$curr_prov_id = intval($prov_order[$p_index]['prov_id']);
		if($prov_order[$p_index]['days_spent'] == 0){
			if($p_index == 0 || $p_index == count($prov_order)-1){
				$prov_order[$p_index]['days_spent'] = 1;
			}
			else{
				$prov_order[$p_index]['days_spent'] = intval($usr_prov_data[$curr_prov_id]->min_days);
			}
		}
	}
	//debugPrint("<p>assignProvMinDays() After assigning min days. prov_order:</p>");
	//debugPrint($prov_order);

	return $prov_order;
}

// assignProvDaysSpent() will assign the days using the remaining amount
// of days after the minimum is assigned. 
//
// $prov_order is expected to be in the format:
//     [ 
//        ['prov_id' => x , 'days_spent' => x ], 
//        ['prov_id' => y , 'days_spent' => y ],
//         ... 
//     ]
//
// $prov_day_offset is in the form $array{ (prov_id, offset) }. e.g. $arr { (24, -1) }
// where prov_id is the key of the array
//
// Returns: $prov_order, with the 'days_spent assigned'
//          If num_days is insufficient, the assignment will still be done
//
// Note: Use is_array() to check the return data, if its a number or the prov_order array
function assignProvDaysSpent($prov_order, $days_left, $prov_day_offset) {
	debugPrint("<p>running assignProvDaysSpent()</p>");
	debugPrint("<p>days_left: ".$days_left."</p>");
	debugPrint("<p>prov_day_offset</p>");
	debugPrint($prov_day_offset);
	//debugPrint("<p>assignProvDaysSpent() Initial prov_order:</p>");
	//debugPrint($prov_order);

	//Retrieve province data from the database
	//Each usr_prov_data element is a row in the DB
	//withe the prov_id used as the index 
	$usr_prov_data = array();
	foreach ($prov_order as $prov) {
		$usr_prov_data[intval($prov['prov_id'])] = getProvData($prov['prov_id']);
	}
	
	//If there are residual days_left, extend the length of stay 
	//till the recommended day for the all provinces 
	//starting with the one closest to the user's preferred style (shopping/sightsee/fun&relax)

	if($days_left > 0){
		$arr_user_pref = array();
		$arr_user_pref[] = intval($_POST["pref-shopping"]); //$pref_shopping
		$arr_user_pref[] = intval($_POST["pref-sightsee"]); //$pref_sightsee
		$arr_user_pref[] = intval($_POST["pref-funrelax"]); //$pref_funrelax
		
		//We first arrange the provinces in the descending order of trait score
		$arr_trait_scores = array();
		foreach($usr_prov_data as $pdata){
			$arr_place_traits = array();
			$arr_place_traits[] = $pdata->prov_trait_shopping;
			$arr_place_traits[] = $pdata->prov_trait_sightsee;
			$arr_place_traits[] = $pdata->prov_trait_funrelax;
			$arr_trait_scores[] = getPrefClosenessScore($arr_place_traits, $arr_user_pref);
		}
		arsort($arr_trait_scores);
		
		foreach($arr_trait_scores as $prov_data_index=>$trait_score){

			//get the prov data
			reset($usr_prov_data);
			for($c=0;$c<$prov_data_index;$c++){
				//Move pointer to the correct province
				next($usr_prov_data);
			}
			$curr_prov_data = current($usr_prov_data);
			//Find the po_index of the curr province
			$po_index = -1;
			for($pi = 1; $pi < count($prov_order); $pi++) {
				if($prov_order[$pi]['prov_id'] == $curr_prov_data->prov_id){
					$po_index = $pi;
					break;
				}
			}

			$prov_recomdays = intval($curr_prov_data->recom_days);
			//Add offset if any (offset value is usually negative)
			if(is_array($prov_day_offset) && 
				array_key_exists($curr_prov_data->prov_id, $prov_day_offset)){
				//Reduce the days_spent at the province according to the offset (offset is negative)
				$prov_order[$po_index]['days_spent'] += $prov_day_offset[$curr_prov_data->prov_id];
				//Set the prov_recomdays to equal that of days_spent, to prevent adding of days to this province
				$prov_recomdays = $prov_order[$po_index]['days_spent'];
			}

			//Check: if the days_spent at the curr prov is already at the reccom, then skip
			if($prov_order[$po_index]['days_spent'] != $prov_recomdays){

				$prov_timespent = $prov_order[$po_index]['days_spent'];
				
				if($days_left <= $prov_recomdays-$prov_timespent){
					$prov_order[$po_index]['days_spent'] += $days_left;
					$days_left = 0;
				}
				else {
					$prov_order[$po_index]['days_spent'] = $prov_recomdays;
					$days_left -= $prov_recomdays-$prov_timespent;
				}

				if($days_left <= 0){ break; }
			}

			//If days_spent at curr prov got reduced to 0, remove it from prov_order
			if($prov_order[$po_index]['days_spent'] <= 0){
				unset($prov_order[$po_index]);
				//compact the array
				$prov_order = array_values($prov_order); 
			}
		}

		//debugPrint("<p>assignProvDaysSpent() After assigning up to recom days. prov_order:</p>");
		//debugPrint($prov_order);
	}

	//If there are still days left after assigning the recommended.. suggest additional provinces
	if($days_left > 0){
		global $wpdb;
    	$prov_candidates = $wpdb->get_results( "SELECT * FROM samp_iti_provinces WHERE is_keyprov=1 ORDER BY recom_days DESC" );

    	if(count($prov_candidates) > 0){
	    	foreach($prov_candidates as $p_cand){
	    		//First check if $p_cand is in prov_order
	    		$has_prov = false;
	    		foreach($prov_order as $s_prov){
	    			if($s_prov['prov_id'] == $p_cand->prov_id){
	    				$has_prov = true;
	    				break;
	    			}
	    		}

	    		if(!$has_prov){
		    		//try each candidate if a path can be formed
		    		$found_cand = false;
		    		$insert_index = 0;
		    		for($prv=1; $prv<count($prov_order); $prv++){
		    			if(hasPath($prov_order[$prv-1]['prov_id'], 
		    				$p_cand->prov_id,
		    				$prov_order[$prv]['prov_id'])){
		    				//If has a path, break out and add the prov
		    				$found_cand = true;
		    				$insert_index = $prv;
		    				break;
		    			}
		    		}

		    		if($found_cand){
		    			//Compute the days to spend at this suggested province
		    			$days_to_spend = 0;
		    			if($days_left <= $p_cand->recom_days){
							$days_to_spend = $days_left;
							$days_left = 0;
						}
						else {
							$days_to_spend = intval($p_cand->recom_days);
							$days_left -= $days_to_spend;
						}
		    			//Add to prov_order (insert to array)
		    			$new_prov = array("prov_id"=> intval($p_cand->prov_id),
		    				"days_spent"=>$days_to_spend);
		    			debugPrint("<p>Going to add new_prov:</p>");
						debugPrint($new_prov);
						//Wrap in an array
						$new_arr = array($new_prov);
		    			array_splice($prov_order, $insert_index, 0, $new_arr);
		    			
		    			//If there are no more days_left, break out of foreach
		    			if($days_left<=0){ break; }
		    		}
	    		}
	    	}//end foreach (suggested candidate)
    	}
    	else {
    		debugPrint("<p>assignProvDaysSpent() Unable to find suggested province</p>");
    		break;
    	}
	}

	debugPrint("<p>assignProvDaysSpent() returning prov_order:</p>");
	debugPrint($prov_order);
	return $prov_order;		

}


// Based on the $prov_order, assign the suitables days to the itinerary
// Returns: array(
//		'iti_days' => $iti_days,
//		'day_ids' => $day_ids);
//     
//      Return 0 if nothing can be found
//
//      Return -1 if assignProvDaysSpent need to be re-run
function assignDays2($prov_order){
	
	debugPrint("<p>running assignDays2</p>");

	$arv_airport = $_POST["arv-airport"];

	$exclude_places = array(); //A list of places to exclude from the search
	$iti_days = array(); //An ordered array containing the days for the itinerary (each element is a row in the DB)
	$day_offset = array();
	$need_day_reassign = false;

	//For first day, search places that start with arv_airport and end in Taipei
	$first_day_choices = getDayItiByProvince($arv_airport, $prov_order[0]['prov_id']);
	$iti_days[] = $first_day_choices[0];

	//Loop through all places in the 1st day, add them to the $exclude_places list
	//if 'can_revisit' = false;
	$iti_data = json_decode($first_day_choices[0]->day_itinerary, true);
	foreach($iti_data as $prov_data){
		foreach($prov_data["places"] as $place){
			$can_revisit = get_post_meta($place["id"], 'place-can-revisit', true);
			if(empty($can_revisit) && $place["id"]!=0){
				$exclude_places[intval($place["id"])] = 1; //using place ID as array index to ensure no repeated entries
			}
		}
	}

	$prevday_endplace = $first_day_choices[0]->end_place_id;
	$prevday_prov = intval($prov_order[0]['prov_id']);
	$nextday_prov = 0;
	//Outer loop to assign itinerary for each day
	for($p_index=1; $p_index<count($prov_order); $p_index++){
		//another for loop for the num of days in each province
		$curr_prov = intval($prov_order[$p_index]['prov_id']);
		$curr_prov_dayspent = intval($prov_order[$p_index]['days_spent']);
		for($p_day_index=0; $p_day_index<$curr_prov_dayspent; $p_day_index++){
			//Update the $nextday_prov
			if($p_index == count($prov_order)-1 && $p_day_index == $curr_prov_dayspent-1){
				//If its the last day skip the loop. We will assign manually later
				continue;
			}
			else{
				//if its not the last day at the last province, we can compute the nextday prov
				if($p_day_index < $curr_prov_dayspent-1){
					$nextday_prov = $curr_prov;
				}
				else{
					$nextday_prov = intval($prov_order[$p_index+1]['prov_id']);
				}
			}

			//Check the province of the prevday_endplace
			debugPrint("<p>prevday_endplace: ".$prevday_endplace."</p>");
			if(!empty($prevday_endplace)){
				$loc_list = wp_get_post_terms($prevday_endplace, 'Location', array("fields" => "ids"));
				$prevday_endplace_prov = $loc_list[0];
			}
			else {
				$prevday_endplace_prov = $curr_prov;	
			}
			
			debugPrint("<p>Beginning search for pindex ".$p_index."</p>");
			debugPrint("<p>prevday_endplace_prov: ".$prevday_endplace_prov.
				" nextday_prov: ".$nextday_prov.
				" curr_prov: ".$curr_prov."</p>");

			//1st Search using :
			// start prov = prevday_prov
			// end prov = curr_prov
			// main prov = curr_prov
			//$day_candidate = searchDayCandidate2($prevday_prov, $nextday_prov, $curr_prov, $prevday_endplace, $exclude_places);
			$day_candidate = searchDayCandidate2($prevday_endplace_prov, $curr_prov, $curr_prov, 
				$prevday_endplace, $exclude_places);


			//2nd search using same as 1st search, but don't care about the majority time spent in curr prov
			if(empty($day_candidate)){
				debugPrint("<p>Doing 2nd search</p>");
				$day_candidate = searchDayCandidate2($prevday_endplace_prov, $curr_prov, 0, $prevday_endplace, $exclude_places);	
			}


			if(!empty($day_candidate)){
				$iti_days[] = $day_candidate;
				//Update the $exclude_places
				$iti_data = json_decode($day_candidate->day_itinerary, true);
				foreach($iti_data as $prov_data){
					foreach($prov_data["places"] as $place){
						//build the array of place ids to intersect with exclude_places
						$can_revisit = get_post_meta($place["id"], 'place-can-revisit', true);
						if(empty($can_revisit) && $place["id"]!=0){
							$exclude_places[intval($place["id"])] = 1;
						}
					}
				}

				//Update the $prevday_endplace
				$prevday_endplace = $day_candidate->end_place_id;
			}
			else{
				//Can't find day even after 2nd search. We have to re-run assignProvDaysSpent
				//$prevday_endplace = 0;
				$day_offset[$curr_prov] = -1;
				$need_day_reassign = true;
				$p_index = count($prov_order);//To break outer for loop
				break;
			}
			//Update the $prevday_prov
			$prevday_prov = $curr_prov;	
		}//end inner for
		
	}//end outer for


	if($need_day_reassign){
		debugPrint("<p>2nd search failed. We have to re-run assignProvDaysSpent. day_offset: </p>");
		debugPrint($day_offset);
		return $day_offset;
	}


	//Finally we find a candidate for the last day..
	$day_candidate = searchDayCandidate2($curr_prov, intval($arv_airport), $curr_prov, $prevday_endplace, $exclude_places);
	$iti_days[] = $day_candidate;

	if(empty($iti_days)){
		return 0;
	}
	else {
		//Get the day_ids array from the iti_days
		foreach($iti_days as $day){
			$day_ids[] = (!empty($day))?$day->day_id:0;
		}

		$r_data = array(
			'iti_days' => $iti_days,
			'day_ids' => $day_ids);

		return $r_data;
	} 
}



function adjustIti($cond_iti_data){

	//reset the 'days_spent' values in prov_order
	$prov_order = $cond_iti_data['prov_order'];
	for($p=0; $p<count($prov_order); $p++){
		if($p==0 || $p==count($prov_order)-1){
			//For first and last province, set to 1 (arrv and depart days)
			$prov_order[$p]['days_spent'] = 1;
		}
		else {
			$prov_order[$p]['days_spent'] = 0;
		}
	}

	$prov_order = assignProvMinDays($prov_order);
	//Compute the days left
	$init_days_spent = 0;
	foreach($prov_order as $p){
		$init_days_spent += $p['days_spent'];
	}
	$days_left = $_POST["num-days"] - $init_days_spent;
	$prov_order = assignProvDaysSpent($prov_order, $days_left, 0);

	$r_data = assignDays2($prov_order);
	$retry = 5;
	while(!array_key_exists('day_ids', $r_data)){
		//rerun assignProvDaysSpent
		reset($r_data); //move internal array pointer to the first position
		
		$prov_order = assignProvDaysSpent($prov_order, abs(current($r_data)), $r_data);
		$r_data = assignDays2($prov_order);

		if(--$retry <= 0){ break; }
	}

	$iti_data = array(
		'prov_visited' => $cond_iti_data['prov_visited'],
		'prov_order' => $prov_order,
		'day_ids' => $r_data['day_ids'],
		'iti_days' => $r_data['iti_days']
		);
	return $iti_data;
	
}

/*
Return values:
A. Under normal cases the $iti_data array will be returned
   e.g. $iti_data = array(
				'prov_visited' => $prov_visited,
				'prov_order' => $prov_order,
				'day_ids' => $r_data['day_ids'],
				'iti_days' => $r_data['iti_days']);

B. AssignProvDaysSpent() might fail if there is insufficient days.
   A negative integer will be returned, representing the number of days short

C. getPossiblePaths() may not be able to find a path, 
   0 will returned
*/
function recomputeIti2(){

	$num_days = intval($_POST["num-days"]);
	
	$usr_prov_id = $_POST["prov-sel"];
	debugPrint("<p>recomputeIti2().. Num days ".$num_days."</p>");

	// start/end node is Taipei for now
	$init_loc_term = get_term_by('slug', 'taipei-taiwan', 'Location');
	//Remove taipei from the node_pool if its inside
	$init_loc_index = array_search( $init_loc_term->term_id, $usr_prov_id );
	
	//debugPrint("<p>finding value: Index found:</p>");
	//debugPrint($init_loc_term->term_id);
	//debugPrint($init_loc_index);

	if($init_loc_index !== false){
		unset($usr_prov_id[$init_loc_index]);
	}
	$usr_prov_id = array_values($usr_prov_id);

	//debugPrint("<p>usr_prov_id after removal:</p>");
	//debugPrint($usr_prov_id);

	$paths = getPossiblePaths($init_loc_term->term_id, $init_loc_term->term_id, $usr_prov_id);
	//debugPrint("<p>Possible paths:</p>");
	//debugPrint($paths);
	if(!empty($paths)){

		$bestpath = pickBestPath($paths);
		//debugPrint("<p>Best province order chosen:</p>");
		//debugPrint($bestpath);

		$prov_order = array();
		$prov_visited = array();
		foreach($bestpath as $p_id){
			$prov_order[] = array(
				'prov_id' => $p_id,
				'days_spent' => 0
			);

			$prov_visited[intval($p_id)] = 0; 
		}

		$prov_order = assignProvMinDays($prov_order);
		//Compute the days left
		$init_days_spent = 0;
		foreach($prov_order as $p){
			$init_days_spent += $p['days_spent'];
		}
		$days_left = $num_days - $init_days_spent;
		$prov_order = assignProvDaysSpent($prov_order, $days_left, 0);

		$r_data = assignDays2($prov_order);
		//If assignDays2 fails to assign a day, an offset will be returned, showing which province to reduce
		$retry = 5;
		while(!array_key_exists('day_ids', $r_data)){
			//rerun assignProvDaysSpent
			reset($r_data); //move internal array pointer to the first position
			
			$prov_order = assignProvDaysSpent($prov_order, abs(current($r_data)), $r_data);
			$r_data = assignDays2($prov_order);

			if(--$retry <= 0){ break; }
		}

		$iti_data = array(
			'prov_visited' => $prov_visited,
			'prov_order' => $prov_order,
			'day_ids' => $r_data['day_ids'],
			'iti_days' => $r_data['iti_days']
			);
		return $iti_data;
		
	}

	return 0;	
}


//Read the itinerary data and output to page
// Expected structure for $iti_days:
//  array() of $iti_day, where each $iti_day should contain:
//   $iti_day['day_itinerary']
function outputIti($iti_days, $province_map){
	$day_num = 1;
	foreach($iti_days as $day_index=>$dayrow){
		$iti_data = json_decode($dayrow->day_itinerary, true);
		$provinces = "";
        $places = "";
        $first_prov = true;
        $prev_provid = -1;
        $place_index = 0;

		//Loop through to get provinces first
        foreach($iti_data as $prov_data){ 
            $curr_provid = intval($prov_data["province"]);
            //Building provinces string
            if($curr_provid != $prev_provid){
                if(!$first_prov){
                    $provinces .= " > ";
                }
                else {
                    $first_prov = false;
                }
                $provinces .= $province_map[$curr_provid];
                $prev_provid = $curr_provid;
            }
        }

        //Start a new div for a new page every 2 days
        if($day_num%2 > 0){
        	//echo '<div id="iti-detail-'.(($day_num+1)/2).'" class="hidden">';
        	echo '<div class="ib-page hidden">';
        }
?>
        <div class="sixcol ib-daybox hidden <?php echo ($day_num%2==0 ? 'last':'first');?>">
        	<input type="hidden" class="ib-day-id" value="<?php echo $dayrow->day_id ?>" />
			<div class="ib-daylabel">
				<p>Day</p>
				<p class="ib-daynum"><?=$day_num?></p>
			</div>
			<span class="ib-day-header"><?=$provinces?></span>
			<div class="clear"></div>
<?php
        foreach($iti_data as $prov_data){ 
            $curr_provid = intval($prov_data["province"]);
            
            $prov_term = get_term_by('id',$curr_provid,'Location');
            $curr_prov_slug = $prov_term->slug;

            //Building places string
            $places_arr = $prov_data["places"];
            foreach ($places_arr as $place_d) {
            	$thepost = get_post($place_d['id']);
            	$postlink = get_permalink($place_d['id']);
            	$image_url = wp_get_attachment_url(get_post_thumbnail_id($place_d['id']));
            	if(empty($image_url)){
            		$image_url = get_template_directory_uri().'/library/images/logo-gray-thumb-4by3.png';
            	}

            	//preg_match('~\((.*?)\)~', $place_d['label'], $output);
            	$placename_parts = split_place_name($place_d['label']);
            	$place_title_eng = $placename_parts[2];
?>
				<div id="<?php echo "itiday_".$day_index."_".$place_index;?>" class="ib-place-entry">
					<div class="ib-place-img">
						<a class="ib-img-anchor" target="_blank" href="<?php echo (empty($postlink)?'#':$postlink); ?>">
							<img src="<?=$image_url?>" />
						</a>
					</div>
					<div class="ib-place-rightcol">
						<span class="tag-loc-<?=$curr_prov_slug?>"><?=$province_map[$curr_provid]?></span>
						<?php if($place_d["id"] != 0): ?>
							<div class="blue-pill-button ib-btn-swap" place-id="<?=$place_d['id']?>">Swap</div>
						<?php endif ?>
						<a class="ib-title-anchor no-decoration" target="_blank" href="<?php echo (empty($postlink)?'#':$postlink); ?>">
							<p class="ib-place-title"><?=$place_title_eng?></p>
						</a>
					</div>
					<input type="hidden" class="ib-place-excerpt" 
						value="<?php if(!empty($thepost)){echo $thepost->post_excerpt;} ?>" />
					<input type="hidden" class="ib-placename-chinese" 
						value="<?=$placename_parts[0]?>" />
					<input type="hidden" class="ib-placename-hanyu" 
						value="<?=$placename_parts[1]?>" />
				</div>
<?php
         
            	$place_index++;
            }
        }//end foreach (places in a day)
        //Close the div for ib-daybox
?>
		</div>

<?php
		//Add a clearing div, and close the ib-page div 
		//If its the last day we also need to close it
		if($day_num%2==0 || $day_num == count($iti_days)){
			echo '<div class="clear"></div> </div>';
		}
    	$day_num++;
	}//end foreach (days in a itinerary)
}


function outputError($errType, $args){
	if($errType === 1){
		//Arg passed in is (user selected days) - (actual days in itinerary)
		$diff_days = abs($args);

		if($args < 0){
			//User is recommended to extend the itinerary
			?>
				<h3>For your choice of provinces, we recommend that you extend your trip by at least 
				<?=$diff_days?> more day<?php echo ($diff_days>1)?"s":""; ?></h3>
			<?php
		}
		else {
			//User is recommended to reduce the number of days
			?>
				<h3>For your choice of provinces, we recommend that you reduce your trip by at least 
				<?=$diff_days?> day<?php echo ($diff_days>1)?"s":""; ?></h3>
			<?php
		}
	}
	else if($errType === 2){
		//User is notified of the provinces that cannot be included
		$missing_prov = "";
		$item_index = 0;
		foreach($args as $prov_id=>$val){
			$term = get_term( $prov_id, "Location" );
			if($item_index == 0){
				$missing_prov .= $term->name;
			}
			else if($item_index == count($args)-1){
				$missing_prov .= " & ".$term->name;
			}
			else{
				$missing_prov .= ", ".$term->name;
			}
			$item_index++;
		}
		?>
		<h3>We are unable to include <?=$missing_prov?> in your itinerary.</h3>
		<?php
	}
	else if($errType === 3){
		?>
		<h3>For your choice of provinces, we could not find any suitable itinerary.</h3>
		<?php
	}
}

?>

<?php get_header();

//Build the province map (for easy search of province name using id)
 $gt_args = array(
    'orderby' => 'id', 
    'order'   => 'ASC',
); 
$terms = get_terms("Location", $gt_args);
$province_map = array();
foreach($terms as $term){
    $province_map[intval($term->term_id)] = $term->name;
}

$num_days = 0;
$has_alert = false;
//$err_type = 0;
//$err_args = 0;
$err_array = array(); //Key of array is the error type, value is the err_args

//if( isset($_POST["num-days"]) ){
if( isset($_POST["post_iti_data"]) ){

	$num_days = $_POST["num-days"];
	//Do recompute
	//$num_days = intval($_POST["num-days"]);
	$pref_sightsee = intval($_POST["pref-sightsee"]);
	$pref_shopping = intval($_POST["pref-shopping"]);
	$pref_funrelax = intval($_POST["pref-funrelax"]);
	$cond_iti_data = json_decode( urldecode($_POST["post_iti_data"]), true );
	

	if(provSetEqual($cond_iti_data['prov_visited'])){
		$iti_data = adjustIti($cond_iti_data);
		//debugPrint("<p>provSetEqual=TRUE... Adjust itinerary</p>");
	}
	else {
		$iti_data = recomputeIti2();
		//debugPrint("<p>provSetEqual=FALSE...Recompute itinerary</p>");
	}

	if(is_int($iti_data)){
		if($iti_data >= 0){
			//No paths found for chosen provinces
			//outputError(3, $iti_data); //error type 3 is: No possible path
			$err_array[3] = $iti_data;
			//$err_type = 3;
			//$err_args = $iti_data;
			$has_alert = true;
		}
	}
	else {
		//If prev posted choices has any province not in iti_data
		// alert the user
		$posted_prov = array_flip($_POST["prov-sel"]);
		$prov_diff = array_diff_key($posted_prov,
			$iti_data['prov_visited']);

		debugPrint("<p>Checking prov_diff....</p>");
		debugPrint("<p>prov_visited: </p>");
		debugPrint($iti_data['prov_visited']);
		debugPrint("<p>posted prov: </p>");
		debugPrint($posted_prov);

		if(!empty($prov_diff)){
			//If found a province that was not in the new itinerary... show warning
			//outputError(2, $prov_diff); //error type 2 is: chosen province not in itinerary
			$err_array[2] = $prov_diff;
			//$err_type = 2;
			//$err_args = $prov_diff;
			$has_alert = true;
		}
		
		//If user selected num of days not equal to itinerary, alert user
		if( (count($iti_data['day_ids']) != 0) &&
			(intval($_POST["num-days"]) - count($iti_data['day_ids']) != 0) ){
			//outputError(1, $num_days - $usr_selected_num_days); //error type 1 is: auto added days
			$err_array[1] = intval($_POST["num-days"]) - count($iti_data['day_ids']);
			//$err_type = 1;
			//$err_args = intval($_POST["num-days"]) - count($iti_data['day_ids']);
			$num_days = count($iti_data['day_ids']);
			$has_alert = true;
		}
	}
}
else if ( isset($_POST["num-days"]) ){
	$num_days = $_POST["num-days"];
	$iti_data = recomputeIti2();
}
else {
	//Get the initial itinerary from DB
	$iti_data = getInitialIti();
	$num_days = count($iti_data['day_ids']);
}

?>	
	<input id="place_swap_url" type="hidden" value="<?php echo admin_url('admin-ajax.php').'?action=get_swap_suggestions'; ?>" />
	<input id="pop_places_url" type="hidden" value="<?php echo admin_url('admin-ajax.php').'?action=get_popular_places'; ?>" />
	<input id="do_export_url" type="hidden" value="<?php echo get_template_directory_uri().'/library/do-export.php'; ?>" />
	<input id="do_email_url" type="hidden" value="<?php echo get_template_directory_uri().'/library/do-send-iti-email.php'; ?>" />
	<input id="template_dir_uri" type="hidden" value="<?php echo get_template_directory_uri(); ?>" />

	<div id="itibuilder-control" class="ib-panel clearfix hidden">
		<form id="desk-iti-form" method="POST">
			<div class="twocol first clearfix">
				<div class="eightcol first">
					<p>AIRPORT:</p>
					<input type="checkbox" name="arv-airport" value="60" checked> Taoyuan<br>
				</div>
				<div class="fourcol last">
					<p>DAYS:</p>
					<select name="num-days">
					<?php for($i=3;$i<21;$i++) {
						echo '<option value="'.$i.'" ';
						if($num_days == $i){ echo "selected"; }
						echo '>'.$i.'</option>';
					} ?>
					</select>
				</div>
			</div>

			<div class="eightcol">
				<p>PROVINCE:</p>
				<table id="control-prov">
					<?php 
						//Read the selected provinces into an array, using priv_id as the key
						$exclude_prov = array(26,55,60,61,87,106,129,132,148,171,172);
						$pcount = 0;
						$prov_visited = array();

						if(is_int($iti_data)){ //If error, then show the previously selected options
							$prov_visited = array_flip($_POST["prov-sel"]);
						}
						else {
							$prov_visited = $iti_data['prov_visited'];
						}

						//Table will have 5 columns
						$pmap_iter = 0;
						$num_cols = 5;

						//Remove exclude_prov from province_map
						$reduced_prov_map = array_diff_key($province_map, array_flip($exclude_prov));
						//foreach($province_map as $pid => $pname){
						foreach($reduced_prov_map as $pid => $pname){
							if($pmap_iter%$num_cols == 0){
								//start a new row
								echo '<tr>';
							}

							echo '<td><input type="checkbox" class="cbox-prov-sel" name="prov-sel[]" value="'.$pid.'" ';
							echo (isset($prov_visited[$pid]))?'checked':' ';
							echo '> '.$pname.'</td>';

							//Check if we need to close the table row
							if($pmap_iter%$num_cols == $num_cols-1 ||
								$pmap_iter == count($reduced_prov_map)-1){
								echo '</tr>';
							}

							$pmap_iter++;
						}
					?>
				</table>
			</div>

			<div class="twocol last">
				<p>PREFERENCE:</p>
				Shopping <input type="range" id="pref-shopping" name="pref-shopping"
					min="0" max="100" step="25"
					value="<?php echo (isset($_POST["pref-shopping"]))?$_POST["pref-shopping"]:50; ?>" 
					><br>
				Sightseeing <input type="range" id="pref-sightsee" name="pref-sightsee"
					min="0" max="100" step="25"
					value="<?php echo (isset($_POST["pref-sightsee"]))?$_POST["pref-sightsee"]:50; ?>" 
					><br>
				Fun &amp; Relax <input type="range" id="pref-funrelax" name="pref-funrelax"
					min="0" max="100" step="25"
					value="<?php echo (isset($_POST["pref-funrelax"]))?$_POST["pref-funrelax"]:50; ?>" 
					><br>
			</div>
			<?php
			//Prepare the condensed form of the data
			$cond_iti_data = array( 'day_ids' => $iti_data['day_ids'],
				'prov_visited' => $iti_data['prov_visited'],
				'prov_order' => $iti_data['prov_order']);
			?> 
			<input id="post_iti_data" type="hidden" name="post_iti_data" 
				value="<?php echo urlencode(json_encode($cond_iti_data))?>">

			<input id="iti-control-submit" type="submit" value="Apply changes"/>
		</form>
	</div>
	<!--  END itibuilder-control  -->
	<div id="btn-toggleview-control" class="iti-action-btn" style="margin-right:40px">
		<img src="<?php echo get_template_directory_uri().'/library/images/ib_btn_edit_inv.png'?>" alt="Customize"/>
		<p>Modify</p>
	</div>
	<div id="btn-export-pdf" class="iti-action-btn">
		<img src="<?php echo get_template_directory_uri().'/library/images/ib_btn_export_inv.png'?>" alt="Export"/>
		<p>Export</p>
	</div>
	<div id="btn-export-pdf2" style="width:280px; float:right; padding-top:20px; padding-right:5px;">
		<img src="<?php echo get_template_directory_uri().'/library/images/grab_your_iti.gif'?>"
			style="width: 100%" />
	</div>
	<div class="clear"></div>

	<?php if($has_alert): ?>
	<div id="iti-msg-div" class="clear" style="padding:20px;">
		<div class="alert-error"><?php 
			foreach($err_array as $err_type => $err_args){
				outputError($err_type, $err_args);
			} ?>
		</div>
	</div>
	<?php endif; ?>

	<div id="content" style="background-color:white; margin: 0;">

		<!-- Itinerary overview div -->
		<div id="iti-overview" class="wrap clearfix">
			<div class="text-center"><h1 class="text-lblack-caps">Itinerary Key highlights</h1></div>
			
				<div id="hl-map-container" class="threecol first">
					<div id="iti-maps">
						<img class="iti-overlay-map" src="<?php echo get_template_directory_uri().'/library/images/overlaymaps/Overlap-Map_base.png';?>" />
						<?php 
							foreach($reduced_prov_map as $pid => $pname){
								if(isset($prov_visited[$pid])){
									$prov_name_uri = str_replace(" ", "_", $pname);
									echo '<img class="iti-overlay-map" src="'.get_template_directory_uri().
										'/library/images/overlaymaps/Overlap-Map_'.$prov_name_uri.'.png" />';
								}
							}
						?>
					</div>
					&nbsp;
					<div id="hl-container-mobile">
						<div class="hl-div-mobile">
							<a class="iti-highlight-url-mobile" href="http://www.happyholidayplanner.com" target="_blank">
								<div class="iti-highlight-mobile"></div>
							</a>
							<p class="text-lblack-caps text-center" style="margin:10px 0 0 0">-</p>
							<div class="clear"></div>
						</div>
						<div class="hl-div-mobile">
							<a class="iti-highlight-url-mobile" href="http://www.happyholidayplanner.com" target="_blank">
								<div class="iti-highlight-mobile"></div>
							</a>
							<p class="text-lblack-caps text-center" style="margin:10px 0 0 0">-</p>
							<div class="clear"></div>
						</div>
						<div class="hl-div-mobile">
							<a class="iti-highlight-url-mobile" href="http://www.happyholidayplanner.com" target="_blank">
								<div class="iti-highlight-mobile"></div>
							</a>
							<p class="text-lblack-caps text-center" style="margin:10px 0 0 0">-</p>
							<div class="clear"></div>
						</div>
						<div class="hl-div-mobile">
							<a class="iti-highlight-url-mobile" href="http://www.happyholidayplanner.com" target="_blank">
								<div class="iti-highlight-mobile"></div>
							</a>
							<p class="text-lblack-caps text-center" style="margin:10px 0 0 0">-</p>
							<div class="clear"></div>
						</div>
					</div>
				</div>

				<!-- hl-container will be hidden in mobile view and hl-container-mobile will be displayed -->
				<div id="hl-container" class="eightcol clearfix">
					
					<div class="hl-div threecol first">
						<div class="hl-placename-box"><p class="text-lblack-caps text-center" style="margin:0">-</p></div>
						<a class="iti-highlight-url" href="http://www.happyholidayplanner.com" target="_blank">
							<div class="iti-highlight" /></div>
						</a>
					</div>
					<div class="hl-div threecol">
						<div class="hl-placename-box"><p class="text-lblack-caps text-center" style="margin:0">-</p></div>
						<a class="iti-highlight-url" href="http://www.happyholidayplanner.com" target="_blank">
							<div class="iti-highlight" /></div>
						</a>
					</div>
					<div class="hl-div threecol">
						<div class="hl-placename-box"><p class="text-lblack-caps text-center" style="margin:0">-</p></div>
						<a class="iti-highlight-url" href="http://www.happyholidayplanner.com" target="_blank">
							<div class="iti-highlight" /></div>
						</a>
					</div>
					<div class="hl-div threecol last">
						<div class="hl-placename-box"><p class="text-lblack-caps text-center" style="margin:0">-</p></div>
						<a class="iti-highlight-url" href="http://www.happyholidayplanner.com" target="_blank">
							<div class="iti-highlight" /></div>
						</a>
					</div>
				</div>

				<!-- hl-go-details will be hidden in mobile view  -->
				<div id="hl-go-details" class="onecol last">
					<p class="text-center iti-details-arrow"><img class="clickable" style="width:60px;" onclick="go_next_ib_page();" src="<?php echo get_template_directory_uri().'/library/images/ani-arrow-next.gif';?>"/></p>
					<p class="text-center" style="margin: 0;">DETAILS</p>
				</div>			
		
		</div>
		<!-- END Itinerary overview div -->
		

		<!-- Itinerary details div -->
		<div id="iti-details" class="wrap clearfix hidden">
			<div class="threecol first">
				<p class="text-center iti-overview-arrow">
					<img class="clickable iti-arrow-back" style="width:20px; margin-bottom:-10px;" onclick="go_ib_highlight_page();" src="<?php echo get_template_directory_uri().'/library/images/white-arrow-prev.png';?>"/> Overview
				</p>
				<!-- Iti trail display for desktop view -->
				<div id="iti-trail">
					<img class="iti-trail-track" src="<?php echo get_template_directory_uri().'/library/images/iti-trail/iti-trail-track.png';?>" />
					<?php
						//Each iti trail marker entry has structure: 
						//	e.g. { "prov_uri":"Taipei_City" , "start_day":"2", "end_day":"3" }
						//
						$iti_trail_markers = array();
						$day_num = 1;
						$prev_prov_id = 0;
						$curr_marker = array();
						foreach($iti_data['iti_days'] as $day_index=>$dayrow){
							$curr_day_data = json_decode($dayrow->day_itinerary, true);
							
							$is_first_loop = true;
							//Loop through to get provinces first
					        foreach($curr_day_data as $prov_data){ 
					            $curr_provid = intval($prov_data["province"]);

					            //if its a new province, create new marker array
					            if($prev_prov_id != $curr_provid){
					            	if($prev_prov_id != 0){
					            		//Before creation,
					            		//Set the end_day
					            		if($is_first_loop){
					            			$curr_marker["end_day"] = $day_num - 1;
					            		}
					            		else {
					            			$curr_marker["end_day"] = $day_num;	
					            		}
					            		//add curr_marker to iti_trail_markers array
					            		$iti_trail_markers[] = $curr_marker;
					            	}

					            	$curr_marker = array();
					            	$curr_marker["prov_uri"] = str_replace(" ", "_", $province_map[$curr_provid]);
					            	$curr_marker["prov_name"] = $province_map[$curr_provid];
					            	$curr_marker["start_day"] = $day_num;
					        	}

					        	$is_first_loop = false;
					        	$prev_prov_id = $curr_provid;
					        }

					        $day_num++;
					    }

					    if(isset($curr_marker["prov_uri"])){
					    	$curr_marker["end_day"] = $day_num - 1;
					    	//add curr_marker to iti_trail_markers array
					        $iti_trail_markers[] = $curr_marker;
					    }

					    //Compute the gap in between markers
					    $num_prov = count($iti_trail_markers);
					    $marker_gap = 0;
  						$curr_top = 20;
						if($num_prov > 1){
							$marker_gap = (460-20)/($num_prov-1);
						}
					    foreach($iti_trail_markers as $marker_data){
					    	echo '<img class="iti-trail-mark" src="'.get_template_directory_uri().
								'/library/images/iti-trail/iti-trail-mark-'.$marker_data["prov_uri"].'.png" '.
								'style="top:'.$curr_top.'px"'.'/>';

							if($marker_data["start_day"] != $marker_data["end_day"]){
								$day_display = "(Day ".$marker_data["start_day"]."-".$marker_data["end_day"].")";
							}
							else {
								$day_display = "(Day ".$marker_data["start_day"].")";
							}
							echo '<div class="iti-trail-label" style="top:'.($curr_top-8).'px">'.$marker_data["prov_name"].
								'<br>'.$day_display.'</div>';

							$curr_top += $marker_gap;
					    }
					?> 
				</div>
				<!-- END Iti trail display for desktop view -->

				<!-- Iti trail display for mobile view -->
				<div id="iti-trail-mobile">
					<img class="iti-trail-track-mobile" src="<?php echo get_template_directory_uri().'/library/images/iti-trail/iti-trail-track-h.png';?>" />
					<?php
					    foreach($iti_trail_markers as $marker_data){
					    	echo '<img class="iti-trail-mark-mobile" src="'.get_template_directory_uri().
								'/library/images/iti-trail/iti-trail-mark-'.$marker_data["prov_uri"].'.png" />';

							if($marker_data["start_day"] != $marker_data["end_day"]){
								$day_display = "(Day ".$marker_data["start_day"]."-".$marker_data["end_day"].")";
							}
							else {
								$day_display = "(Day ".$marker_data["start_day"].")";
							}
							echo '<div class="iti-trail-label-mobile">'.$marker_data["prov_name"].
								'<br>'.$day_display.'</div>';
					    }
					?> 
				</div>
				<!-- END Iti trail display for mobile view -->
			</div>

			<div class="eightcol clearfix">
				<div class="text-center"><h1 class="text-lblack-caps">Your itinerary details</h1></div>

				<?php outputIti($iti_data['iti_days'], $province_map); ?>
			</div>

			<div id="iti-navi" class="onecol last">
				<div style="padding-bottom:40px">&nbsp;</div>
				<div id="iti-next-buttondiv">
					<p class="text-center iti-next-arrow"><img class="clickable" onclick="go_next_ib_page();" src="<?php echo get_template_directory_uri().'/library/images/white-arrow-next.png';?>"/></p>
					<!--<p class="text-center" style="margin: 0;">NEXT DAY</p>-->
				</div>

				<div id="iti-prev-buttondiv">
					<p class="text-center iti-prev-arrow"><img class="clickable" onclick="go_prev_ib_page();" src="<?php echo get_template_directory_uri().'/library/images/white-arrow-prev.png';?>"/></p>
					<!--<p class="text-center" style="margin: 0;">PREV DAY</p>-->
				</div>
			</div>			
		</div>
		<!-- END Itinerary details div -->


		<!-- Swap place dialog div -->
		<div id="swap-place-dialog" title="Choose another place">
		  <p class="swap-dialog-title">Swap <span id="swap-from-placename">[From Place]</span> with:</p>

		  <p id="swapchoice-loading" class="text-center"><img alt="Loading..." 
		  	src="<?php echo get_template_directory_uri(); ?>/library/images/round-loader.gif"/>
		  </p>

		  <div class="swapchoice-container">
		  	<p class="swapchoice-title">Place Title</p>
		  	<div class="swapchoice-img-div">
		  		<img class="swapchoice-thumburl" src=""/>
		  	</div>
		  	<div class="float-right">
		  		<div id="scbtn_swap0" class="blue-pill-button swap-dialog-btn1">Swap</div>
		  		<a id="scbtn_detail0" class="no-decoration" target="_blank" href="#">
		  			<div class="blue-pill-button swap-dialog-btn1">Detail</div>
		  		</a>
		  	</div>

		  	<p class="clear swapchoice-excerpt">Excerpt</p>
		  	<input type="hidden" class="swapchoice-place-id" value="" />
		  	<input type="hidden" class="swapchoice-prov-name" value="" />
		  	<input type="hidden" class="swapchoice-prov-class" value="" />
		  </div>

		  <div class="swapchoice-container">
		  	<p class="swapchoice-title">Place Title</p>
		  	<div class="swapchoice-img-div">
		  		<img class="swapchoice-thumburl" src=""/>
		  	</div>
		  	<div class="float-right">
		  		<div id="scbtn_swap1" class="blue-pill-button swap-dialog-btn1">Swap</div>
		  		<a id="scbtn_detail1" class="no-decoration" target="_blank" href="#">
		  			<div class="blue-pill-button swap-dialog-btn1">Detail</div>
		  		</a>
		  	</div>

		  	<p class="clear swapchoice-excerpt">Excerpt</p>
		  	<input type="hidden" class="swapchoice-place-id" value="" />
		  	<input type="hidden" class="swapchoice-prov-name" value="" />
		  	<input type="hidden" class="swapchoice-prov-class" value="" />
		  </div>

		  <div class="swapchoice-container">
		  	<p class="swapchoice-title">Place Title</p>
		  	<div class="swapchoice-img-div">
		  		<img class="swapchoice-thumburl" src=""/>
		  	</div>
		  	<div class="float-right">
		  		<div id="scbtn_swap2" class="blue-pill-button swap-dialog-btn1">Swap</div>
		  		<a id="scbtn_detail2" class="no-decoration" target="_blank" href="#">
		  			<div class="blue-pill-button swap-dialog-btn1">Detail</div>
		  		</a>
		  	</div>

		  	<p class="clear swapchoice-excerpt">Excerpt</p>
		  	<input type="hidden" class="swapchoice-place-id" value="" />
		  	<input type="hidden" class="swapchoice-prov-name" value="" />
		  	<input type="hidden" class="swapchoice-prov-class" value="" />
		  </div>
		</div>
		<!-- END Swap place dialog div -->


		<!-- Send Email dialog div -->
		<div id="email-iti-dialog" title="Email your Itinerary">
			<p>Email your itinerary to us! You will get a copy too!</p>
			<p><input id="email-iti-addr" placeholder="Email address"/></p>
			<p>Got a message for us?</p>
			<textarea id="email-iti-msg" rows="4" placeholder="Enter message" style="width:100%;"></textarea>

			<!--<button id="export-iti-getpdf">PDF</button>-->
			
			<p id="email-iti-loader" class="text-center hidden">
				<img src="<?php echo get_template_directory_uri(); ?>/library/images/rect-loader.gif"/>
			</p>
			<div id="email-iti-error" class="alert-error hidden">Error</div>
			<div id="email-iti-success" class="alert-success hidden">Email has been sent successfully!</div>

			<p class="text-center"><button id="export-iti-getpdf" class="hidden">Download PDF version</button></p>
		</div>
	</div>


	<!-- Definitions for Mobile popup panels -->
	<div id="sft-selpage-panel" class="sft-panel boxed sft-p-hidden">
		<!--<p id="sftwidth"></p>-->
		<?php for($day_num=1; $day_num<=$num_days; $day_num++): ?>
			<div class="btn_goto_itiday" onclick="show_ib_page_mobile(<?=$day_num?>);">
				<p>Day<br><?=$day_num?></p>
			</div>		
		<?php endfor; ?>
	</div>

	<div id="sft-export-panel" class="sft-panel boxed sft-p-hidden">
		<p>Export function here</p>
		<p>.. ... .. ...</p>
	</div>
<?php get_footer("itibuilder"); ?>
