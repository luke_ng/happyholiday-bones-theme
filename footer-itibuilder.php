			<footer class="footer" role="contentinfo" >

				<div id="inner-footer" class="wrap clearfix">
					<?php if(!is_page("Itinerary Builder")): ?>
					<nav role="navigation">
						<?php bones_footer_links(); ?>
					</nav>
					<?php endif ?>

					<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>.</p>

				</div>

			</footer>

		</div>


		<div id="sticky-footer-toolbar">
			<div class="footer-pagination">
				<div id="ib-pagebtn-prev" class="ib-pagebtn ib-sides"></div>
				<div id="ib-pagebtn-edit" class="ib-pagebtn ib-mid"></div>
				<div id="ib-pagebtn-sel" class="ib-pagebtn ib-mid"></div>
				<div id="ib-pagebtn-export" class="ib-pagebtn ib-mid"></div>
				<div id="ib-pagebtn-next" class="ib-pagebtn ib-sides"></div>
			</div>
		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		
	</body>

</html>
