<?php
/*
Template Name: Destinations Page Layout 
*/
?>

<?php get_header(); ?>

			<div id="content">

				<div id="inner-content" class="wrap clearfix">

					<div class="ninecol first clearfix" role="main">
						<div class="first fourcol">
							<div class="search-taiwan clearfix">
								<p>Search Taiwan</p>
								<img class="alignright" src="<?php echo get_template_directory_uri(); ?>/library/images/search-taiwan-icon.png" />
							</div>
					        <div class="boxed searchbox">
					          <?php get_search_form(); ?>
						      <br class="clear"/>
						    </div>
						</div>
						
						<div class="last eightcol boxed results-box">
							<div id="search-results">
								<?php if (have_posts()) : while (have_posts()) : the_post();
								the_content();
								endwhile; endif; ?>
							</div> 
						</div>

					</div>

					<?php get_sidebar(); ?>

				</div>

			</div>


<script type='text/javascript'>
	//Another copy of this code in search.php
	jQuery( document ).ready(function() {

	  	jQuery( "#searchform" ).on('submit', function( event ) {
	  		show_loader();
			event.preventDefault();
			var s_params = jQuery( this ).serialize();
			jQuery.get( "<?php echo get_template_directory_uri(); ?>/search-pre.php", s_params )
			.done( function( data ) {
				//returns the processed parameters in 'data'
				window.location.href = '<?php echo home_url("/"); ?>' + data;
			});
			//execute_search(jQuery( this ).serialize());
		  	//jQuery("#search-results").empty().append( jQuery( this ).serialize() );
		});

		jQuery( "#sel_all" ).click(function(){
			jQuery('.search-cat').prop("checked", true);
		});

		jQuery( "#sel_none" ).click(function(){
			jQuery('.search-cat').prop("checked", false);
		});
		
		//jQuery( "#adv-search" ).click(function(){
		//	jQuery("#cat-filters").slideToggle();
		//});
		//jQuery( "#cat-filters" ).hide();
	} );


	function show_loader() {
		jQuery("#search-results").empty()
		.append('<div class="loader"><img src="' + 
			'<?php echo get_template_directory_uri(); ?>/library/images/round-loader.gif" /><p>Searching</p></div>')
		.fadeIn('slow');
	}
</script>    
<?php get_footer(); ?>



<!--

	<form id="search-place" class="clearfix" >
					            <label for="place_name">Search for:</label>
					            <input id="place-name" name="place-name" type="text" value="" class="search-short text-field" placeholder="Place name">
					            
						        <h4 class="red-h">City/Township</h4>
						        <select id="location" name="location">
						          <option value="all">All</option>
						          	<?php 
										$terms = get_terms("Location", 'orderby=count&hide_empty=1&order=DESC');
										$count = count($terms);
										if ( $count > 0 ){
										    foreach ( $terms as $term ) {
										      echo "<option value='" . $term->slug . "'>" . $term->name . "</option>";
										    }
										}
									?>
						        </select>
					          
						        <br><br><br>
						        <h4 class="blue-h">Categories</h4>

					            <input type="checkbox" name="category[]" value="all">All<br>
					            <input type="checkbox" name="category[]" value="arts-culture">Arts & Culture<br>
					            <input type="checkbox" name="category[]" value="nature">Nature<br>
					            <input type="checkbox" name="category[]" value="night-life">Night Life<br>
					            <input type="checkbox" name="category[]" value="restaurants-eating-places">Restaurants/Eating Places<br>
					            <input type="checkbox" name="category[]" value="shopping">Shopping<br>
					            <input type="checkbox" name="category[]" value="spas">Spas<br>
					            <input type="checkbox" name="category[]" value="sports-recreation">Sports & Recreation<br>
					            <input type="checkbox" name="category[]" value="theme-park">Theme Park<br>
					            <input type="checkbox" name="category[]" value="tourist-attraction">Tourist Attraction<br>
					            <input type="checkbox" name="category[]" value="wedding">Wedding
					            <br><br>
					            <input type="submit" value="Search" class="alignright">
						      </form>						

-->
