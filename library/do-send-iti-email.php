<?php
// Load WP components, no themes.
define('WP_USE_THEMES', false);
require('../../../../wp-load.php');

$headers = array(
	'From: Darius <darius@happyholidayplanner.com>',
	'Content-Type: text/html; charset=UTF-8'
);

$recipients = array('darius@happyholidayplanner.com');
if(isset($_POST['user_email']) && !empty($_POST['user_email'])){
	$recipients[] = $_POST['user_email'];
}
else {
	echo "Email address is required, please try again";
	exit;
}

$subj = 'Your Taiwan Itinerary from Happy Holiday Planner';
$body = 'Greetings,<br><br>Thank you for creating your itinerary with Happy Holiday Planner!<br>';


if(isset($_POST['iti_string']) && !empty($_POST['iti_string'])){
	$body .= urldecode($_POST['iti_string']);
}
else {
	$body .= '<br>No itinerary data<br>';
}

//Append additional message if any
if(isset($_POST['extra_msg']) && !empty($_POST['extra_msg'])){
	//nl2br replaces newline chars with <br>
	$body .= '<br><br><br>Addtional Message:<br>'. nl2br($_POST['extra_msg']);
}

wp_mail( $recipients, $subj, $body, $headers );

echo "0";
exit;

?>