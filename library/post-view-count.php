<?php
 //Reference 
 // http://www.wpbeginner.com/wp-tutorials/how-to-track-popular-posts-by-views-in-wordpress-without-a-plugin/


function hhp_set_post_views($postID) {
    $count_key = 'hhp_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);


// add the tracker in the header by using wp_head hook
function hhp_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    hhp_set_post_views($post_id);
}
add_action( 'wp_head', 'hhp_track_post_views');


//Function to return the post count 
function hhp_get_post_views($postID){
    $count_key = 'hhp_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


//Show this column in the admin view
add_filter('manage_posts_columns', 'my_columns');
function my_columns($columns) {
    $columns['post_views'] = 'Views';
    return $columns;
}

add_action( 'manage_posts_custom_column' , 'custom_columns', 10, 2 );

function custom_columns( $column, $post_id ) {

    switch ( $column ) {

    case 'post_views' :
        echo hhp_get_post_views( $post_id ); 
        break;
    }
}

?>