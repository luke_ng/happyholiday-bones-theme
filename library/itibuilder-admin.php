<?php

// Admin Dashboard Widget for Itinerary builder
//

add_action( 'admin_enqueue_scripts', 'hhp_itibuilder_admin_scripts' );
// Hook for adding admin menus
add_action('admin_menu', 'hhp_itibuilder_menus');


function hhp_itibuilder_admin_scripts() {
	wp_enqueue_script( 'hhp_itibuilder_admin_js' );
	wp_enqueue_style( 'hhp_itibuilder_admin_css' );

    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-autocomplete' );
    wp_enqueue_script( 'jquery-ui-widget' );       
    wp_enqueue_style( 'hhp_jquery_ui_css' );
}

function hhp_itibuilder_menus() {

	// Add a new top-level menu:
    add_menu_page('Itinerary Builder', 'Itinerary Builder', 'manage_options', 
    	'itibuilder-top-level-handle', 'itibuilder_toplevel_page', null, 3 );
	// adding the custom sub-menu 
	// add_submenu_page( $parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
	add_submenu_page('itibuilder-top-level-handle','Province Data', 'Province Data',
		'manage_options', 'itibuilder-province', 'itibuilder_province_page');
	add_submenu_page('itibuilder-top-level-handle','Itinerary Day View', 'Itinerary Day View',
		'manage_options', 'itibuilder-day-view', 'itibuilder_day_view_page');
	add_submenu_page('itibuilder-top-level-handle','Itinerary Day Create or Update', 'Itinerary Day Create or Update',
		'manage_options', 'itibuilder-day-edit', 'itibuilder_day_edit_page');
}


function itibuilder_toplevel_page() {
   ?>
	<h1>Itinerary Builder Module</h1>

    <?php if(isset($_GET["action"])) : ?>
        <?php if($_GET["action"]=="util_updatetraits") : ?>
        <div class="updated"><p>Recomputing traits for itinerary days completed. <?=$_GET["updated"]?> rows updated.</p></div>
        <?php endif ?>

        <?php if($_GET["action"]=="util_distancetable") : ?>
        <div class="updated"><p>Calculating distance table completed. <?=$_GET["updated"]?> rows updated.</p></div>
        <?php endif ?>
    <?php endif ?>

    <h3>Utility functions</h3>

    <form method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
        <input type="hidden" name="action" value="util_updatetraits" />
        <p>Recompute the traits (shopping/sightsee/fun and relax) for all the itinerary days</p>
        <input type="submit" class="button button-primary" value="Recompute" />
    </form>

    <br>
    <form method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
        <input type="hidden" name="action" value="util_distancetable" />
        <p>Recompute the distance table for all places</p>
        <input type="submit" class="button button-primary" value="Recompute" />
    </form>
    
	<?php
}


add_action( 'admin_action_util_updatetraits', 'hhp_action_updatetraits' );
function hhp_action_updatetraits() {

    global $wpdb;
    $day_list = $wpdb->get_results( "SELECT * FROM samp_iti_day");

    $rows_updated = 0;

    foreach ($day_list as $day_row) {
        $day_data = json_decode($day_row->day_itinerary, true);
        $total_shopping = 0;
        $total_sightsee = 0;
        $total_funrelax = 0;
        $total_num_places = 0;
        foreach($day_data as $prov_entry){
            foreach($prov_entry["places"] as $place_entry){
                $total_shopping += intval( get_post_meta( $place_entry["id"],'place-trait-shopping', true ) );
                $total_sightsee += intval( get_post_meta( $place_entry["id"],'place-trait-sightsee', true ) );
                $total_funrelax += intval( get_post_meta( $place_entry["id"],'place-trait-funrelax', true ) );
                $total_num_places += 1;
            }
        }

        $curr_shopping = (int)($total_shopping/$day_row->num_places);
        $curr_sightsee = (int)($total_sightsee/$day_row->num_places);
        $curr_funrelax = (int)($total_funrelax/$day_row->num_places);

        if($curr_shopping != intval($day_row->day_trait_shopping) ||
            $curr_sightsee != intval($day_row->day_trait_sightsee) ||
            $curr_funrelax != intval($day_row->day_trait_funrelax)) {
            //If there are any differences, do an update

            //error_log("About to update day_id=".$day_row->day_id.
            //    "\n curr_shopping=".$curr_shopping."  DB value=".intval($day_row->day_trait_shopping).
            //    "\n curr_sightsee=".$curr_sightsee."  DB value=".intval($day_row->day_trait_sightsee).
            //    "\n curr_funrelax=".$curr_funrelax."  DB value=".intval($day_row->day_trait_funrelax) );

            $wpdb->update( 'samp_iti_day', 
                array( 
                    'day_trait_shopping' => (int)($total_shopping/$total_num_places),
                    'day_trait_sightsee' => (int)($total_sightsee/$total_num_places),
                    'day_trait_funrelax' => (int)($total_funrelax/$total_num_places)
                ), 
                array('day_id' => $day_row->day_id),
                array( 
                    '%d','%d','%d'
                ),
                array('%d')
            );

            $rows_updated += 1;
        }
    }

    $redirect_url = admin_url( 'admin.php?page=itibuilder-top-level-handle' )
        ."&action=util_updatetraits&updated=".$rows_updated;
    wp_redirect( $redirect_url );
    exit();
}


add_action( 'admin_action_util_distancetable', 'hhp_action_distancetable' );
function hhp_action_distancetable() {
    //In DB, coodinates are in decimal degrees, LAT,LONG e.g. 24.83525,121.526834
    //Meta column name is place-coordinates-[BRANCH-NUM], where [BRANCH_NUM] starts from 1
    global $wpdb;
    $limit_radius = 80000; //Don't store any entries that are beyond this distance
    $place_list = $wpdb->get_results( "SELECT * FROM wp_postmeta WHERE meta_key LIKE 'place-coordinates-%' ");
    //Example result in a row 
    // meta_id     22195  
    // post_id     1559
    // meta_key    place-coordinates-1
    // meta_value  25.136692,121.507149

    $rows_updated = 0;
    foreach($place_list as $place_from) {
        foreach($place_list as $place_to) {
            if($place_from->post_id !== $place_to->post_id) {
                //Compute distance only if the places are different
                $coord_from = explode(",", $place_from->meta_value);
                $coord_to = explode(",", $place_to->meta_value);
                $tok_from = explode("-", $place_from->meta_key);
                $branch_from = $tok_from[2];
                $tok_to = explode("-", $place_to->meta_key);
                $branch_to = $tok_to[2];
                $curr_dist = haversineGreatCircleDistance($coord_from[0], $coord_from[1],
                    $coord_to[0], $coord_to[1]);

                if($curr_dist < $limit_radius) {

                    //Try update first
                    $upd_result = $wpdb->update( 'samp_iti_dist_table', 
                        array( 
                            'dist_meter' => $curr_dist 
                        ), 
                        array(
                            'from_place_id' => $place_from->post_id,
                            'to_place_id' => $place_to->post_id, 
                            'from_place_branch' => $branch_from, 
                            'to_place_branch' => $branch_to
                        ),
                        array('%d'),
                        array( 
                            '%d',
                            '%d',
                            '%d',
                            '%d'
                        )
                    );

                    if($upd_result == 0){
                        $ins_result = $wpdb->insert( 
                            'samp_iti_dist_table', 
                            array( 
                                'from_place_id' => $place_from->post_id,
                                'to_place_id' => $place_to->post_id, 
                                'from_place_branch' => $branch_from, 
                                'to_place_branch' => $branch_to,
                                'dist_meter' => $curr_dist 
                            ), 
                            array( 
                                '%d',
                                '%d',
                                '%d',
                                '%d',
                                '%d' 
                            ) 
                        );

                        if($ins_result == 1){
                            $rows_updated += 1;
                        }
                    }
                    else {
                        $rows_updated += 1;
                    }
                }
            }
        }//end inner foreach
    }//end outer foreach

    $redirect_url = admin_url( 'admin.php?page=itibuilder-top-level-handle' )
        ."&action=util_distancetable&updated=".$rows_updated;
    wp_redirect( $redirect_url );
    exit();
}


/**
 * Calculates the great-circle distance between two points, with
 * the Haversine formula.
 * @param float $latitudeFrom Latitude of start point in [deg decimal]
 * @param float $longitudeFrom Longitude of start point in [deg decimal]
 * @param float $latitudeTo Latitude of target point in [deg decimal]
 * @param float $longitudeTo Longitude of target point in [deg decimal]
 * @param float $earthRadius Mean earth radius in [m]
 * @return float Distance between points in [m] (same as earthRadius)
 */
function haversineGreatCircleDistance(
  $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
{
  // convert from degrees to radians
  $latFrom = deg2rad($latitudeFrom);
  $lonFrom = deg2rad($longitudeFrom);
  $latTo = deg2rad($latitudeTo);
  $lonTo = deg2rad($longitudeTo);

  $latDelta = $latTo - $latFrom;
  $lonDelta = $lonTo - $lonFrom;

  $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
    cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
  return $angle * $earthRadius;
}


function itibuilder_province_page() {
	if(isset($_GET["province"])) { 
		//Read the province data from db and display in the fields
		global $wpdb;
    	$prov_data = $wpdb->get_row( "SELECT * FROM samp_iti_provinces WHERE prov_id = ".$_GET["province"] );

    	$prov_mindays = $prov_data->min_days;
    	$prov_maxdays = $prov_data->max_days;
    	$prov_recomdays = $prov_data->recom_days;
    	$prov_bufferdays = $prov_data->buffer_days;
    	$prov_trait_shopping = $prov_data->prov_trait_shopping;
    	$prov_trait_sightsee = $prov_data->prov_trait_sightsee;
    	$prov_nexthop_data = json_decode($prov_data->nexthop_prov, true);
        $prov_is_keyprov = $prov_data->is_keyprov;
	}

    if(isset($_GET["updated"])) : ?>
		<div class="updated"><p>Entry updated</p></div>
	<?php endif ?>

	
	<h3>View and modify province data</h3>
	<form id="prov_form" method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
    	<input type="hidden" name="action" value="upd_province" />
    	<label for="term_id">Province</label>
    	<select id="term_id" name="term_id">
    		<option value="0">Select province</option>
      	<?php 
			$terms = get_terms("Location", 'order=ASC');
			$count = count($terms);
			$sel_prov = (isset($_GET["province"]) ? $_GET["province"] : 0 );
			if ( $count > 0 ){
			    foreach ( $terms as $term ) {
			      echo "<option value='".$term->term_id."' ".($sel_prov==$term->term_id?'selected':'').">" 
			      	. $term->name . "</option>";
			    }
			}
		?>
        </select>
        <div>
        	<p><label for="min_days">Min Days</label>
        	<input id="min_days" name="min_days" type="text" 
        		<?php if(isset($prov_mindays)){ echo "value='".$prov_mindays."'"; }?> /></p>
        	<p><label for="max_days">Max Days</label>
        	<input id="max_days" name="max_days" type="text" 
        		<?php if(isset($prov_maxdays)){ echo "value='".$prov_maxdays."'"; }?> /></p>
        	<p><label for="recom_days">Recommended Days</label>
        	<input id="recom_days" name="recom_days" type="text"
        		<?php if(isset($prov_recomdays)){ echo "value='".$prov_recomdays."'"; }?> /></p>
        	<p><label for="buffer_days">Buffer Days</label>
        	<input id="buffer_days" name="buffer_days" type="text"
        		<?php if(isset($prov_bufferdays)){ echo "value='".$prov_bufferdays."'"; }?> /></p>

            <p><label>Is Key Province?</label>
            <input name="is_keyprov" type="checkbox" 
               <?php if(isset($prov_is_keyprov) && $prov_is_keyprov){ echo "checked"; }?> />
            </p>

        	<br>
            <h4>Province Characteristics</h4>
            <p>Shopping Score</p>
            <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="prov_trait_shopping" value="0"
                    <?php if($prov_trait_shopping==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="prov_trait_shopping" value="25"
                    <?php if($prov_trait_shopping==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="prov_trait_shopping" value="50"
                    <?php if($prov_trait_shopping==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="prov_trait_shopping" value="75"
                    <?php if($prov_trait_shopping==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="prov_trait_shopping" value="100"
                    <?php if($prov_trait_shopping==100){echo "checked";}?>>
                </td>
            </tr></table>
            
            <p>Sightseeing Score</p>
            <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="prov_trait_sightsee" value="0"
                    <?php if($prov_trait_sightsee==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="prov_trait_sightsee" value="25"
                    <?php if($prov_trait_sightsee==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="prov_trait_sightsee" value="50"
                    <?php if($prov_trait_sightsee==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="prov_trait_sightsee" value="75"
                    <?php if($prov_trait_sightsee==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="prov_trait_sightsee" value="100"
                    <?php if($prov_trait_sightsee==100){echo "checked";}?>>
                </td>
            </tr></table>
            
            <p>Fun and relax Score</p>
            <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="prov_trait_funrelax" value="0"
                    <?php if($prov_trait_funrelax==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="prov_trait_funrelax" value="25"
                    <?php if($prov_trait_funrelax==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="prov_trait_funrelax" value="50"
                    <?php if($prov_trait_funrelax==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="prov_trait_funrelax" value="75"
                    <?php if($prov_trait_funrelax==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="prov_trait_funrelax" value="100"
                    <?php if($prov_trait_funrelax==100){echo "checked";}?>>
                </td>
            </tr></table>
            

        	<br><br>
        	<h4>Next-hop Provinces</h4>
        	<div>
        		<?php for($loop=1; $loop<=12; $loop++) : ?>
        		<p><?=$loop?><select id="nexthop_id_<?=$loop?>" name="nexthop_id_<?=$loop?>">
        			<option value="0">Select province</option>
	      			<?php

	      			$curr_prov = 0;
	      			if(isset($prov_nexthop_data)){
	      				if($loop <= count($prov_nexthop_data)){
	      					$curr_prov = $prov_nexthop_data[$loop-1];
	      				}
	      			}
	      			
	      			if ( $count > 0 ){
				    	foreach ( $terms as $term ) {
					      echo "<option value='".$term->term_id."' "
					      	.($term->term_id==$curr_prov ? "selected" : "")
					      	.">" . $term->name . "</option>";
					    }
					}
	      			?>
	      			</select>
      			</p>
      			<?php endfor ?>
        	</div>
	
        </div>

        <div id="error_message" class="error" style="display:none"><p>Error message here</p></div>
        <br>
    	<input type="submit" class="button button-primary" value="Update" />
	</form>
	<?php
}


//POST handler (admin_action_ hook)
// http://wordpress.stackexchange.com/questions/10500/how-do-i-best-handle-custom-plugin-page-actions
add_action( 'admin_action_upd_province', 'hhp_province_action' );
function hhp_province_action()
{
    // Read the inputs and insert/update the province data
	$nexthop_ids = array();
	$loop = 1;
	while(isset($_POST["nexthop_id_".$loop])){
		if($_POST["nexthop_id_".$loop] != 0){
			$nexthop_ids[] = $_POST["nexthop_id_".$loop];
		}
		$loop++;
	}

	//Prepare $nexthop_data
	$nexthop_data = json_encode($nexthop_ids);

    $is_keyprov = false;
    if( isset($_POST["is_keyprov"]) ){
        $is_keyprov = true;
    }
	
    global $wpdb;
    $wpdb->update( 'samp_iti_provinces', 
    	array( 
			'min_days' => $_POST["min_days"], 
			'max_days' => $_POST["max_days"],
			'recom_days' => $_POST["recom_days"],
			'buffer_days' => $_POST["buffer_days"],
			'nexthop_prov' => $nexthop_data,
			'prov_trait_shopping' => $_POST["prov_trait_shopping"],
			'prov_trait_sightsee' => $_POST["prov_trait_sightsee"],
            'prov_trait_funrelax' => $_POST["prov_trait_funrelax"],
            'is_keyprov' => $is_keyprov
		), 
        array('prov_id' => $_POST["term_id"]),
		array( 
			'%d',
			'%d',
			'%d',
			'%d',
			'%s',
			'%d', 
			'%d',
            '%d'
		),
        array('%d')
	);

    $redirect_url = $_SERVER['HTTP_REFERER'];
    if (strlen(strstr($redirect_url, "province=")) == 0) {
        // need to append '&province=...'
        $redirect_url = $redirect_url . "&province=".$_POST["term_id"];
    }
    if (strlen(strstr($redirect_url, "updated=")) == 0) {
        // need to append '&updated=...'
        $redirect_url = $redirect_url . "&updated=true";
    }
    wp_redirect( $redirect_url );
    exit();
}




function itibuilder_day_view_page() {

    global $wpdb;

    if(isset($_GET["delete_id"])) {
        $del_result = $wpdb->delete( "samp_iti_day", array("day_id" => $_GET["delete_id"]), array( '%d' ) );
    } 

    $rows_per_page = 20;
    $page_num = (isset($_GET["paged"]) ? $_GET["paged"] : 1);
    if(!is_numeric($page_num)){
        $page_num = 1;
    }

    $limit_offset = ($page_num - 1) * $rows_per_page;

    //Count the total num rows
    $num_items = $wpdb->get_var( "SELECT COUNT(*) FROM samp_iti_day" );
    $last_page = ceil($num_items / $rows_per_page);

    if($page_num > $last_page){
        $page_num = 1;
    }

    //Get the data according to the page
    $day_rows = $wpdb->get_results( "SELECT * FROM samp_iti_day".
        " ORDER BY day_itinerary ASC".
        " LIMIT " .$limit_offset.",".$rows_per_page );

    //Build the province map (for easy search of province name using id)
    $gt_args = array(
        'orderby' => 'id', 
        'order'   => 'ASC',
    ); 
    $terms = get_terms("Location", $gt_args);
    $province_map = array();
    foreach($terms as $term){
        $province_map[intval($term->term_id)] = $term->name;
    }
?>
	<h3>Shows all Itinerary Day data in table</h3>
	
    <?php if(isset($_GET["delete_id"]) && $del_result!=false) : ?>
        <div class="updated"><p>Entry deleted</p></div>
    <?php endif ?>

    <form method="get"><div class='tablenav'>
        <div class='tablenav-pages'><span class="displaying-num"><?=$num_items?> items</span>
            <span class='pagination-links'>
                <a class='first-page <?php if($page_num==1){echo "disabled";}?>' title='Go to the first page' 
                    href='/wp-admin/admin.php?page=itibuilder-day-view'>&laquo;</a>
                <a class='prev-page <?php if($page_num==1){echo "disabled";}?>' title='Go to the previous page' 
                    href=<?php if($page_num>1){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".($page_num-1);}?> >&lsaquo;</a>
                
                    <input type="hidden" name="page" value="itibuilder-day-view" />
                    <span class="paging-input">
                        <label for="current-page-selector" class="screen-reader-text">Select Page</label>
                        <input class='current-page' id='current-page-selector' title='Current page' type='text' 
                            name='paged' value='<?=$page_num?>' size='1' /> of <span class='total-pages'><?=$last_page?></span>
                    </span>
                
                <a class='next-page <?php if($page_num==$last_page){echo "disabled";}?>' title='Go to the next page' 
                    href=<?php if($page_num<$last_page){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".($page_num+1);}?> >&rsaquo;</a>
                <a class='last-page <?php if($page_num==$last_page){echo "disabled";}?>' title='Go to the last page' 
                    href=<?php if($page_num<$last_page){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".$last_page;}?> >&raquo;</a>
            </span>
        </div>
    </div></form>

    <table class="wp-list-table widefat fixed">
    	<thead>
    		<tr>
    			<th style="width:70px">Day ID</th>
    			<th style="width:200px">Provinces Visited</th>
    			<th>Places Visited</th>
    			<th style="width:80px">Shopping %</th>
    			<th style="width:80px">Sightsee %</th>
    			<th style="width:100px">Action</th>
    		</tr>
    	</thead>
    	<tbody>
        <?php 
            $row_count = 0;
            foreach($day_rows as $dayrow) : ?>
        <?php
            $iti_data = json_decode($dayrow->day_itinerary, true);
            $provinces = "";
            $places = "";
            $first_prov = true;
            $first_place = true;
            $prev_provid = -1;
            
            foreach($iti_data as $prov_data){
                //Building provinces string
                $curr_provid = intval($prov_data["province"]);
                if($curr_provid != $prev_provid){
                    if(!$first_prov){
                        $provinces .= " -> ";
                    }
                    else {
                        $first_prov = false;
                    }
                    $provinces .= $province_map[$curr_provid];
                    $prev_provid = $curr_provid;
                }

                //Building places string
                
                $places_arr = $prov_data["places"];
                foreach ($places_arr as $place_d) {
                    if(!$first_place){
                        $places .= "<br>";
                    }
                    else {
                        $first_place = false;
                    }
                    $places .= $place_d["label"];
                }
            }

            $row_count++;
            if($row_count%2 == 0){
                echo "<tr>";
            }
            else {
                echo '<tr style="background-color:lightgrey">';   
            }
        ?>
    			<td><?=$dayrow->day_id?></td>
    			<td><?=$provinces?></td>
    			<td><?=$places?></td>
    			<td><?=$dayrow->day_trait_shopping?></td>
    			<td><?=$dayrow->day_trait_sightsee?></td>
    			<td>
                    <a href="/wp-admin/admin.php?page=itibuilder-day-edit&edit_id=<?=$dayrow->day_id?>" >View/Edit</a>
                    <p>&nbsp;</p>
                    <a href="/wp-admin/admin.php?page=itibuilder-day-view&delete_id=<?=$dayrow->day_id?>"
                        onClick="delete_day_iti(<?=$dayrow->day_id?>)" >Delete</a>
                </td>
    		</tr>
        <?php endforeach; ?>
    		
    	</tbody>
    </table>


    <form method="get"><div class='tablenav'>
        <div class='tablenav-pages'><span class="displaying-num"><?=$num_items?> items</span>
            <span class='pagination-links'>
                <a class='first-page <?php if($page_num==1){echo "disabled";}?>' title='Go to the first page' 
                    href='/wp-admin/admin.php?page=itibuilder-day-view'>&laquo;</a>
                <a class='prev-page <?php if($page_num==1){echo "disabled";}?>' title='Go to the previous page' 
                    href=<?php if($page_num>1){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".($page_num-1);}?> >&lsaquo;</a>
                
                    <input type="hidden" name="page" value="itibuilder-day-view" />
                    <span class="paging-input">
                        <label for="current-page-selector" class="screen-reader-text">Select Page</label>
                        <input class='current-page' id='current-page-selector' title='Current page' type='text' 
                            name='paged' value='<?=$page_num?>' size='1' /> of <span class='total-pages'><?=$last_page?></span>
                    </span>
                
                <a class='next-page <?php if($page_num==$last_page){echo "disabled";}?>' title='Go to the next page' 
                    href=<?php if($page_num<$last_page){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".($page_num+1);}?> >&rsaquo;</a>
                <a class='last-page <?php if($page_num==$last_page){echo "disabled";}?>' title='Go to the last page' 
                    href=<?php if($page_num<$last_page){echo "/wp-admin/admin.php?page=itibuilder-day-view&paged=".$last_page;}?> >&raquo;</a>
            </span>
        </div>
    </div></form>

    <?php
}


function itibuilder_day_edit_page() {
 
	if(isset($_GET["edit_id"])){
    	echo "<h3>View and modify Itinerary Day data</h3>";
        
        //Read the province data from db and display in the fields
        global $wpdb;
        $day_data = $wpdb->get_row( "SELECT * FROM samp_iti_day WHERE day_id = ".$_GET["edit_id"] );

        $itiday_place_data = $day_data->day_itinerary;
        $itiday_numplaces = $day_data->num_places;
        $itiday_numprov = $day_data->num_prov;
        $itiday_trait_shopping = $day_data->day_trait_shopping;
        $itiday_trait_sightsee = $day_data->day_trait_sightsee;
        $itiday_trait_funrelax = $day_data->day_trait_funrelax;
        $itiday_need_driver = $day_data->need_driver;
        $id_day_start_place = $day_data->start_place_id;
        $id_day_end_place = $day_data->end_place_id;
        
    }
    else {
    	echo "<h3>Create new Itinerary Day data</h3>";
	}
    
    if(isset($_GET["updated"])) : ?>
        <div class="updated"><p>Entry updated</p></div>
    <?php endif ?>

    <input id="places_lookup_url" type="hidden" value="<?php echo admin_url('admin-ajax.php').'?action=places_lookup'; ?>" />
    <input id="places_data" type="hidden" value="<?php echo htmlspecialchars($itiday_place_data); ?>" />

	<p><button id="btn_add_prov" class="button button-primary" type="button" onclick="addProv()">Add Province</button></p>

	<form id="admin_day_form" method="POST" action="<?php echo admin_url( 'admin.php' ); ?>">
    	<input type="hidden" name="action" value="upd_day" />
        <?php if(isset($_GET["edit_id"])): ?>
            <input type="hidden" name="edit_id" value='<?=$_GET["edit_id"]?>' />
        <?php endif ?>
        <input type="hidden" name="admin_itibuilder_numprov" />
    	
    	<div id="admin_day_container">
            <p>Total number of provinces: <span id="num_prov">9999</span></p>

    		<div class="admin_itibuilder_prov">
                <input class="admin_itibuilder_numplaces" type="hidden"/>
    			<button type="button" class="admin_itibuilder_btndelprov">Delete Province</button>
    			<h3 class="prov_meta">[Start province] [End Province]</h3>
    			
    			<label>Province</label>
    			<select class="admin_itibuilder_provsel" name="prov_id_0">
		    		<option value="0">Select province</option>
		      	<?php 
					$terms = get_terms("Location", 'order=ASC');
					$count = count($terms);
					if ( $count > 0 ){
					    foreach ( $terms as $term ) {
					      echo "<option value='".$term->term_id."'>" 
					      	. $term->name . "</option>";
					    }
					}
				?>
		        </select>

                <p>
                    <label>% time spent in province</label>
                    <input class="admin_prov_timespent" name="prov_timespent_0" type="text" />
                </p>

		        <p><button class="admin_itibuilder_btnaddplace" type="button" onclick="addPlace(0)">Add place</button></p>
    		</div>

    	</div>

        <br>
        <h4>Start and end places</h4>
        <p>
            <label>Start Place</label>
            <input class="admin_itibuilder_placeinput" name="day_start_place" type="text" size="60"
            <?php if(isset($id_day_start_place)){ 
                echo 'value="'.get_the_title($id_day_start_place).'" style="color:darkblue; font-weight:bold;"';  
            } ?>
            />
            <input name="id_day_start_place" type="hidden"
            <?php if(isset($id_day_start_place)){ echo 'value="'.$id_day_start_place.'"'; } ?>
            />
        </p>
        <p>
            <label>End Place</label>
            <input class="admin_itibuilder_placeinput" name="day_end_place" type="text" size="60"
            <?php if(isset($id_day_end_place)){ 
                echo 'value="'.get_the_title($id_day_end_place).'" style="color:darkblue; font-weight:bold;"';  
            } ?>
            />
            <input name="id_day_end_place" type="hidden"
            <?php if(isset($id_day_end_place)){ echo 'value="'.$id_day_end_place.'"'; } ?>
            />
        </p>

        <br>
		<h4>Shopping / Sightseeing / Fun and relax %<br>(Values will auto update after submission)</h4>
    	<p>
    		<label>Shopping %</label>
    		<input id="day_trait_shopping" name="day_trait_shopping" type="text"
                <?php if(isset($itiday_trait_shopping)){ echo "value='".$itiday_trait_shopping."'"; }?>
            disabled>
    	</p>
    	<p>
    		<label>Sightseeing %</label>
    		<input id="day_trait_sightsee" name="day_trait_sightsee" type="text"
                <?php if(isset($itiday_trait_sightsee)){ echo "value='".$itiday_trait_sightsee."'"; }?>
            disabled>
    	</p>
        <p>
            <label>Fun and Relax %</label>
            <input id="day_trait_funrelax" name="day_trait_funrelax" type="text"
                <?php if(isset($itiday_trait_funrelax)){ echo "value='".$itiday_trait_funrelax."'"; }?>
            disabled>
        </p>
        <br><br>
        <p>
            <label>Need Driver </label>
            <input name="need_driver" type="checkbox" 
               <?php if(isset($itiday_need_driver) && $itiday_need_driver){ echo "checked"; }?> />
        </p>

    	<br>
        <div id="error_message" class="error" style="display:none"><p>Error message here</p></div>
        <br>
    	<input type="submit" class="button button-primary" value="Update" />
    </form>
    <?php
} 

function itibuilder_jsonRemoveUnicodeSequences($struct) {
   return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", 
        json_encode($struct));
}


add_action( 'admin_action_upd_day', 'hhp_day_update' );

function hhp_day_update()
{
	//First form the data structure to store day_itinerary 
    //check how many provinces, to form the outer loop
    $iti_numprov = intval($_POST['admin_itibuilder_numprov']);
    $start_prov = 0;
    $end_prov = 0;
    
    $iti_array = array();
    $total_num_places = 0;
    $total_shopping = 0;
    $total_sightsee = 0;
    $total_funrelax = 0;
    
    for($prov_index=0; $prov_index<$iti_numprov; $prov_index++){
        if ( isset( $_POST['admin_itibuilder_numplaces_'.$prov_index] ) && $_POST['admin_itibuilder_numplaces_'.$prov_index] != '' ) {
            
            $num_places = $_POST['admin_itibuilder_numplaces_'.$prov_index];
            //Loop through all places of the province
            $places_arr = array();
            for($place_index=0; $place_index<$num_places; $place_index++){
                $place_id = $_POST['id_placeinput_'.$prov_index.'_'.$place_index]; 
                $place_label = $_POST['placeinput_'.$prov_index.'_'.$place_index];
                //remove '\' in the place label if any
                $place_label = stripcslashes($place_label);
                if(isset($place_id, $place_label) && $place_id != '' && $place_label != ''){
                    $place_data = array('id' => $place_id, 
                        'label' => $place_label);
                    //Append the place_data to the day_of_places array
                    $places_arr[] = $place_data;
                    //Update the total_num_places
                    $total_num_places += 1;

                    //Collate the shopping/sightsee/fun relax %
                    $total_shopping += intval( get_post_meta( $place_id, 'place-trait-shopping', true ) );
                    $total_sightsee += intval( get_post_meta( $place_id, 'place-trait-sightsee', true ) );
                    $total_funrelax += intval( get_post_meta( $place_id, 'place-trait-funrelax', true ) );
                }
            }

            $prov_obj = array('province' => $_POST['prov_id_'.$prov_index],
                'places' => $places_arr,
                'timespent' => $_POST['prov_timespent_'.$prov_index]
                );

            //retrieve the start_prov
            if($prov_index==0){
                $start_prov = intval($_POST['prov_id_'.$prov_index]);
            }
            //retrieve the end_prov
            if($prov_index == ($iti_numprov-1)){
                $end_prov = intval($_POST['prov_id_'.$prov_index]);
            }

            //Append the province data to the iti_array
            $iti_array[] = $prov_obj;
        }
    }
    $day_itinerary = json_encode($iti_array);
    //$day_itinerary = itibuilder_jsonRemoveUnicodeSequences($iti_array);

    $need_driver = false;
    if( isset($_POST["need_driver"]) ){
        $need_driver = true;
    }

    $start_place_id = $_POST["id_day_start_place"];
    $end_place_id = $_POST["id_day_end_place"];


    global $wpdb;
    $entry_pk = 0;

    if( isset($_POST['edit_id']) && $_POST['edit_id'] != '' ){
        $entry_pk = $_POST["edit_id"];

        $wpdb->update( 'samp_iti_day', 
            array( 
                'day_itinerary' => $day_itinerary,
                'num_places' => $total_num_places,
                'num_prov' => $iti_numprov,
                'day_trait_shopping' => (int)($total_shopping/$total_num_places),
                'day_trait_sightsee' => (int)($total_sightsee/$total_num_places),
                'day_trait_funrelax' => (int)($total_funrelax/$total_num_places),
                'need_driver' => $need_driver,
                'start_prov' => $start_prov,
                'end_prov' => $end_prov,
                'start_place_id' => $start_place_id,
                'end_place_id' => $end_place_id
            ), 
            array('day_id' => $entry_pk),
            array(
                '%s','%d','%d','%d','%d',
                '%d','%d','%d','%d','%d',
                '%d' 
            ),
            array("%d")
        );
    }
    else {
        $wpdb->insert( 'samp_iti_day', 
            array( 
                'day_itinerary' => $day_itinerary,
                'num_places' => $total_num_places,
                'num_prov' => $iti_numprov,
                'day_trait_shopping' => (int)($total_shopping/$total_num_places),
                'day_trait_sightsee' => (int)($total_sightsee/$total_num_places),
                'day_trait_funrelax' => (int)($total_funrelax/$total_num_places),
                'need_driver' => $need_driver,
                'start_prov' => $start_prov,
                'end_prov' => $end_prov,
                'start_place_id' => $start_place_id,
                'end_place_id' => $end_place_id
            ), 
            array( 
                '%s','%d','%d','%d','%d',
                '%d','%d','%d','%d','%d',
                '%d'
            )
        );

        $entry_pk = $wpdb->insert_id;
    }

    $redirect_url = $_SERVER['HTTP_REFERER'];
    if (strlen(strstr($redirect_url, "edit_id=")) == 0) {
        // need to append '&edit_id=...'
        $redirect_url = $redirect_url . "&edit_id=".$entry_pk;
    }
    if (strlen(strstr($redirect_url, "updated=")) == 0) {
        // need to append '&updated=...'
        $redirect_url = $redirect_url . "&updated=true";
    }
    wp_redirect( $redirect_url );
    exit();
}



// Create AJAX handler to retrieve posts
add_action('wp_ajax_places_lookup', 'places_lookup');

function places_lookup() {
    
    //The query is in the 'term' parameter of the GET request
    $input_query = $_GET['term'];
    //echo '[{"id":"the_id", "value":"the_value", "custom":"' . $input_query .
    //'"}, {"id":"the_id2", "value":"the_value2", "custom":"' . $input_query . '"}]';

    global $wpdb;

    $search = $wpdb->esc_like($input_query);
    $search = '%'.$search.'%';

    $query = 'SELECT ID,post_title FROM ' . $wpdb->posts . '
        WHERE post_title LIKE %s
        AND post_type = \'places\'
        AND post_status = \'publish\'
        ORDER BY post_title ASC';

    $query = $wpdb->prepare($query,$search);

    //For jQuery Autocomplete
    //The label property is displayed in the suggestion menu. 
    //The value will be inserted into the input element when a user selects an item. 
    //If just one property is specified, it will be used for both, e.g., if you provide 
    //only value properties, the value will also be used as the label
    $return_arr = array();
    foreach ($wpdb->get_results($query) as $row) {
        $post_title = $row->post_title;
        $id = $row->ID;

        //$meta = get_post_meta($id, 'YOUR_METANAME', TRUE);
        $row_packed = array('id' => $row->ID, 'value' => $row->post_title);
        $return_arr[] = $row_packed;
    }

    echo json_encode($return_arr);
    die();
}


?>