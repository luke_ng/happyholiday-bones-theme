<?php
/*
Author: Luke Ng

This is where the login customizations are done.
*/

/*************************************************************
 * Disable admin bar on the frontend of your website
 * for subscribers.
 *************************************************************/
function happyhol_disable_admin_bar() { 
	if( ! current_user_can('edit_posts') )
		add_filter('show_admin_bar', '__return_false');	
}
add_action( 'after_setup_theme', 'happyhol_disable_admin_bar' );
 

/*************************************************************
 * Swapping out the default wordpress logo for our own
 *************************************************************/
/*
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri(); ?>/library/images/hhp_logo2_small.png);
            padding-bottom: 5px;
            background-size: auto;
            height: 140px;
            width: 140px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
*/

/*************************************************************
 * Adding additional scripts
 *************************************************************/

function bones_login_css() {
    wp_enqueue_style( 'bones_login_css', get_template_directory_uri() . '/library/css/login.css', false );
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_script( 'hhp-login-js', 
        get_stylesheet_directory_uri() . '/library/js/hhp-login.js',
        array( 'jquery' ));

    wp_enqueue_style( 'hhp_jquery_ui_css', get_stylesheet_directory_uri() . '/library/css/jquery-ui-1.10.4.custom.min.css');
}

add_action( 'login_enqueue_scripts', 'bones_login_css', 10 );


/*************************************************************
 * Swapping out the default wordpress logo for our own
 *************************************************************/
function hhp_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'hhp_login_logo_url' );


function hhp_login_logo_url_title() {
    return 'Happy Holiday Planner';
}
add_filter( 'login_headertitle', 'hhp_login_logo_url_title' );


/*
function hhp_add_social_logins() {
?>
    <div id="fb_login_form">
        <div id="fb-root"></div>
        <p><fb:login-button scope="basic_info,email,user_birthday" show-faces="false"
            onlogin="gotoSignon();" data-size="large" width="200" max-rows="1"></fb:login-button> with Facebook</p>
    </div>
<?php
}
add_action( 'login_form', 'hhp_add_social_logins' );
*/

function hhp_login_redirect() {
    global $redirect_to; 
    if (!isset($_GET['redirect_to'])) { 
        $redirect_to = get_option('siteurl');
    }
}

add_action( 'login_form', 'hhp_login_redirect' );


function hhp_extra_err_handling($errors, $redirect_to) {
    if( isset($_GET['retry_signon']) && 'fb' == $_GET['retry_signon'] ) {
        $errors->add('fbSignonRetry', 'We were unable to connect to Facebook. Please try again.');
    }
    return $errors;
}
add_filter('wp_login_errors', 'hhp_extra_err_handling', 10, 2);

function hhp_pre_social($message) {
    $social = '<div id="fb_login_form">
        <div id="fb-root"></div>
        <p>Register or Login with Facebook <fb:login-button scope="basic_info,email,user_birthday" show-faces="false"
            onlogin="gotoSignon();" data-size="large" width="200" max-rows="1"></fb:login-button></p>
    </div><div id="logintext_or">OR</div>';
    return $message.$social;
}
add_filter('login_message', 'hhp_pre_social');

/*************************************************************
 * Adding on to the default registration
 *************************************************************/

//Additionally we need the First name, last name, and birthdate

function hhp_add_regis_fields() 
{
    $user_firstname = $_POST['first_name'];
    $user_lastname = $_POST['last_name'];
    $user_dob = $_POST['user_dob'];
    
?>
    <p>
        <label for="first_name">First name<br />
        <input type="text" name="first_name" id="first_name" class="input" value="<?php echo esc_attr(wp_unslash($user_firstname)); ?>" size="20" /></label>
    </p>
    <p>
        <label for="last_name">Last name<br />
        <input type="text" name="last_name" id="last_name" class="input" value="<?php echo esc_attr(wp_unslash($user_lastname)); ?>" size="20" /></label>
    </p>
    <p>
        <label for="user_dob">Birthdate (MM/DD/YYYY)<br />
        <input type="text" name="user_dob" id="user_dob" class="input" value="<?php echo esc_attr(wp_unslash($user_dob)); ?>" size="10" /></label>
    </p>
<?php
}
add_action('register_form', 'hhp_add_regis_fields');


function hhp_regischeck_fields ( $login, $email, $errors )
{
    global $user_firstname, $user_lastname, $user_dob;

    if ( $_POST['first_name'] == '' )
    {
        $errors->add( 'empty_firstname', "<strong>ERROR</strong>: Please Enter your First Name" );
    }
    else
    {
        $user_firstname = $_POST['first_name'];
    }

    if ( $_POST['last_name'] == '' )
    {
        $errors->add( 'empty_lastname', "<strong>ERROR</strong>: Please Enter your Last Name" );
    }
    else
    {
        $user_lastname = $_POST['last_name'];
    }

    if ( $_POST['user_dob'] == '' )
    {
        $errors->add( 'empty_dob', "<strong>ERROR</strong>: Please Enter your birthdate in MM/DD/YYYY format" );
    }
    else
    {
        $dob_info = explode('/', $_POST['user_dob']);
        if(count($dob_info) != 3 || $dob_info[0] > 12 || $dob_info[1] > 31 || $dob_info[2] < 1900) {
            $errors->add( 'empty_dob', "<strong>ERROR</strong>: Please Enter your birthdate in MM/DD/YYYY format" );
        }
        else {
            $user_dob = $_POST['user_dob'];
        }
    }
}
add_action('register_post','hhp_regischeck_fields',10,3);


function register_extra_fields ( $user_id, $password = "", $meta = array() )
{
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $user_name = $first_name . ' ' . $last_name;
    $url_safe_name = str_replace(' ', '-', $user_name);
    $user_id = wp_update_user( 
        array(  'ID' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name, 
                'display_name' => $user_name,
                'user_nicename' => $url_safe_name,
                'nickname' => $url_safe_name ) 
    );

    $dob_info = explode('/', $_POST['user_dob']); //MM/DD/YYYY
    update_user_meta( $user_id, 'user_birthday', $dob_info[1] );
    update_user_meta( $user_id, 'user_birthmonth', $dob_info[0] ); 
    update_user_meta( $user_id, 'user_birthyear', $dob_info[2] );  
}
add_action('user_register', 'register_extra_fields');


/*************************************************************
 * Adding additional logout code
 *************************************************************/
//function hhp_logout_social() { }
//add_action('wp_logout', 'hhp_logout_social');
?>