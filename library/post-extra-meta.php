<?php

//***************************
// Extra Meta data for posts 
//***************************

//
// Enqueue required scripts
//

function hhp_admin_post_scripts() {
	wp_enqueue_script('hhp_post_admin_js');
	wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style( 'hhp_post_admin_css' );
    wp_enqueue_style( 'hhp_jquery_ui_css' );
}

add_action( 'admin_enqueue_scripts', 'hhp_admin_post_scripts' );

//
// Meta Box Creation
//

function hhp_posts_promo_meta_box( $post ) {

    $promo_from = get_post_meta( $post->ID,
        'promo-from', true );
    if(!empty($promo_from)){
    	//Convert to MM/DD/YYYY format
    	$dfull = date("m/d/Y G i", $promo_from);
    	$dfull_ex = explode(' ', $dfull);
    	$promo_from = $dfull_ex[0];
    	$promo_from_hh = $dfull_ex[1];
    	$promo_from_mm = $dfull_ex[2];
    }

    $promo_to = get_post_meta( $post->ID,
        'promo-to', true );
    if(!empty($promo_to)){
    	//Convert to MM/DD/YYYY format
    	$dfull = date("m/d/Y G i", $promo_to);
    	$dfull_ex = explode(' ', $dfull);
    	$promo_to = $dfull_ex[0];
    	$promo_to_hh = $dfull_ex[1];
    	$promo_to_mm = $dfull_ex[2];
    }

    $promo_buynow_url = esc_html( get_post_meta( $post->ID,
        'promo-buynow-url', true ) );
?>

    <div class="cell-stackable">Promo Start Date<input type="text" size="12"
        id="promo-from" name="promo-from"
        value="<?php echo $promo_from ?>" />
    </div>
    <div class="cell-stackable">Promo Start Time
    	<select id="promo-from-hh" name="promo-from-hh">
    		<?php
    		for($i=0; $i<24; $i++){
    			if($i == $promo_from_hh){
    				echo '<option value="' . $i . '" selected>' . $i . '</option>';
    			}
    			else {
    				echo '<option value="' . $i . '">' . $i . '</option>';	
    			}
    		}
    		?>
    	</select>
        :
        <select id="promo-from-mm" name="promo-from-mm">
    		<?php
    		for($i=0; $i<60; $i+=15){
    			$optstr = '<option value="' . $i . '"';
    			$i == $promo_from_mm ? $optstr .= ' selected>' : $optstr .= '>';
    			$i == 0 ? $optstr .= '00</option>' : $optstr .= $i . '</option>';
    			echo $optstr;

    			if($i == 45){ $i--; } //To get 59 next
    		}
    		?>
    	</select>
    </div>

    <div class="clear"></div>

    <div class="cell-stackable">Promo End Date<input type="text" size="12"
        id="promo-to" name="promo-to"
        value="<?php echo $promo_to ?>" />
    </div>
    <div class="cell-stackable">Promo End Time
    	<select id="promo-to-hh" name="promo-to-hh">
    		<?php
    		for($i=0; $i<24; $i++){
    			if($i == $promo_to_hh){
    				echo '<option value="' . $i . '" selected>' . $i . '</option>';
    			}
    			else {
    				echo '<option value="' . $i . '">' . $i . '</option>';	
    			}
    		}
    		?>
    	</select>
        :
        <select id="promo-to-mm" name="promo-to-mm">
    		<?php
    		for($i=0; $i<60; $i+=15){
    			$optstr = '<option value="' . $i . '"';
    			$i == $promo_to_mm ? $optstr .= ' selected>' : $optstr .= '>';
    			$i == 0 ? $optstr .= '00</option>' : $optstr .= $i . '</option>';
    			echo $optstr;

    			if($i == 45){ $i--; } //To get 59 next
    		}
    		?>
    	</select>
    </div>

    <div class="clear"></div>

    <div class="full-row">Buy Now URL <input type="text" size="70"
        id="promo-buynow-url" name="promo-buynow-url"
        value="<?php echo $promo_buynow_url ?>" />
    </div>
    
<?php
}


function hhp_posts_metabox_init() {
    
    add_meta_box( 'hhp_posts_promo_meta_box',
        'Promo Details',
        'hhp_posts_promo_meta_box',
        'post', 'normal', 'high' );
}

add_action( 'add_meta_boxes', 'hhp_posts_metabox_init' );


//
// Function to save extra meta data
//
function hhp_save_post_meta($post_id, $post){
// Check post type == places
    if ( $post->post_type == 'post' ) {
        // Store data in post meta table if present in post data

        //Date is in MM/DD/YYYY format
        if ( isset( $_POST['promo-from'] ) && $_POST['promo-from'] != '' ) {
        	$pfrom = explode('/', $_POST['promo-from']);
        	if(count($pfrom) == 3 && $pfrom[0] <= 12 || $pfrom[1] <= 31 || $pfrom[2] > 2000) {

        		if ( isset( $_POST['promo-from-hh'] ) && $_POST['promo-from-hh'] != '' ) {
        			$pfrom_hh = $_POST['promo-from-hh'];
        		}
        		else {
        			$pfrom_hh = 0;
        		}
        		if ( isset( $_POST['promo-from-mm'] ) && $_POST['promo-from-mm'] != '' ) {
        			$pfrom_mm = $_POST['promo-from-mm'];
        		}
        		else {
        			$pfrom_mm = 0;
        		}
           		update_post_meta( $post_id, 'promo-from',
                	mktime($pfrom_hh, $pfrom_mm, 0, $pfrom[0],$pfrom[1],$pfrom[2]) );
           	}
        }
        else {
            delete_post_meta( $post_id, 'promo-from');
        }

        if ( isset( $_POST['promo-to'] ) && $_POST['promo-to'] != '' ) {
        	$pto = explode('/', $_POST['promo-to']);
        	if(count($pto) == 3 && $pto[0] <= 12 || $pto[1] <= 31 || $pto[2] > 2000) {

        		if ( isset( $_POST['promo-to-hh'] ) && $_POST['promo-to-hh'] != '' ) {
        			$pto_hh = $_POST['promo-to-hh'];
        		}
        		else {
        			$pto_hh = 0;
        		}
        		if ( isset( $_POST['promo-to-mm'] ) && $_POST['promo-to-mm'] != '' ) {
        			$pto_mm = $_POST['promo-to-mm'];
        		}
        		else {
        			$pto_mm = 0;
        		}
           		update_post_meta( $post_id, 'promo-to',
                	mktime($pto_hh,$pto_mm,0,$pto[0],$pto[1],$pto[2]) );
           	}
        }
        else {
            delete_post_meta( $post_id, 'promo-to');
        }

        if ( isset( $_POST['promo-buynow-url'] ) && $_POST['promo-buynow-url'] != '' ) {
        	update_post_meta( $post_id, 'promo-buynow-url',
        		$_POST['promo-buynow-url']);
        }
        else {
            delete_post_meta( $post_id, 'promo-buynow-url');
        }
    }
}

add_action( 'save_post', 'hhp_save_post_meta', 10, 2 ); //Default priority is 10

?>