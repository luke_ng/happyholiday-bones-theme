<?php

/**************************************************************
 Register Custom Taxonomies for 'Places' Custom Post Type
**************************************************************/

add_action( 'init', 'create_topics_hierarchical_taxonomy', 0 );
add_action( 'init', 'create_taxonomy_interest_category', 1 );

function create_topics_hierarchical_taxonomy() {
    // Add new taxonomy, make it hierarchical like categories

    //first do the translations part for GUI
    $labels = array(

    'name' => _x( 'Location', 'taxonomy general name' ),
    'singular_name' => _x( 'Location', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Locations' ),
    'all_items' => __( 'All Locations' ),
    'parent_item' => __( 'Parent Location' ),
    'parent_item_colon' => __( 'Parent Location:' ),
    'edit_item' => __( 'Edit Location' ), 
    'update_item' => __( 'Update Location' ),
    'add_new_item' => __( 'Add New Location' ),
    'new_item_name' => __( 'New Location Name' ),
    'menu_name' => __( 'Locations' ),

    );  

    // Now register the taxonomy

    register_taxonomy('Location',array('places'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'location' ),

    ));

}


function create_taxonomy_interest_category() {
    // Add new taxonomy, make it hierarchical like categories

    //first do the translations part for GUI

    $labels = array(

        'name' => _x( 'Place-category', 'taxonomy general name' ),
        'singular_name' => _x( 'Place-category', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Place-category' ),
        'all_items' => __( 'All Place-categories' ),
        'parent_item' => __( 'Parent Place-category' ),
        'parent_item_colon' => __( 'Parent Place-category:' ),
        'edit_item' => __( 'Edit Place-category' ), 
        'update_item' => __( 'Update Place-category' ),
        'add_new_item' => __( 'Add New Place-category' ),
        'new_item_name' => __( 'New Place-category Name' ),
        'menu_name' => __( 'Place-categories' ),

    );    

    // Now register the taxonomy
    register_taxonomy('Place-categories',array('places'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'Place-category' ),
    ));

}

/**************************************************************
 Register 'Places' Custom Post Type
 **************************************************************/

function custom_post_type() {

    $labels = array(
        'name'                => _x( 'Places', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Place', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Places', 'text_domain' ),
        'parent_item_colon'   => __( 'Parent Place:', 'text_domain' ),
        'all_item'           => __( 'All Places', 'text_domain' ),
        'view_item'           => __( 'View Place', 'text_domain' ),
        'add_new_item'        => __( 'Add New Place', 'text_domain' ),
        'add_new'             => __( 'New Place', 'text_domain' ),
        'edit_item'           => __( 'Edit Place', 'text_domain' ),
        'update_item'         => __( 'Update Place', 'text_domain' ),
        'search_items'        => __( 'Search places', 'text_domain' ),
        'not_found'           => __( 'No places found', 'text_domain' ),
        'not_found_in_trash'  => __( 'No places found in Trash', 'text_domain' ),

    );

    $args = array(

        'label'               => __( 'places', 'text_domain' ),
        'description'         => __( 'Post the places of interest here, categorize them and tag location', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array('title','editor','custom-fields','thumbnail','excerpt' ),
        'taxonomies'          => array( 'location' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        //'capability_type'     => array('place','places'),
        'map_meta_cap'        => true,
        'capabilities' => array(
                    'publish_posts' => 'publish_places',
                    'edit_posts' => 'edit_places',
                    'edit_others_posts' => 'edit_others_places',
                    'delete_posts' => 'delete_places',
                    'delete_others_posts' => 'delete_others_places',
                    'read_private_posts' => 'read_private_places',
                    'edit_post' => 'edit_place',
                    'delete_post' => 'delete_place',
                    'read_post' => 'read_place')

    );

    register_post_type( 'Places', $args );
}

// Hook into the 'init' action
add_action( 'init', 'custom_post_type', 0 );

/************* Adding template to display the 'places' post type *************/

add_filter( 'template_include', 'hhp_places_template_include', 1 );

function hhp_places_template_include( $template_path ) {
    if ( get_post_type() == 'places' ) {
        if ( is_single() ) {
            // checks if the file exists in the theme first,
            // otherwise serve the file from the plugin
            if ( $theme_file = locate_template( array
                ( 'single-places.php' ) ) ) {
                $template_path = $theme_file;
            } else {
                $template_path = plugin_dir_path( __FILE__ ) .
                    '/single-places.php';
            }
        }
    }
    return $template_path;
}


/************* Adding meta box for the 'places' post type (shown when creating posts) *************/


//add_action( 'admin_init', 'hhp_places_admin_init' ); //deprecated way to add metabox
add_action( 'add_meta_boxes', 'hhp_places_metabox_init' ); //new way since WP 3.0

add_action( 'admin_enqueue_scripts', 'hhp_admin_places_scripts' );
add_action( 'wp_enqueue_scripts' , 'hhp_wp_enqueue_js' );

//Add bootstrap javascript code to the footer of wp pages (for admin page use admin_footer)
//add_action( 'wp_footer', 'hhp_places_meta_bootstrap' );
add_action( 'save_post', 'hhp_save_places_fields', 10, 2 ); //Default priority is 10


function hhp_places_metabox_init() {
    
    add_meta_box( 'hhp_places_details_meta_box',
        'Place Details',
        'hhp_places_display_details_meta_box',
        'places', 'normal', 'high' );
}

function hhp_admin_places_scripts() {
    wp_enqueue_script( 'hhp_custom_post_admin_js' );
    wp_enqueue_style( 'hhp_custom_post_admin_css' );
}

function hhp_wp_enqueue_js() {
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'jquery-ui-core' );
    wp_enqueue_script( 'jquery-ui-widget' );       
    wp_enqueue_script( 'jquery-ui-tabs' );
    wp_enqueue_style( 'hhp_jquery_ui_css' );
}

function hhp_places_display_details_meta_box( $place ) {
    // Retrieve current author and rating based on review ID
    $place_name = esc_html( get_post_meta( $place->ID,
        'place-name', true ) );

    $place_webpage = esc_html( get_post_meta( $place->ID,
        'place-webpage', true ) );

    $place_email = get_post_meta( $place->ID,
        'place-email', true );

    $place_food_style = get_post_meta( $place->ID,
        'place-food-style', true );

    $place_price_range = get_post_meta( $place->ID,
        'place-price-range', true );

    $place_num_branch = intval( get_post_meta( $place->ID,
        'place-num-branch', true ) );

    $place_num_todo = intval( get_post_meta( $place->ID,
        'place-num-todo', true ) );

    $place_trait_shopping = intval( get_post_meta( $place->ID,
        'place-trait-shopping', true ) );

    $place_trait_sightsee = intval( get_post_meta( $place->ID,
        'place-trait-sightsee', true ) );

    $place_trait_funrelax = intval( get_post_meta( $place->ID,
        'place-trait-funrelax', true ) );

    $place_can_revisit = get_post_meta( $place->ID,
        'place-can-revisit', true );
?>
    <table>
        <tr>
        <td style="width: 100%">Place Name</td>
        <td><input type="text" size="70"
        id="place_name" name="place_name"
        value="<?php echo $place_name ?>" /></td>
        </tr>

        <tr>
        <td style="width: 100%">Webpage</td>
        <td><input type="text" size="70"
        id="place_webpage" name="place_webpage"
        value="<?php echo $place_webpage ?>" /></td>
        </tr>

        <tr>
        <td style="width: 100%">Email</td>
        <td><input type="text" size="70"
        id="place_email" name="place_email"
        value="<?php echo $place_email ?>" /></td>
        </tr>

        <tr>
        <td style="width: 100%">Food style</td>
        <td><input type="text" size="70"
        id="place_food_style" name="place_food_style"
        value="<?php echo $place_food_style ?>" /></td>
        </tr>

        <tr>
        <td style="width: 100%">Price range</td>
        <td><input type="text" size="70"
        id="place_price_range" name="place_price_range"
        value="<?php echo $place_price_range ?>" /></td>
        </tr>

        <tr>
        <td style="width: 100%">Can revisit?</td>
        <td>
            <input type="checkbox" name="place_can_revisit" value="1"
                <?php if(isset($place_can_revisit) && $place_can_revisit==1){echo "checked";}?>/>
            (Some places like night markets can be revisited in a single trip)
        </td>
        </tr>

        <tr><td>&nbsp</td></tr>
        
        <tr>
        <td style="width: 100%"><strong>Place Characteristics</strong></td>
        </tr>
        <tr>
            <td style="width: 100%">Shopping %</td>
            <td>
                <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="trait_shopping" value="0"
                    <?php if($place_trait_shopping==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="trait_shopping" value="25"
                    <?php if($place_trait_shopping==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="trait_shopping" value="50"
                    <?php if($place_trait_shopping==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="trait_shopping" value="75"
                    <?php if($place_trait_shopping==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="trait_shopping" value="100"
                    <?php if($place_trait_shopping==100){echo "checked";}?>>
                </td>
                </tr></table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%">Sightseeing %</td>
            <td>
                <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="trait_sightsee" value="0"
                    <?php if($place_trait_sightsee==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="trait_sightsee" value="25"
                    <?php if($place_trait_sightsee==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="trait_sightsee" value="50"
                    <?php if($place_trait_sightsee==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="trait_sightsee" value="75"
                    <?php if($place_trait_sightsee==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="trait_sightsee" value="100"
                    <?php if($place_trait_sightsee==100){echo "checked";}?>>
                </td>
                </tr></table>
            </td>
        </tr>
        <tr>
            <td style="width: 100%">Fun and relax %</td>
            <td>
                <table><tr>
                <td style="padding: 0 10px; text-align: center;">None<br>
                    <input type="radio" name="trait_funrelax" value="0"
                    <?php if($place_trait_funrelax==0){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A little<br>
                    <input type="radio" name="trait_funrelax" value="25"
                    <?php if($place_trait_funrelax==25){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Some<br>
                    <input type="radio" name="trait_funrelax" value="50"
                    <?php if($place_trait_funrelax==50){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">A lot<br>
                    <input type="radio" name="trait_funrelax" value="75"
                    <?php if($place_trait_funrelax==75){echo "checked";}?>>
                </td>
                <td style="padding: 0 10px; text-align: center;">Most<br>
                    <input type="radio" name="trait_funrelax" value="100"
                    <?php if($place_trait_funrelax==100){echo "checked";}?>>
                </td>
                </tr></table>
            </td>
        </tr>
    </table>

    <input type="hidden" id="place_num_branch" name="place_num_branch" 
        value="<?php echo $place_num_branch ?>" />

    <h4>Branch details</h4>
    <div id="hhp_branches"></div>
    <button id="btn_add_branch">Add branch</button>

    <br><br>
    <h4>Things to do here</h4>
    <div id="hhp_todo_container"></div>
    <button id="btn_add_todo">Add To-Do</button>    

    <input type="hidden" id="place_num_todo" name="place_num_todo" 
        value="<?php echo $place_num_todo ?>" />
    

    <script type="text/javascript">
        jQuery(document).ready(function() {

            jQuery("#btn_add_branch").click(function() { 
                var curr_num_branches = parseInt(jQuery('#place_num_branch').val());
                addBranch(curr_num_branches+1);
                return false; 
            });

            jQuery("#btn_add_todo").click(function() { 
                var curr_num_todos = parseInt(jQuery('#place_num_todo').val());
                addTodo(curr_num_todos+1);
                return false; 
            });


            <?php if($place_num_branch > 0) {
                // Pass each branch info into javascript function
                for ( $branch_num = 1; $branch_num <= $place_num_branch; $branch_num ++ ) { ?>
                    addBranch(<?php echo $branch_num ?>); 
                    <?php
                    $order   = array("\r\n", "\n\r", "\n", "\r");
                    $replace = '<br />';
                    // Processes \r\n's first so they aren't converted twice.
                    //For multi-line inputs, we must take note to remove 
                    //all \n\r so that the output will 
                    //not cause error in javascript

                    $b_address = get_post_meta( $place->ID,
                        'place-address-'.$branch_num, true );
                    $b_address = str_replace($order, $replace, $b_address);
                    $b_address = addslashes($b_address);
                    //if(empty($b_address)){ $b_address = 'null'; }

                    $b_map_url = esc_html( get_post_meta( $place->ID,
                        'place-map-url-'.$branch_num, true ) );
                    //if(empty($b_map_url)){ $b_map_url = 'null'; }

                    $b_map_iframe = get_post_meta( $place->ID,
                        'place-map-iframe-'.$branch_num, true );
                    //if(empty($b_map_iframe)){ $b_map_iframe = 'null'; }

                    $b_coordinates = esc_html( get_post_meta( $place->ID,
                        'place-coordinates-'.$branch_num, true ) ); 
                    //if(empty($b_coordinates)){ $b_coordinates = 'null'; }

                    $b_op_hours = get_post_meta( $place->ID,
                        'place-operating-hours-'.$branch_num, true );
                    $b_op_hours = str_replace($order, $replace, $b_op_hours); 
                    $b_op_hours = addslashes($b_op_hours);

                    $b_phone = get_post_meta( $place->ID,
                        'place-phone-'.$branch_num, true );
                    $b_phone = str_replace($order, $replace, $b_phone);
                    $b_phone = addslashes($b_phone);

                    $b_fax = get_post_meta( $place->ID,
                        'place-fax-'.$branch_num, true );
                    $b_fax = str_replace($order, $replace, $b_fax);
                    $b_fax = addslashes($b_fax);
                    ?>


                    fillBranchData(<?php echo $branch_num ?>, '<?php echo $b_address ?>', 
                        '<?php echo $b_map_url ?>', '<?php echo $b_map_iframe ?>', 
                        '<?php echo $b_coordinates ?>', '<?php echo $b_op_hours ?>',
                        '<?php echo $b_phone ?>', '<?php echo $b_fax ?>');
                <?php }
            }
            else { ?>
                addBranch(1);
            <?php } ?>



            <?php if($place_num_todo > 0) {
                // Pass each todo info into javascript function
                for ( $todo_num = 1; $todo_num <= $place_num_todo; $todo_num ++ ) { ?>
                    addTodo(<?php echo $todo_num ?>); 
                    <?php                
                    // Processes \r\n's first so they aren't converted twice.
                    //For multi-line inputs, we must take note to remove 
                    //all \n\r so that the output will 
                    //not cause error in javascript
                    $order   = array("\r\n", "\n\r", "\n", "\r");
                    $replace = '<br />';

                    $todo_type = get_post_meta( $place->ID,
                        'place-todo-type-'.$todo_num, true );
                    
                    $todo_img_url = esc_html( get_post_meta( $place->ID,
                        'place-todo-img-'.$todo_num, true ) );

                    $todo_text = get_post_meta( $place->ID,
                        'place-todo-text-'.$todo_num, true );
                    $todo_text = str_replace($order, $replace, $todo_text);
                    // Special characters (' " \) need to be escaped for passing into
                    // Javascript function
                    $todo_text = addslashes($todo_text);
                    
                    ?>

                    fillTodoData(<?php echo $todo_num ?>, "<?php echo $todo_type ?>", 
                        "<?php echo $todo_img_url ?>", "<?php echo $todo_text ?>");
                <?php }
            }
            else { ?>
                addTodo(1);
            <?php } ?>

        });
    </script>
<?php }


function hhp_save_places_fields($place_id, $place) {
    // Check post type == places
    if ( $place->post_type == 'places' ) {
        // Store data in post meta table if present in post data
        if ( isset( $_POST['place_name'] ) && $_POST['place_name'] != '' ) {
            update_post_meta( $place_id, 'place-name',
                $_POST['place_name'] );
        }
        else {
            delete_post_meta( $place_id, 'place-name');
        }

        if ( isset( $_POST['place_webpage'] ) && $_POST['place_webpage'] != '' ) {
            update_post_meta( $place_id, 'place-webpage',
                $_POST['place_webpage'] );
        }
        else {
            delete_post_meta( $place_id, 'place-webpage');
        }

        if ( isset( $_POST['place_email'] ) && $_POST['place_email'] != '' ) {
            update_post_meta( $place_id, 'place-email',
                $_POST['place_email'] );
        }
        else {
            delete_post_meta( $place_id, 'place-email');
        }

        if ( isset( $_POST['place_food_style'] ) && $_POST['place_food_style'] != '' ) {
            update_post_meta( $place_id, 'place-food-style',
                $_POST['place_food_style'] );
        }
        else {
            delete_post_meta( $place_id, 'place-food-style');
        }

        if ( isset( $_POST['place_price_range'] ) && $_POST['place_price_range'] != '' ) {
            update_post_meta( $place_id, 'place-price-range',
                $_POST['place_price_range'] );
        }
        else {
            delete_post_meta( $place_id, 'place-price-range');
        }

        if ( isset( $_POST['place_can_revisit'] ) && $_POST['place_can_revisit'] != '' ) {
            update_post_meta( $place_id, 'place-can-revisit',
                $_POST['place_can_revisit'] );
        }
        else {
            update_post_meta( $place_id, 'place-can-revisit', 0 );
        }

        if ( isset( $_POST['trait_shopping'] ) && $_POST['trait_shopping'] != '' ) {
            update_post_meta( $place_id, 'place-trait-shopping',
                $_POST['trait_shopping'] );
        }
        else {
            delete_post_meta( $place_id, 'place-trait-shopping');
        }

        if ( isset( $_POST['trait_sightsee'] ) && $_POST['trait_sightsee'] != '' ) {
            update_post_meta( $place_id, 'place-trait-sightsee',
                $_POST['trait_sightsee'] );
        }
        else {
            delete_post_meta( $place_id, 'place-trait-sightsee');
        }

        if ( isset( $_POST['trait_funrelax'] ) && $_POST['trait_funrelax'] != '' ) {
            update_post_meta( $place_id, 'place-trait-funrelax',
                $_POST['trait_funrelax'] );
        }
        else {
            delete_post_meta( $place_id, 'place-trait-funrelax');
        }

        //for each branch, save the data
        if ( isset( $_POST['place_num_branch'] ) && $_POST['place_num_branch'] != '' ) {
            update_post_meta( $place_id, 'place-num-branch',
                $_POST['place_num_branch'] );

            $place_num_branch = intval($_POST['place_num_branch']);

            for ( $branch_num = 1; $branch_num <= $place_num_branch; $branch_num ++ ) {
                if( isset( $_POST['place_address_'.$branch_num] ) && $_POST['place_address_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-address-'.$branch_num,
                        $_POST['place_address_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-address-'.$branch_num);
                }

                if( isset( $_POST['place_map_url_'.$branch_num] ) && $_POST['place_map_url_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-map-url-'.$branch_num,
                        $_POST['place_map_url_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-map-url-'.$branch_num);
                }

                if( isset( $_POST['place_map_iframe_'.$branch_num] ) && $_POST['place_map_iframe_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-map-iframe-'.$branch_num,
                        $_POST['place_map_iframe_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-map-iframe-'.$branch_num);
                }

                if( isset( $_POST['place_coordinates_'.$branch_num] ) && $_POST['place_coordinates_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-coordinates-'.$branch_num,
                        $_POST['place_coordinates_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-coordinates-'.$branch_num);
                }

                if( isset( $_POST['place_operating_hours_'.$branch_num] ) && $_POST['place_operating_hours_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-operating-hours-'.$branch_num,
                        $_POST['place_operating_hours_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-operating-hours-'.$branch_num);
                }

                if( isset( $_POST['place_phone_'.$branch_num] ) && $_POST['place_phone_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-phone-'.$branch_num,
                        $_POST['place_phone_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-phone-'.$branch_num);
                }

                if( isset( $_POST['place_fax_'.$branch_num] ) && $_POST['place_fax_'.$branch_num] != '') {
                    update_post_meta( $place_id, 'place-fax-'.$branch_num,
                        $_POST['place_fax_'.$branch_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-fax-'.$branch_num);
                }
            }

        }//End saving data for each branch


        //for each todo, save the data
        if ( isset( $_POST['place_num_todo'] ) && $_POST['place_num_todo'] != '' ) {
            update_post_meta( $place_id, 'place-num-todo',
                $_POST['place_num_todo'] );

            $place_num_todo = intval($_POST['place_num_todo']);

            for ( $todo_num = 1; $todo_num <= $place_num_todo; $todo_num ++ ) {
                if( isset( $_POST['place_todo_type_'.$todo_num] ) && $_POST['place_todo_type_'.$todo_num] != '') {
                    update_post_meta( $place_id, 'place-todo-type-'.$todo_num,
                        $_POST['place_todo_type_'.$todo_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-todo-type-'.$todo_num);
                }

                if( isset( $_POST['place_todo_img_'.$todo_num] ) && $_POST['place_todo_img_'.$todo_num] != '') {
                    update_post_meta( $place_id, 'place-todo-img-'.$todo_num,
                        $_POST['place_todo_img_'.$todo_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-todo-img-'.$todo_num);
                }

                if( isset( $_POST['place_todo_text_'.$todo_num] ) && $_POST['place_todo_text_'.$todo_num] != '') {
                    update_post_meta( $place_id, 'place-todo-text-'.$todo_num,
                        $_POST['place_todo_text_'.$todo_num] );
                }
                else {
                    delete_post_meta( $place_id, 'place-todo-text-'.$todo_num);
                }

            }
        }//End saving data for each todo
    }

}

//This is for post displays
function hhp_places_meta_bootstrap() { ?>
    <script type="text/javascript">
        jQuery( document ).ready(function() {
            
            jQuery( "#hhp_branch_tabs" ).tabs({
                activate: function( event, ui ) {
                    var active_tab = jQuery( "#hhp_branch_tabs" ).tabs( "option", "active" );
                    active_tab = active_tab + 1;
                    var frame = jQuery('#p_map_iframe'+active_tab).contents();
                    jQuery('#p_map_iframe'+active_tab).empty().append(frame);
                    
                    //jQuery('#p_map_iframe'+active_tab).append('<p>Hahahaha</p>');
                }
            });
            
        } );
    </script>
<?php }

?>