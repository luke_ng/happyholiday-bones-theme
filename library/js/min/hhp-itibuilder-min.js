///////
// Global variables
///////

var ib_current_page; //Stores the current page (overview or detail pg)
                     // Pg 0 = overview pg, 1,2,3... = detail pg

var swapped_places; //Stores the places that user has swapped
//Structure of swapped places:
// {
//   day_id#1 : { orig_place#1 : new_place#1, orig_place#2 : new_place#2, ... } ,
//   day_id#2 : { orig_place#1 : new_place#1 } ,
// };

///////
// FUNCTIONS
///////

function hasProv(prov_id){
  var hasprov = false;

  jQuery( ".selected_prov" ).each(function( prov_index ) {
    var curr_id =  jQuery(this).find("select").val();
    if(curr_id == prov_id){
      hasprov = true;
    }
  });

  return hasprov;
}


function doItiBuildSubmit(){
  //jQuery("#desk-iti-form").trigger("submit");
}

function adjustGoToDayBtnMobile(){
  var panel_width = jQuery("#sft-selpage-panel").width();
  //jQuery("#sftwidth").text(panel_width);

  //Set the default first
  var style4btn = {
    "width" : "21%",
    "padding-bottom" : "21%"
  };
  jQuery(".btn_goto_itiday").css(style4btn);

  if(panel_width > 300){
    //Do 5 buttons in a row
    var style5btn = {
      "width" : "16%",
      "padding-bottom" : "16%"
    };
    jQuery(".btn_goto_itiday").css(style5btn);
  }

  if(panel_width > 360){
    //Do 6 buttons in a row
    var style6btn = {
      "width" : "12.666%",
      "padding-bottom" : "12.666%"
    };
    jQuery(".btn_goto_itiday").css(style6btn);
  }

  //Finally set the max height of the sft-selpage-panel according to the screen height
  jQuery("#sft-selpage-panel").css("max-height", (screen.height-150));
}

//To get the province name from ID
//$(".loc-select option[value='2']").text()

/* Sample output of an itinerary day
  <p style="font-size:28px; color:#ff6600;">Day 1</p>
  <p style="font-size:26px">Taoyuan -> Taipei City</p>
  <br><br><br>
  <div style="width:450px; position:relative;">
    <img width="1280" height="720" 
      src="http://devgz.happyholidayplanner.com/wp-content/uploads/2014/05/ning-xia-ye-shi-ning-xia-night.jpg" 
      class="feature-img-med wp-post-image" alt="寧夏夜市 &#8211; Níng Xià Yè Shì (Ning Xia Night Market)" />
        <div style="position:absolute; bottom:7px; right:2px; font-size:140%;">
            <button class="btn-swap-place" place-id="1063">Change</button>
        </div>
  </div>
  <br>
  <strong style="font-size:18px">寧夏夜市 - Níng Xià Yè Shì (Ning Xia Night Market)</strong>
  <br><br><br>
*/

//Reveal the chosen page number and update the pagination buttons accordingly
// Pg num starts from 0
function show_ib_page(pg_to_show){
  //var numdays = jQuery(".ib-daybox").length;
  var numpages = 1 + jQuery(".ib-page").length; //+1 f

  if(pg_to_show == 0){
    //Show the overview page
    jQuery("#iti-details").addClass("hidden");
    jQuery(".ib-page").addClass("hidden");
    jQuery("#iti-overview").removeClass("hidden");

    //Update the current page
    ib_current_page = pg_to_show;
  }
  else if(pg_to_show > 0 && pg_to_show < numpages){
    jQuery("#iti-overview").addClass("hidden");
    jQuery("#iti-details").removeClass("hidden");

    //Hide the current shown day
    if(ib_current_page>0){
      jQuery(".ib-page").eq(ib_current_page-1).addClass("hidden");
    }
    //Show the chosen day
    jQuery(".ib-page").eq(pg_to_show-1).toggleClass("hidden");

    //Show/hide next/prev buttons
    if(pg_to_show == 1){
      //Next button displayed, Prev button hidden
      jQuery("#iti-next-buttondiv").removeClass("hidden");
      jQuery("#iti-prev-buttondiv").addClass("hidden");
    }
    else if(pg_to_show == numpages-1){
      //Next button hidden, Prev button displayed
      jQuery("#iti-next-buttondiv").addClass("hidden");
      jQuery("#iti-prev-buttondiv").removeClass("hidden");
    }
    else {
      //Both buttons displayed
      jQuery("#iti-next-buttondiv").removeClass("hidden");
      jQuery("#iti-prev-buttondiv").removeClass("hidden");
    }

    //Update the current page
    ib_current_page = pg_to_show;
  }
}


//Mobile version shows the itinerary page 1 by 1, instead of in pairs
function show_ib_page_mobile(pg_to_show){
  var numpages = 1 + jQuery(".ib-daybox").length; //+1 f

  if(pg_to_show == 0){
    //Show the overview page
    jQuery("#iti-details").addClass("hidden");
    jQuery(".ib-page").addClass("hidden");
    jQuery("#iti-overview").removeClass("hidden");

    //Update the current page
    ib_current_page = pg_to_show;
  }
  else if(pg_to_show > 0 && pg_to_show < numpages){
    jQuery("#iti-overview").addClass("hidden");
    jQuery("#iti-details").removeClass("hidden");
    // Each .ib-page contains 2 
    // .ib-daybox

    //Hide the current page
    var ib_page_index = 0;

    if(ib_current_page > 0){
      ib_page_index = Math.floor((ib_current_page-1)/2);
      jQuery('.ib-page').eq(ib_page_index).addClass("hidden");
      jQuery('.ib-daybox').eq(ib_current_page-1).addClass("hidden");
    }
    //Show the new page
    ib_page_index = Math.floor((pg_to_show-1)/2);
    jQuery('.ib-page').eq(ib_page_index).removeClass("hidden");
    jQuery('.ib-daybox').eq(pg_to_show-1).removeClass("hidden");

    //Update the current page
    ib_current_page = pg_to_show;
  }

  //After showing, hide the panel if its shown
  jQuery("#sft-selpage-panel").addClass("sft-p-hidden");
}


function go_next_ib_page(){
  show_ib_page(1+ib_current_page);
}


function go_prev_ib_page(){
  show_ib_page(ib_current_page-1);
}

function go_ib_highlight_page(){
  show_ib_page(0);
}


function sendItiEmail(){

  //Show the loading bar
  jQuery("#email-iti-loader").removeClass("hidden");
  jQuery("#email-iti-success").addClass("hidden");
  jQuery("#email-iti-error").addClass("hidden");
  //Disable the dialog buttons
  jQuery("#email-iti-dialog").find(".ui-dialog-buttonset button").prop("disabled",true);

  var do_email_url = jQuery("#do_email_url").val();
  var iti_data = "";

  //Gather the itinerary data
  jQuery(".ib-daybox").each(function(day_index){
    //Append the day number
    iti_data += "<br><br>== Day " + (day_index+1) + " ==<br><br>";

    jQuery(this).children(".ib-place-entry").each(function(){
      //Append the province name
      var provname = jQuery(this).find('[class*="tag-loc-"]').text();
      iti_data += "[" + provname + "] ";

      //Append the place name
      var placename = jQuery(this).find('.ib-place-title').text();
      //Get the chinese place name
      var placename_chi = jQuery(this).find('.ib-placename-chinese').val();
      iti_data += placename_chi + "  " + placename + "<br>";
    });  
  });
  

  jQuery.post( do_email_url, 
    { user_email: jQuery("#email-iti-addr").val(),
      iti_string: encodeURIComponent(iti_data),
      extra_msg: jQuery("#email-iti-msg").val() })
    .done(function( return_data ) {
      //Hide the loading bar
      jQuery("#email-iti-loader").addClass("hidden");
      //Enable the dialog buttons
      jQuery("#email-iti-dialog").find(".ui-dialog-buttonset button").prop("disabled", false);

      //Check the outcome
      if(return_data == "0"){
        jQuery("#email-iti-success").removeClass("hidden");
      }
      else {
        jQuery("#email-iti-error").text(return_data).removeClass("hidden");
      }
    });
}


function show_iti_highlights(){
  //Build the list of places to exclude
  var place_list = "";
  jQuery( ".ib-btn-swap" ).each(function() {
    place_list += jQuery(this).attr("place-id") + ",";
  });

  //Check if its in mobile or desktop view
  var mobile_disp = jQuery("#hl-container-mobile").css("display");
  var mobile_mode = true;
  if(mobile_disp == "none"){
    mobile_mode = false;
  } 

  //Make sure all entries are hidden first
  if(mobile_mode){
    jQuery(".hl-div-mobile").addClass("hidden");
  }
  else {
    jQuery(".hl-div").addClass("hidden");
  }

  var pop_places_url = jQuery("#pop_places_url").val();
  jQuery.post( pop_places_url, 
      { num_pop_places: 4, place_list: place_list })
    .done( function( data ) {
      var hl_data = JSON.parse(data);
      for(c_index=0; c_index<hl_data.length; c_index++){

        if(mobile_mode){
          jQuery(".hl-div-mobile").eq(c_index).find("p").empty().append(hl_data[c_index]['title']);
          jQuery(".iti-highlight-mobile").eq(c_index).css("background-image", "url('"+ hl_data[c_index]['image_full'] +"')");
          jQuery(".iti-highlight-url-mobile").eq(c_index).attr("href", hl_data[c_index]['permalink']);

          jQuery(".hl-div-mobile").eq(c_index).removeClass("hidden");
        }
        else {
          jQuery(".hl-placename-box").eq(c_index).find("p").empty().append(hl_data[c_index]['title']);
          jQuery(".iti-highlight").eq(c_index).css("background-image", "url('"+ hl_data[c_index]['image_full'] +"')");
          jQuery(".iti-highlight-url").eq(c_index).attr("href", hl_data[c_index]['permalink']);

          jQuery(".hl-div").eq(c_index).removeClass("hidden");
        }
      }
    });
}


function show_iti_trail_mobile(){
  //Position the iti trail markers (3% - 91%)
  var curr_left = 3;
  var num_markers = jQuery(".iti-trail-mark-mobile").length;
  var gap_width = (91-3) / (num_markers-1);

  for(m=0; m<num_markers; m++){
    jQuery('.iti-trail-mark-mobile').eq(m).css("left", curr_left+'%');
    if(m%2 == 0){
      //Needs to be places below the marker
      jQuery('.iti-trail-label-mobile').eq(m).css("left", (curr_left-3)+'%');
      jQuery('.iti-trail-label-mobile').eq(m).css("top", "70px");
    }
    else {
      //Needs to be placed above the marker
      jQuery('.iti-trail-label-mobile').eq(m).css("left", (curr_left-3)+'%');
      jQuery('.iti-trail-label-mobile').eq(m).css("top", "12px");
    }
    curr_left += gap_width;
  }
}

function swap_place(choice_index, container_id){

  //Place Title: choice_data[choice_index]['title']
  //Image url: choice_data[choice_index]['image'] 

  var swap_cont = jQuery(".swapchoice-container").eq(choice_index);

  //Save the details of the place that is about to be swapped
  var orig_title = jQuery("#"+container_id+" .ib-place-title").html();
  var orig_imgurl = jQuery("#"+container_id+" .ib-place-img img").attr("src");
  //var orig_thumburl = jQuery("#"+container_id+" .place-thumb-url").val();
  var orig_place_id = jQuery("#"+container_id+" .ib-btn-swap").attr("place-id");
  var orig_prov_name = jQuery("#"+container_id+" .ib-place-rightcol span").text();
  var orig_prov_class = jQuery("#"+container_id+" .ib-place-rightcol span").attr("class");
  var orig_excerpt = jQuery("#"+container_id+" .ib-place-excerpt").val();
  var orig_permalink = jQuery("#"+container_id+" .ib-img-anchor").attr("href");

  var swap_title = swap_cont.find(".swapchoice-title").html();
  //var swap_imgurl = swap_cont.find(".swapchoice-imgurl").val();
  var swap_imgurl = swap_cont.find(".swapchoice-thumburl").attr("src");
  var swap_place_id = swap_cont.find(".swapchoice-place-id").val();
  var swap_prov_name = swap_cont.find(".swapchoice-prov-name").val();
  var swap_prov_class = swap_cont.find(".swapchoice-prov-class").val();
  var swap_excerpt = swap_cont.find(".swapchoice-excerpt").html();
  var swap_permalink = jQuery("#scbtn_detail"+choice_index).attr("href");

  //Update the selected place to the itinerary
  jQuery("#"+container_id+" .ib-place-title").html(swap_title);
  jQuery("#"+container_id+" .ib-place-img img").attr("src", swap_imgurl);
  jQuery("#"+container_id+" .ib-btn-swap").attr("place-id", swap_place_id);
  jQuery("#"+container_id+" .ib-place-rightcol span").text(swap_prov_name);
  jQuery("#"+container_id+" .ib-place-rightcol span").attr("class", swap_prov_class);
  jQuery("#"+container_id+" .ib-place-excerpt").val(swap_excerpt);
  jQuery("#"+container_id+" .ib-img-anchor").attr("href", swap_permalink);
  jQuery("#"+container_id+" .ib-title-anchor").attr("href", swap_permalink);
  

  //Update the orginal place to the swap choices
  swap_cont.find(".swapchoice-title").html(orig_title);
  swap_cont.find(".swapchoice-thumburl").attr("src", orig_imgurl);
  swap_cont.find(".swapchoice-place-id").val(orig_place_id);
  swap_cont.find(".swapchoice-prov-name").val(orig_prov_name);
  swap_cont.find(".swapchoice-prov-class").val(orig_prov_class);
  swap_cont.find(".swapchoice-excerpt").html(orig_excerpt);
  jQuery("#scbtn_detail"+choice_index).attr("href", orig_permalink);

  update_swapped_places(container_id, orig_place_id, swap_place_id);
}


function update_swapped_places(container_id, orig_place_id, swap_place_id){
  //console.log("Updating swapped_places.. Container_id=" + container_id + 
    //"  orig_place_id=" + orig_place_id + "  swap_place_id=" + swap_place_id);

  //retrieve the day_id
  var day_id = jQuery('#'+container_id).parent().find(".ib-day-id").val();
  //console.log("Day_id retrieved: " + day_id);
  if(!swapped_places.hasOwnProperty(day_id)){
    swapped_places[day_id] = new Object();  
  }
  
  swapped_places[day_id][orig_place_id] = swap_place_id;
  //console.log("swapped_places updated : " + swapped_places);
}

////////
// Handle document-ready event
////////

jQuery( document ).ready(function() {

  //Init variables
  swapped_places = new Object();

  //activate only on mobile
  if (window.matchMedia('(max-width: 767px)').matches) {
    jQuery('html').on('touchstart', function() {
        //Hide the panels if visible
        //jQuery("#sft-edit-panel").addClass("sft-p-hidden");
        //jQuery("#sft-selpage-panel").addClass("sft-p-hidden");
        //jQuery("#sft-export-panel").addClass("sft-p-hidden");
        //Hide popup dialog if visible
        //jQuery( "#swap-place-dialog" ).dialog("destroy");
    });


    jQuery("#sft-edit-panel").on('touchstart', function(event){
      event.stopPropagation();
    });

    jQuery("#ib-pagebtn-edit").on('touchstart', function(event){
      event.stopPropagation();
      //jQuery("#sft-edit-panel").toggleClass("sft-p-hidden");
      jQuery("#itibuilder-control").slideToggle("slow").removeClass("hidden")
      //Hide all other panels;
      jQuery("#sft-selpage-panel").addClass("sft-p-hidden");
      jQuery("#sft-export-panel").addClass("sft-p-hidden");
    });

    jQuery("#ib-pagebtn-sel").on('touchstart', function(event){
      event.stopPropagation();
      jQuery("#sft-selpage-panel").toggleClass("sft-p-hidden");
      //Hide all other panels;
      jQuery("#sft-edit-panel").addClass("sft-p-hidden");
      jQuery("#sft-export-panel").addClass("sft-p-hidden");
    });

    jQuery("#ib-pagebtn-export").on('touchstart', function(event){
      event.stopPropagation();
      //jQuery("#sft-export-panel").toggleClass("sft-p-hidden");
      //Hide all other panels;
      jQuery("#sft-edit-panel").addClass("sft-p-hidden");
      jQuery("#sft-selpage-panel").addClass("sft-p-hidden");

      //Open the email dialog
      email_iti_dialog.dialog( "option", "width", screen.width );
      email_iti_dialog.dialog( "open" );
    });

    //Pagination onclick buttons
    jQuery("#ib-pagebtn-prev").on('touchstart', function(event){
      show_ib_page_mobile(ib_current_page-1);
    });

    jQuery("#ib-pagebtn-next").on('touchstart', function(event){
      show_ib_page_mobile(ib_current_page+1);
    });

    /*
    jQuery("#ib-pagebtn-prev").click(function(event){
      event.stopPropagation();
      show_ib_page_mobile(ib_current_page-1);
    });

    jQuery("#ib-pagebtn-next").click(function(event){
      event.stopPropagation();
      show_ib_page_mobile(ib_current_page+1);
    });
    */

    show_iti_trail_mobile();
    adjustGoToDayBtnMobile();
    
    //If window width changes on mobile...
    jQuery( window ).resize(function() {
      adjustGoToDayBtnMobile();
    });

    //On mobile, we need to hide the dialog title bar
    jQuery(".ui-dialog-titlebar").hide();
  }
  else { //Its non mobile mode...
    //Unhide all day boxes
    jQuery(".ib-daybox").removeClass("hidden");
  }

  //On init, show the first page
  ib_current_page = 0;

  show_ib_page(0);
  show_iti_highlights();
  //show_iti_trail();


  jQuery( "#swap-place-dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "fade",
        duration: 500
      },
      position: { my: "right top", at: "right-5% top+5%", of: window },
      width: 200,
  });

  jQuery( "#swap-place-dialog" ).on('touchstart', function(event){
    event.stopPropagation();
  });

 
  var email_iti_dialog = jQuery( "#email-iti-dialog" ).dialog({
      autoOpen: false,
      show: {
        effect: "fade",
        duration: 500
      },
      maxWidth: 512,
      width: 512,
      buttons: {
        "Send": sendItiEmail,
        Close: function() {
          jQuery("#email-iti-loader").addClass("hidden");
          jQuery("#email-iti-success").addClass("hidden");
          jQuery("#email-iti-error").addClass("hidden");
          email_iti_dialog.dialog( "close" );
        }
      },
  });

  email_iti_dialog.on('touchstart', function(event){
    event.stopPropagation();
  });
 

  jQuery("#btn-toggleview-control").click(function(event){
    event.stopPropagation();
    jQuery("#itibuilder-control").slideToggle("slow").removeClass("hidden");
  });


  jQuery( ".ib-btn-swap" ).click(function(event) {
      event.stopPropagation();

      var in_place_id = jQuery(this).attr("place-id");
      var container_id = jQuery(this).parents(".ib-place-entry").first().attr("id");
      var place_swap_url = jQuery("#place_swap_url").val();

      jQuery( "#swap-place-dialog" ).dialog("destroy");
      var swap_place_dialog = jQuery( "#swap-place-dialog" ).dialog({
          autoOpen: false,
          show: {
            effect: "fade",
            duration: 500
          },
          position: { my: "right top", at: "right-5% top+5%", of: window },
          width: 200,
      });

      if (window.matchMedia('(max-width: 767px)').matches) {
        swap_place_dialog.dialog("option", "dialogClass", "swap-titlebar-hidden");
      }

      //Set the from-place name on the dialog
      var place_name = jQuery("#"+container_id+" .ib-place-title").html();
      jQuery( "#swap-from-placename" ).html(place_name);
      //Hide the swapchoice-container and show the loading text
      jQuery( "#swap-place-dialog .swapchoice-container" ).addClass("hidden");
      jQuery( "#swapchoice-loading" ).removeClass("hidden");

      //Open the dialog
      //jQuery( "#swap-place-dialog" ).attr("container-id", container_id)
      swap_place_dialog.attr("container-id", container_id)
        .dialog( "open" );

      //Build the list of places to exclude
      var ex_list = "";
      jQuery( ".ib-btn-swap" ).each(function() {
        ex_list += jQuery(this).attr("place-id") + ",";
      });

      jQuery.post( place_swap_url, 
          { place_id: in_place_id, exclude_list: ex_list })
        .done( function( data ) {
          var choice_data = JSON.parse(data);
          for(c_index=0; c_index<choice_data.length; c_index++){
            jQuery(".swapchoice-title").eq(c_index).html(choice_data[c_index]['title']);
            jQuery(".swapchoice-thumburl").eq(c_index).attr("src", choice_data[c_index]['image_full']);
            jQuery(".swapchoice-excerpt").eq(c_index).html(choice_data[c_index]['excerpt']);
            //Fill in the hidden fields
            //jQuery(".swapchoice-imgurl").eq(c_index).val(choice_data[c_index]['image_full']);
            jQuery(".swapchoice-place-id").eq(c_index).val(choice_data[c_index]['place_id']);
            jQuery(".swapchoice-prov-name").eq(c_index).val(choice_data[c_index]['prov_name']);
            jQuery(".swapchoice-prov-class").eq(c_index).val("tag-loc-" + choice_data[c_index]['prov_slug']);
            //Setup the 'swap' and 'detail' buttons
            jQuery("#scbtn_swap"+c_index).attr("onclick",
              "swap_place('" + c_index + "', '" + jQuery( "#swap-place-dialog" ).attr("container-id") + "')");
            jQuery("#scbtn_detail"+c_index).attr("href", choice_data[c_index]['permalink']);
          }

          //Hide the loader and show the entries
          jQuery( "#swapchoice-loading" ).addClass("hidden");
          jQuery( "#swap-place-dialog .swapchoice-container" ).removeClass("hidden");
        });


    jQuery('html').on('touchstart', function(event) {
      event.stopPropagation();
      jQuery( "#swap-place-dialog" ).dialog("close");
      jQuery('html').unbind('touchstart');
    });

  });
  

  jQuery( "#btn-export-pdf" ).click(function(event) {
      event.stopPropagation();

      /*
      var do_export_url = jQuery("#do_export_url").val();
      //Read out the day_ids part
      var itidata_str = jQuery("#post_iti_data").val();
      var cond_iti_data = JSON.parse(decodeURIComponent(itidata_str));
      var day_ids_str = JSON.stringify(cond_iti_data.day_ids);

      var arg1 = 'day_ids=' + encodeURIComponent(day_ids_str);
      var sp_str = JSON.stringify(swapped_places);
      var arg2 = 'swapped_places=' + encodeURIComponent(sp_str);

      window.open(do_export_url+'?'+arg1+'&'+arg2, '_blank');
      */
      email_iti_dialog.dialog( "open" );

  });

  jQuery( "#btn-export-pdf2" ).click(function(event) {
      event.stopPropagation();
      email_iti_dialog.dialog( "open" );
  });


  jQuery( "#export-iti-getpdf" ).click(function(event) {
      event.stopPropagation();

      var do_export_url = jQuery("#do_export_url").val();
      //Read out the day_ids part
      var itidata_str = jQuery("#post_iti_data").val();
      var cond_iti_data = JSON.parse(decodeURIComponent(itidata_str));
      var day_ids_str = JSON.stringify(cond_iti_data.day_ids);

      var arg1 = 'day_ids=' + encodeURIComponent(day_ids_str);
      var sp_str = JSON.stringify(swapped_places);
      var arg2 = 'swapped_places=' + encodeURIComponent(sp_str);

      window.open(do_export_url+'?'+arg1+'&'+arg2, '_blank');
  });


});



/* OLD CODE

function append_prov_visited(sel_prov_id){
  //Get the index that will be assigned to this new prov box
  var new_prov_index = jQuery(".selected_prov").length;

  jQuery("#prov_visited_box").append('<div class="selected_prov"></div>')

  //Set the contents of the new entry
  var p_content = jQuery(".selected_prov").html();
  jQuery(".selected_prov").eq(new_prov_index).html(p_content);

  assignNewIndexes();

  //Set the selected value
  jQuery("select[name='prov-sel-" + new_prov_index + "']").val(sel_prov_id);

  //Reset the Adding box to default
  jQuery("#prov_add").val(0);
}


function assignNewIndexes() {
  jQuery( ".selected_prov" ).each(function( prov_index ) {
    //Set the name of the select box
    jQuery(this).find("select").attr("name", "prov-sel-" + prov_index);
    //Set the onclick function for delete
    jQuery(this).find(".btn_del_prov").attr("onclick", "deleteVisitedProv(" + prov_index + ")");
    //Set the on change function for <select>
    jQuery(this).find(".loc-select").change(function(){
      doItiBuildSubmit();
    });
  });
}


function deleteVisitedProv(prov_index) {
  if(jQuery(".selected_prov").length <= 1){
      alert("Delete not allowed. There must be at least 1 province.");
    }
    else {
      jQuery(".selected_prov").eq( prov_index ).remove();
      assignNewIndexes();
    }
}



//Setup sidebar
  jQuery('#sticky-sidebar').stickySidebar({
    sidebarTopMargin: 40,
    footerThreshold: 40
  });
  

  //Set the sidebar size to match the parent
  var parentwidth = jQuery("#sidebar-parent").width();
  jQuery('#sticky-sidebar').width(parentwidth);
  //In case the window resizes..
  jQuery(window).resize(function(){
    var parentwidth = jQuery("#sidebar-parent").width();
    jQuery('#sticky-sidebar').width(parentwidth);
  });


  //handle clicks for adding/removing provinces
  jQuery("#btn_add_prov").click(function(event){
    event.preventDefault();
    var sel_id = jQuery("#prov_add").val();
    //Add a new entry with selection
    if(sel_id != 0){
      if(hasProv(sel_id)){
        alert("Province already exist");
      }
      else {
        append_prov_visited(sel_id);
      }
    }
  });


 //Allow auto submit when any input changes
  jQuery("select[name='arv-airport']").change(function(){
    doItiBuildSubmit();
  });

  jQuery("select[name='num-days']").change(function(){
    doItiBuildSubmit();
  });

  jQuery("input[name='pref-shopping']").change(function(){
    doItiBuildSubmit();
  });

  jQuery("input[name='pref-sightsee']").change(function(){
    doItiBuildSubmit();
  });

  jQuery("input[name='pref-funrelax']").change(function(){
    doItiBuildSubmit();
  });
*/

