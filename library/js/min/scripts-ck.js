if (!window.getComputedStyle) {
    window.getComputedStyle = function(t, e) {
        this.el = t;
        this.getPropertyValue = function(e) {
            var n = /(\-([a-z]){1})/g;
            if (e === "float") {
                e = "styleFloat";
            }
            if (n.test(e)) {
                e = e.replace(n, function() {
                    return arguments[2].toUpperCase();
                });
            }
            return t.currentStyle[e] ? t.currentStyle[e] : null;
        };
        return this;
    };
}

jQuery(document).ready(function(t) {
    var e = t(window).width();
    if (e < 481) {}
    if (e > 481) {}
    if (e >= 768) {
        t(".comment img[data-gravatar]").each(function() {
            t(this).attr("src", t(this).attr("data-gravatar"));
        });
    }
    if (e > 1030) {}
});

(function(t) {
    if (!(/iPhone|iPad|iPod/.test(navigator.platform) && navigator.userAgent.indexOf("AppleWebKit") > -1)) {
        return;
    }
    var e = t.document;
    if (!e.querySelector) {
        return;
    }
    var n = e.querySelector("meta[name=viewport]"), i = n && n.getAttribute("content"), a = i + ",maximum-scale=1", r = i + ",maximum-scale=10", o = true, u, c, f, s;
    if (!n) {
        return;
    }
    function l() {
        n.setAttribute("content", r);
        o = true;
    }
    function d() {
        n.setAttribute("content", a);
        o = false;
    }
    function m(e) {
        s = e.accelerationIncludingGravity;
        u = Math.abs(s.x);
        c = Math.abs(s.y);
        f = Math.abs(s.z);
        if (!t.orientation && (u > 7 || (f > 6 && c < 8 || f < 8 && c > 6) && u > 5)) {
            if (o) {
                d();
            }
        } else if (!o) {
            l();
        }
    }
    t.addEventListener("orientationchange", l, false);
    t.addEventListener("devicemotion", m, false);
})(this);