//For each POI in the itinerary, there are 2 inputs:
//Both have attribute "name" set uniquely accordingly to their position in the itinerary
// 1. Place Name :  From <input> tag with name = placeinput_<day_index>_<place_index>
// 1. Place ID :  From hidden <input> tag with name = id_placeinput_<day_index>_<place_index>

function populateIti(json_str_input) {
    var iti_data = JSON.parse(json_str_input);
    for(day_index=0; day_index<iti_data.length; day_index++){
        //Create new day element
        addDay();
        for(place_index=0; place_index<iti_data[day_index].length; place_index++){
            //console.log(" " + day_index + "_" + place_index + " = " + iti_data[day_index][place_index]['label']);
            if(place_index > 0){//Skip first iter because addDay() already adds 1 new place by default
                addPlace(day_index);
            }
            jQuery('[name="placeinput_' + day_index + '_' + place_index + '"]')
                .val(iti_data[day_index][place_index]['label']);
            jQuery('[name="id_placeinput_' + day_index + '_' + place_index + '"]')
                .val(iti_data[day_index][place_index]['id']);
        }
    }
}

function count_days_places() {
    var num_days = jQuery(".admin_sampiti_day").length;
    jQuery("#num_days").empty().append(num_days);
    //Set the num of days in the hidden input
    jQuery('[name="admin_sampiti_numdays"]').val(num_days);

    jQuery( ".admin_sampiti_day" ).each(function( index ) {
        var num_places = jQuery(this).find(".admin_sampiti_place").length;
        jQuery(this).find(".num_places").empty().append(num_places);
        //Set the number of places in the hidden input
        jQuery('[name="admin_sampiti_numplaces_' + index + '"]').val(num_places);

        //Place the day number
        var day_num = index + 1;
        jQuery(this).children(".admin_sampiti_day_header").empty().append("Day " + day_num);
    });
    
}


function assignNewPosIndexes() {
    jQuery( ".admin_sampiti_day" ).each(function( day_index ) {
        //Set the button onclick attributes
        jQuery(this).find(".admin_sampiti_btndel").attr("onclick", "deleteDay(" + day_index + ")");
        jQuery(this).find(".admin_sampiti_btnaddplace").attr("onclick", "addPlace(" + day_index + ")");

        //Each .admin_sampiti_day has a hidden input to store the number of places within that day
        jQuery(this).find(".admin_sampiti_numplaces").attr("name", "admin_sampiti_numplaces_" + day_index);

        //Loop through each place and set the attributes for each place
        jQuery(this).find(".admin_sampiti_place").each(function( place_index ) {
            //Each .admin_sampiti_place will be assigned a unique id according to its indexes
            var asp_id = 'asp' + day_index + '_' + place_index;
            jQuery(this).attr("id", asp_id);

            //Assign its delete button event
            jQuery(this).find(".admin_sampiti_btndelplace").attr("onclick", 'deletePlace("' + asp_id + '")');
            //Assign a name for the inputs according to its indexes
            jQuery(this).find(".admin_sampiti_placeinput").attr("name", "placeinput_" + day_index + "_" + place_index);
            jQuery(this).find(".admin_sampiti_placeinput_id").attr("name", "id_placeinput_" + day_index + "_" + place_index);
        });

    });

    hhp_samp_iti_attach_event();
}




//Add a new input for Place in the specified day number
//day_index assumes 0-based index
function addPlace(day_index) {
    //This is the new place number added (eq() uses 0 based index)
    //var placeNum = jQuery(".admin_sampiti_day").eq( day_index ).find(".admin_sampiti_place").length + 1;

    jQuery(".admin_sampiti_day").eq( day_index ).append(
        '<tr class="admin_sampiti_place">' +
        '<th>Place Name </th>' +
        '<td><input class="admin_sampiti_placeinput" type="text" size="60"/></td>' +
        '<td><button class="admin_sampiti_btndelplace" type="button">Delete</button></td>' +
        '<input class="admin_sampiti_placeinput_id" type="hidden"/>' +
        '</tr>'
        );

    count_days_places();
    assignNewPosIndexes();
}


function addDay() {

    jQuery("#admin_sampiti_container").append('<div class="admin_sampiti_day">' + 
        '<button class="admin_sampiti_btndel" type="button">Delete day</button>' +
        '<h3 class="admin_sampiti_day_header"></h3>' +
        '<p>Number of places: <span class="num_places"></span></p>' +
        '<input type="hidden" class="admin_sampiti_numplaces" />' +
        '<table>' + 
        '<tr><td><button class="admin_sampiti_btnaddplace" type="button">Add place</button></td></tr>' +
        '</table><br />'
        );

    addPlace(jQuery(".admin_sampiti_day").length-1);
}


//Delete a whole day
//day_index assumes 0-based index
function deleteDay(day_index) {
    var result = window.confirm("The entire day and its contents will be deleted. Are you sure?");

    if(result == true) {
        jQuery(".admin_sampiti_day").eq( day_index ).remove();
        count_days_places();
        assignNewPosIndexes();
    }
}

//Delete a single place from a specified day
//
function deletePlace(id_to_delete) {
    var result = window.confirm("This place will be deleted from itinerary. Are you sure?");

    if(result == true) {
        jQuery("#"+id_to_delete).remove();
        count_days_places();
        assignNewPosIndexes();
    }
}

