window.fbAsyncInit = function() {
    FB.init({
        appId: "573561169383977",
        status: true,
        cookie: true,
        xfbml: true,
        version: "v1.0"
    });
    FB.Event.subscribe("auth.authResponseChange", function(e) {
        if (e.status === "connected") {
            handleConnectedUser();
        }
    });
};

function handleConnectedUser() {
    jQuery("#fb_login_form").empty().append('<a href="/wp-content/themes/hhp-bones/signon-with-fb.php">' + '<div id="btn_login_with_fb"><span class="icon-login-fb"></span>Log In with Facebook</div></a>');
}

function gotoSignon() {
    jQuery("#fb_login_form").empty().append("<p>Signing you in ...</p>" + '<div id="fb_login_loading"></div>');
    window.location.href = "/wp-content/themes/hhp-bones/signon-with-fb.php";
    return false;
}

(function(e) {
    var n, t = "facebook-jssdk", o = e.getElementsByTagName("script")[0];
    if (e.getElementById(t)) {
        return;
    }
    n = e.createElement("script");
    n.id = t;
    n.async = true;
    n.src = "//connect.facebook.net/en_US/all.js";
    o.parentNode.insertBefore(n, o);
})(document);

jQuery(document).ready(function() {
    var e = new Date();
    jQuery("#user_dob").datepicker({
        showOn: "both",
        buttonImage: "/wp-content/themes/hhp-bones/library/images/calendar.gif",
        buttonImageOnly: true,
        changeMonth: true,
        changeYear: true,
        minDate: "-100Y",
        maxDate: "-1Y",
        yearRange: "1900:" + e.getFullYear()
    });
});