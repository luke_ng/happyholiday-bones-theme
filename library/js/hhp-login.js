window.fbAsyncInit = function() {
	FB.init({
		appId      : '573561169383977',
		status     : true, // check login status
		cookie     : true, // enable cookies to allow the server to access the session
		xfbml      : true,  // parse XFBML
		version    : 'v1.0' // use version 1.0
	});



  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
  // for any authentication related change, such as login, logout or session refresh. This means that
  // whenever someone who was previously logged out tries to log in again, the correct case below 
  // will be handled. 
  FB.Event.subscribe('auth.authResponseChange', function(response) {
    // Here we specify what we do with the response anytime this event occurs. 
    if (response.status === 'connected') {
      // The response object is returned with a status field that lets the app know the current
      // login status of the person. In this case, we're handling the situation where they 
      // have logged in to the app.
      handleConnectedUser();
    }
  });
	
};

function handleConnectedUser() {

  jQuery("#fb_login_form").empty().append('<a href="/wp-content/themes/hhp-bones/signon-with-fb.php">' + 
    '<div id="btn_login_with_fb"><span class="icon-login-fb"></span>Log In with Facebook</div></a>');
  //
}

function gotoSignon() {
  //console.log("gotoSignon() called.");
  //Show loading bar
  jQuery("#fb_login_form").empty().append('<p>Signing you in ...</p>' + 
    '<div id="fb_login_loading"></div>');

  //Load signon URL
  window.location.href = "/wp-content/themes/hhp-bones/signon-with-fb.php";
  return false;
}

// Load the SDK asynchronously
(function(d){
  var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement('script'); js.id = id; js.async = true;
  js.src = "//connect.facebook.net/en_US/all.js";
  ref.parentNode.insertBefore(js, ref);
}(document));

jQuery( document ).ready(function() {
  var today = new Date();

  jQuery( "#user_dob" ).datepicker({
    showOn: "both",
    buttonImage: "/wp-content/themes/hhp-bones/library/images/calendar.gif",
    buttonImageOnly: true,
    changeMonth: true,
    changeYear: true,
    minDate: "-100Y", 
    maxDate: "-1Y",
    yearRange: "1900:"+today.getFullYear(),
  });
});