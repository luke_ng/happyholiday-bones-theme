//Javascript for use in the admin page


function fillBranchData(branch_num, p_address, p_map_url, p_map_iframe, p_coords, p_op_hours, p_phone, p_fax) {
	//All newline chars will be replaced by <br /> when passed to this function
	//so we need to replace them, for fieldsa that are multi-line

	var regex = /<br\s*[\/]?>/gi;

	p_address = p_address.replace(regex, "\r\n");
	p_op_hours = p_op_hours.replace(regex, "\r\n");
	p_phone = p_phone.replace(regex, "\r\n");
	p_fax = p_fax.replace(regex, "\r\n");

	jQuery('#place_address_' + branch_num).val(p_address);
	jQuery('#place_map_url_' + branch_num).val(p_map_url);
	jQuery('#place_map_iframe_' + branch_num).val(p_map_iframe);
	jQuery('#place_coordinates_' + branch_num).val(p_coords);
	jQuery('#place_operating_hours_' + branch_num).val(p_op_hours);
	jQuery('#place_phone_' + branch_num).val(p_phone);
	jQuery('#place_fax_' + branch_num).val(p_fax);

}


function addBranch(branch_num) {
	//alert("Adding branch!");
	jQuery("#hhp_branches").append('<div id="branch' + branch_num + '"></div>');

	
	var btn_del = jQuery('<button/>',
    {
        text: 'Delete branch',
        class: 'btn_delete',
        click: function(){ deleteBranch(branch_num); return false; }
    });

	
	jQuery('#branch'+branch_num).append('<h3 class="metabox_branch_title">Branch ' + branch_num + '</h3>');

	jQuery('#branch'+branch_num).append(btn_del);

	jQuery('#branch'+branch_num).append('<table class="metabox_branch_table">' +
			//Field: Address
			'<tr><td style="width: 100%">Address</td>' +
			'<td><textarea class="place_address" rows="3" cols="60" id="place_address_' + branch_num +
			'" name="place_address_' + branch_num + '" ></textarea></td></tr>' +
			//Field: Map URL
			'<tr><td style="width: 100%">Map URL</td>' +
			'<td><input class="place_map_url" type="text" size="61" id="place_map_url_' + branch_num +
			'" name="place_map_url_' + branch_num + '" value=""/></td></tr>' +
			//Field: Map iframe
			'<tr><td style="width: 100%">Map iframe</td>' +
			'<td><input class="place_map_iframe" type="text" size="61" id="place_map_iframe_' + branch_num +
			'" name="place_map_iframe_' + branch_num + '" value=""/></td></tr>' +
			//Field: Coordinates
			'<tr><td style="width: 100%">Coordinates</td>' +
			'<td><input class="place_coordinates" type="text" size="61" id="place_coordinates_' + branch_num +
			'" name="place_coordinates_' + branch_num + '" value=""/></td></tr>' +
			//Field: Operating hours
			'<tr><td style="width: 100%">Operating hours</td>' +
			'<td><textarea class="place_operating_hours" rows="3" cols="60" id="place_operating_hours_' + branch_num +
			'" name="place_operating_hours_' + branch_num + '" value=""></textarea></td></tr>' +
			//Field: Phone numbers
			'<tr><td style="width: 100%">Phone numbers</td>' +
			'<td><textarea class="place_phone" rows="3" cols="60" id="place_phone_' + branch_num +
			'" name="place_phone_' + branch_num + '" value=""></textarea></td></tr>' +
			//Field: Fax numbers
			'<tr><td style="width: 100%">Fax numbers</td>' +
			'<td><textarea class="place_fax" rows="3" cols="60" id="place_fax_' + branch_num +
			'" name="place_fax_' + branch_num + '" value=""></textarea></td></tr>' +

			'</table><br /><br />'
		);

	//Update the number of branches field
	jQuery('#place_num_branch').val(branch_num);


	//saveBranchData();

	//var new_branch_num = jQuery("#hhp_place_branch_sel option").size() + 1; 

	//jQuery("#hhp_place_branch_sel").append('<option value="' + new_branch_num 
		//+ '" >Branch ' + new_branch_num + '</option>');
	//init_empty_branch_data();
	
	//Set the selection to the new branch
	//jQuery("#hhp_place_branch_sel").val(new_branch_num);
}

function deleteBranch(branch_num) {
	//alert("Delete branch " + branch_num);
	jQuery("#branch"+branch_num).remove();
	//Update the number of branches field
	var num_branches = jQuery('#place_num_branch').val();
	jQuery('#place_num_branch').val(num_branches-1);
	assignNewPosIndexesBranch();
}

function assignNewPosIndexesBranch() {
    jQuery( "#hhp_branches > div" ).each(function( div_index ) {
    	var b_index = 1+div_index;
    	jQuery(this).find(".metabox_branch_title").text("Branch " + b_index);
    	jQuery(this).find(".place_address").attr("id", "place_address_" + b_index);
    	jQuery(this).find(".place_address").attr("name", "place_address_" + b_index);
    	jQuery(this).find(".place_map_url").attr("id", "place_map_url_" + b_index);
    	jQuery(this).find(".place_map_url").attr("name", "place_map_url_" + b_index);
    	jQuery(this).find(".place_map_iframe").attr("id", "place_map_iframe_" + b_index);
    	jQuery(this).find(".place_map_iframe").attr("name", "place_map_iframe_" + b_index);
    	jQuery(this).find(".place_coordinates").attr("id", "place_coordinates_" + b_index);
    	jQuery(this).find(".place_coordinates").attr("name", "place_coordinates_" + b_index);
    	jQuery(this).find(".place_operating_hours").attr("id", "place_operating_hours_" + b_index);
    	jQuery(this).find(".place_operating_hours").attr("name", "place_operating_hours_" + b_index);
    	jQuery(this).find(".place_phone").attr("id", "place_phone_" + b_index);
    	jQuery(this).find(".place_phone").attr("name", "place_phone_" + b_index);
    	jQuery(this).find(".place_fax").attr("id", "place_fax_" + b_index);
    	jQuery(this).find(".place_fax").attr("name", "place_fax_" + b_index);
    });
}


function fillTodoData(todo_num, todo_type, todo_img_url, todo_text) {
	//All newline chars will be replaced by <br /> when passed to this function
	//so we need to replace them, for fieldsa that are multi-line

	var regex = /<br\s*[\/]?>/gi;

	todo_text = todo_text.replace(regex, "\r\n");

	jQuery('#place_todo_type_' + todo_num).val(todo_type);
	jQuery('#place_todo_img_' + todo_num).val(todo_img_url);
	jQuery('#place_todo_text_' + todo_num).val(todo_text);
}


function addTodo(todo_num) {
	//alert("Adding todo!");
	jQuery("#hhp_todo_container").append('<div id="todo' + todo_num + '"></div>');

	var btn_del = jQuery('<button/>',
    {
        text: 'Delete To-do',
        class: 'btn_delete',
        click: function(){ deleteTodo(todo_num); return false; }
    });

	
	jQuery('#todo'+todo_num).append('<h3 class="metabox_branch_title">#' + todo_num + ' To Do</h3>');

	jQuery('#todo'+todo_num).append(btn_del);

	jQuery('#todo'+todo_num).append('<table class="metabox_branch_table">' +
			//Field: To Do Type
			'<tr><td style="width: 100%">To-do Type</td>' +
			'<td><select class="admin_todo_typesel" id="place_todo_type_' + todo_num +
			'" name="place_todo_type_' + todo_num + '">' + 
            '<option value="Sightseeing">Sightseeing</option>' +
            '<option value="Shopping">Shopping</option>' +
            '<option value="Fun and relax">Fun and relax</option>' +
            '<option value="Eating">Eating</option>' +
            '<option value="Accommodation">Accommodation</option>' +
            '<option value="Useful Tip">Useful Tip</option>' +
            '</select></td></tr>' +
			//Field: Image URL
			'<tr><td style="width: 100%">To-do Image URL</td>' +
			'<td><input class="admin_todo_img" type="text" size="61" id="place_todo_img_' + todo_num +
			'" name="place_todo_img_' + todo_num + '" value=""/></td></tr>' +
			//Field: Description
			'<tr><td style="width: 100%">To-do Description</td>' +
			'<td><textarea class="admin_todo_text" rows="3" cols="60" id="place_todo_text_' + todo_num +
			'" name="place_todo_text_' + todo_num + '" ></textarea></td></tr>' +

			'</table><br /><br />'
		);

	//Update the number of todos
	jQuery('#place_num_todo').val(todo_num);
}


function deleteTodo(todo_num) {
	//alert("Delete branch " + branch_num);
	jQuery("#todo"+todo_num).remove();
	//Update the number of branches field
	var num_todos = jQuery('#place_num_todo').val();
	jQuery('#place_num_todo').val(num_todos-1);
	assignNewPosIndexesTodo();
}


function assignNewPosIndexesTodo() {
    jQuery( "#hhp_todo_container > div" ).each(function( div_index ) {
    	var todo_index = 1+div_index;
    	jQuery(this).find(".metabox_branch_title").text("#" + todo_index + " To Do");
    	jQuery(this).find(".admin_todo_typesel").attr("id", "place_todo_type_" + todo_index);
    	jQuery(this).find(".admin_todo_typesel").attr("name", "place_todo_type_" + todo_index);
    	jQuery(this).find(".admin_todo_img").attr("id", "place_todo_img_" + todo_index);
        jQuery(this).find(".admin_todo_img").attr("name", "place_todo_img_" + todo_index);
        jQuery(this).find(".admin_todo_text").attr("id", "place_todo_text_" + todo_index);
        jQuery(this).find(".admin_todo_text").attr("name", "place_todo_text_" + todo_index);
    });
}




function fillTodoData(todo_num, todo_type, todo_img_url, todo_text) {
	//All newline chars will be replaced by <br /> when passed to this function
	//so we need to replace them, for fieldsa that are multi-line

	var regex = /<br\s*[\/]?>/gi;

	todo_text = todo_text.replace(regex, "\r\n");

	jQuery('#place_todo_type_' + todo_num).val(todo_type);
	jQuery('#place_todo_img_' + todo_num).val(todo_img_url);
	jQuery('#place_todo_text_' + todo_num).val(todo_text);
}


function addTodo(todo_num) {
	//alert("Adding todo!");
	jQuery("#hhp_todo_container").append('<div id="todo' + todo_num + '"></div>');

	var btn_del = jQuery('<button/>',
    {
        text: 'Delete To-do',
        class: 'btn_delete',
        click: function(){ deleteTodo(todo_num); return false; }
    });

	
	jQuery('#todo'+todo_num).append('<h3 class="metabox_branch_title">#' + todo_num + ' To Do</h3>');

	jQuery('#todo'+todo_num).append(btn_del);

	jQuery('#todo'+todo_num).append('<table class="metabox_branch_table">' +
			//Field: To Do Type
			'<tr><td style="width: 100%">To-do Type</td>' +
			'<td><select id="place_todo_type_' + todo_num +
			'" name="place_todo_type_' + todo_num + '">' + 
            '<option value="Sightseeing">Sightseeing</option>' +
            '<option value="Shopping">Shopping</option>' +
            '<option value="Fun and relax">Fun and relax</option>' +
            '<option value="Eating">Eating</option>' +
            '<option value="Accommodation">Accommodation</option>' +
            '</select></td></tr>' +
			//Field: Image URL
			'<tr><td style="width: 100%">To-do Image URL</td>' +
			'<td><input type="text" size="61" id="place_todo_img_' + todo_num +
			'" name="place_todo_img_' + todo_num + '" value=""/></td></tr>' +
			//Field: Description
			'<tr><td style="width: 100%">To-do Description</td>' +
			'<td><textarea rows="3" cols="60" id="place_todo_text_' + todo_num +
			'" name="place_todo_text_' + todo_num + '" ></textarea></td></tr>' +

			'</table><br /><br />'
		);

	//Update the number of todos
	jQuery('#place_num_todo').val(todo_num);
}


function deleteTodo(todo_num) {
	//alert("Delete branch " + branch_num);
	jQuery("#todo"+todo_num).remove();
	//Update the number of branches field
	var num_todos = jQuery('#place_num_todo').val();
	jQuery('#place_num_todo').val(num_todos-1);
}

