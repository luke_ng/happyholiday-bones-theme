
jQuery( document ).ready(function() {

  	jQuery( "#searchform" ).on('submit', function( event ) {
  		show_loader();
		event.preventDefault();
		var s_params = jQuery( this ).serialize();
		jQuery.get( "<?php echo get_template_directory_uri(); ?>/search-pre.php", s_params )
		.done( function( data ) {
			//returns the processed parameters in 'data'
			window.location.href = '<?php echo home_url("/"); ?>' + data;
		});
		//execute_search(jQuery( this ).serialize());
	  	//jQuery("#search-results").empty().append( jQuery( this ).serialize() );
	});
	
} );


function show_loader() {
	jQuery("#search-results").empty()
	.append('<div class="loader"><img src="' + 
		'<?php echo get_template_directory_uri(); ?>/library/images/round-loader.gif" /><p>Searching</p></div>')
	.fadeIn('slow');
}
