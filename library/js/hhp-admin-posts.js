jQuery( document ).ready(function() {

  jQuery( "#promo-from" ).datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: "-1Y",
    maxDate: "+5Y",
  });

  jQuery( "#promo-to" ).datepicker({
    changeMonth: true,
    changeYear: true,
    minDate: "-1Y",
    maxDate: "+5Y",
  });

});