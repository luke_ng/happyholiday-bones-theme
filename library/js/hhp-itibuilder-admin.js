// JS file for itibuilder-admin

jQuery(document).ready(function() {

	//-------------------------------
	// For province form
	//-------------------------------

	//When province selection changes, load the correct page
	jQuery("#term_id").change(function(){
		var prov_id = jQuery( this ).val();
		var load_url = window.location.protocol + "//" + window.location.host + window.location.pathname;
		//Add query parameters to url
		load_url = load_url + "?page=itibuilder-province&province=" + prov_id;
		window.location.href = load_url;
	});

	//Auto fill prov trait
    /*
	jQuery("#prov_trait_shopping").focusout(function(event){
		var curr_p = jQuery(event.target).val();
		if(curr_p.length > 0 && curr_p >= 0 && curr_p <= 100){
			jQuery("#prov_trait_sightsee").val(100-curr_p);
		}
	});

	jQuery("#prov_trait_sightsee").focusout(function(event){
		var curr_p = jQuery(event.target).val();
		if(curr_p.length > 0 && curr_p >= 0 && curr_p <= 100){
			jQuery("#prov_trait_shopping").val(100-curr_p);
		}
	});
    */

	//Province form input validation
    /*
	jQuery("#prov_form").submit(function(event){
		//Check that the percentages add to 100
		var shop_p = jQuery("#prov_trait_shopping").val();
		var sight_p = jQuery("#prov_trait_sightsee").val();

		if(parseInt(shop_p) + parseInt(sight_p) != 100){
			showError("Shopping and sightseeing % must add up to 100!");
			event.preventDefault();
		}

		return;
	});
    */


	//-------------------------------
	// For itnerary day form
	//-------------------------------

    //Initialise by creating a place in the first province
    addPlace(0);
    //If #places_data has any value assigned, run the populateIti()
    var places_data_val = jQuery("#places_data").val();
    if(places_data_val != null && places_data_val.length > 2){
        populateIti(places_data_val);
    }



});


//-------------------------------
// Shared Functions for 
// province form, itinerary day form
//-------------------------------

function showError(err_string){
	jQuery("#error_message p").empty().text(err_string);
	jQuery("#error_message").show();
}

//-------------------------------
// Functions for itinerary day view form
//-------------------------------

function delete_day_iti(day_id){
    return confirm("Are you sure u want to delete entry (day_id="+day_id+") ?");
}

//-------------------------------
// Functions for itinerary day form
//-------------------------------

function hhp_samp_iti_attach_event() {
	var places_lookup_url = jQuery("#places_lookup_url").val();
    //Also add the event listener for autocomplete
    jQuery(".admin_itibuilder_placeinput").autocomplete({
        source: places_lookup_url,
        minLength: 3,
        select: function(event, ui) {
            if(ui.item){
                var target = jQuery(event.target);
                var tname = target.attr("name");
                //Sets the post id in the hidden field
                jQuery("input[name='id_" + tname + "']").val(ui.item.id);
                //Make the text blue, to show that a valid post id has been assigned
                target.css("color","darkblue");
                target.css("font-weight", "bold");
            }
            //alert( ui.item ?
            //        "Selected: " + ui.item.value + " aka " + ui.item.id + " custom " + ui.item.custom :
            //        "Nothing selected, input was " + this.value );
        }
    });

    jQuery(".admin_itibuilder_placeinput").change(function(event){
        var target = jQuery(event.target);
        var tname = target.attr("name");
        //Removes the post id in the hidden field
        jQuery("input[name='id_" + tname + "']").val("0");
        //Make the text blue, to show that a valid post id has been assigned
        target.css("color","black");
        target.css("font-weight", "normal");
    });
}


//For each POI in the itinerary, there are 2 inputs:
//Both have attribute "name" set uniquely accordingly to their position in the itinerary
// 1. Place Name :  From <input> tag with name = placeinput_<prov_index>_<place_index>
// 1. Place ID :  From hidden <input> tag with name = id_placeinput_<prov_index>_<place_index>

function populateIti(json_str_input) {
    console.log("populateIti() received json input: " + json_str_input);
    var iti_data = JSON.parse(json_str_input);
    for(prov_index=0; prov_index<iti_data.length; prov_index++){
        //Create new day element (except for the first loop)
        if(prov_index > 0){
            addProv();
        }

        //Display the province name
        jQuery(".admin_itibuilder_provsel").eq(prov_index).val(iti_data[prov_index]['province']);

        //Display the prov_timespent
        jQuery(".admin_prov_timespent").eq(prov_index).val(iti_data[prov_index]['timespent']);

        //Display the places
        for(place_index=0; place_index<iti_data[prov_index]['places'].length; place_index++){
            //console.log(" " + prov_index + "_" + place_index + " = " + iti_data[prov_index][place_index]['label']);
            if(place_index > 0){//Skip first iter because addDay() already adds 1 new place by default
                addPlace(prov_index);
            }
            jQuery('[name="placeinput_' + prov_index + '_' + place_index + '"]')
                .val(iti_data[prov_index]['places'][place_index]['label']);
            jQuery('[name="id_placeinput_' + prov_index + '_' + place_index + '"]')
                .val(iti_data[prov_index]['places'][place_index]['id']);

            if(iti_data[prov_index]['places'][place_index]['id'] > 0){
                jQuery('[name="placeinput_' + prov_index + '_' + place_index + '"]').css("color","darkblue");
                jQuery('[name="placeinput_' + prov_index + '_' + place_index + '"]').css("font-weight", "bold");
            }
            else {
                jQuery('[name="placeinput_' + prov_index + '_' + place_index + '"]').css("color","black");
                jQuery('[name="placeinput_' + prov_index + '_' + place_index + '"]').css("font-weight", "normal");
            }
        }
    }
}


function assignNewPosIndexes() {
    jQuery( ".admin_itibuilder_prov" ).each(function( prov_index ) {
        //Set the button onclick attributes
        jQuery(this).find(".admin_itibuilder_btndelprov").attr("onclick", "deleteProv(" + prov_index + ")");
        jQuery(this).find(".admin_itibuilder_btnaddplace").attr("onclick", "addPlace(" + prov_index + ")");

        //Each .admin_itibuilder_prov has a hidden input to store the number of places within that province
        jQuery(this).find(".admin_itibuilder_numplaces").attr("name", "admin_itibuilder_numplaces_" + prov_index);

        //Each select tag must be named according to prov index
        jQuery(this).find(".admin_itibuilder_provsel").attr("name", "prov_id_" + prov_index);

        //Each prov_timespent input must be named according to prov_index
        jQuery(this).find(".admin_prov_timespent").attr("name", "prov_timespent_" + prov_index);

        //Loop through each place and set the attributes for each place
        jQuery(this).find(".admin_itibuilder_place").each(function( place_index ) {
            //Each .admin_itibuilder_place will be assigned a unique id according to its indexes
            var aip_id = 'aip' + prov_index + '_' + place_index;
            jQuery(this).attr("id", aip_id);

            //Assign its delete button event
            jQuery(this).find(".admin_itibuilder_btndelplace").attr("onclick", 'deletePlace("' + aip_id + '")');
            //Assign a name for the inputs according to its indexes
            jQuery(this).find(".admin_itibuilder_placeinput").attr("name", "placeinput_" + prov_index + "_" + place_index);
            jQuery(this).find(".admin_itibuilder_placeinput_id").attr("name", "id_placeinput_" + prov_index + "_" + place_index);
        });

    });

    hhp_samp_iti_attach_event();
}


function count_prov_places() {
    var num_prov = jQuery(".admin_itibuilder_prov").length;
    jQuery("#num_prov").empty().append(num_prov);
    //Set the num of days in the hidden input
    jQuery('[name="admin_itibuilder_numprov"]').val(num_prov);

    jQuery( ".admin_itibuilder_prov" ).each(function( index ) {
        var num_places = jQuery(this).find(".admin_itibuilder_place").length;
        jQuery(this).find(".num_places").empty().append(num_places);
        //Set the number of places in the hidden input
        jQuery('[name="admin_itibuilder_numplaces_' + index + '"]').val(num_places);

        //Place the prov number
        //var prov_num = index + 1;
        //jQuery(this).children(".admin_itibuilder_day_header").empty().append("Day " + day_num);

        //Place the [Start province] , [End Province] headers
        jQuery(this).find(".prov_meta").empty();
        if(index == 0){
        	jQuery(this).find(".prov_meta").append("[Start province] ");
        }
        if(num_prov == index+1){
        	jQuery(this).find(".prov_meta").append("[End province] ");
        }
        
    });
    
}

//Add a new input for Place in the specified province number
//prov_index assumes 0-based index
function addPlace(prov_index) {
    //This is the new place number added (eq() uses 0 based index)
    //var placeNum = jQuery(".admin_sampiti_day").eq( prov_index ).find(".admin_sampiti_place").length + 1;

    jQuery(".admin_itibuilder_prov").eq( prov_index ).append(
        '<p class="admin_itibuilder_place">' +
        '<label>Place Name </label>' +
        '<input class="admin_itibuilder_placeinput" type="text" size="60"/>' +
        '<button class="admin_itibuilder_btndelplace" type="button">Delete</button>' +
        '<input class="admin_itibuilder_placeinput_id" type="hidden"/>' +
        '</p>'
        );

    assignNewPosIndexes();
    count_prov_places();
}

function addProv() {

    //Get the index that will be assigned to this new prov
    var new_prov_index = jQuery(".admin_itibuilder_prov").length;

    jQuery("#admin_day_container").append('<div class="admin_itibuilder_prov">' + 
        '<input class="admin_itibuilder_numplaces" type="hidden"/>' +
        '<button type="button" class="admin_itibuilder_btndelprov">Delete Province</button>' +
        '<h3 class="prov_meta"></h3>' +
        '<label>Province</label>' +
        '<select class="admin_itibuilder_provsel"></select>' +
        '<p><label>% time spent in province</label><input class="admin_prov_timespent" type="text" /></p>' +
        '<p><button class="admin_itibuilder_btnaddplace" type="button">Add place</button><p>' +
        '</div>'
        );

    //Set the options in the select tag
    var prov_options = jQuery(".admin_itibuilder_provsel").html();
    jQuery(".admin_itibuilder_provsel").eq(new_prov_index).html(prov_options);

    addPlace(new_prov_index);
}


//Delete a whole province
//prov_index assumes 0-based index
// User is not allowed to delete the province if it is the only one left
function deleteProv(prov_index) {

    if(jQuery(".admin_itibuilder_prov").length <= 1){
        alert("Delete not allowed. There must be at least 1 province.");
    }
    else {
        var result = window.confirm("The entire day and its contents will be deleted. Are you sure?");

        if(result == true) {
            jQuery(".admin_itibuilder_prov").eq( prov_index ).remove();
            
            assignNewPosIndexes();
            count_prov_places();
        }
    }
}

//Delete a single place from a specified province
//
function deletePlace(id_to_delete) {
    var result = window.confirm("This place will be deleted from itinerary. Are you sure?");

    if(result == true) {
        jQuery("#"+id_to_delete).remove();
        
        assignNewPosIndexes();
        count_prov_places();
    }
}