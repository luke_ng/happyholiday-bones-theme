<?php
require_once('../../../../wp-config.php');
require_once('../../../../wp-includes/wp-db.php');
require_once('run-mpdf-export.php');

/*  
$html = '<html>
<head> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<link rel="stylesheet" id="bones-stylesheet-css"
	href="'. $_SERVER['DOCUMENT_ROOT'] .'/wp-content/themes/hhp-bones/library/css/style.css" 
	type="text/css" media="all" />
<link rel="stylesheet" type="text/css" media="all" 
	href="'. $_SERVER['DOCUMENT_ROOT'] .'/wp-content/themes/hhp-bones/library/css/print.css"
	type="text/css" media="all" />
</head>';
*/
/*
Parameters received by do-export:

1. day_ids -- A list of "day_id" that are in the itinerary
   Structure: [ day_id#1, day_id#2, ... ]

2. swapped_places -- A list of places that were swapped by the user.
   Structure: {
	  day_id#1 : { orig_place#1 : swapped_place#1 , orig_place#2 : swapped_place#2 , ... },
	  day_id#2 : { orig_place#1 : swapped_place#1 , ... },
   }
*/

$jsonString = urldecode( $_GET["day_ids"] );
$jsonStringReplaced = str_replace("\\","",$jsonString);
$day_ids = json_decode($jsonStringReplaced,true);

$jsonString = urldecode( $_GET["swapped_places"] );
$jsonStringReplaced = str_replace("\\","",$jsonString);
$swapped_places = json_decode($jsonStringReplaced, true);



//Query the database for data about the days using the day_id
global $wpdb;
$query = "SELECT * FROM samp_iti_day WHERE day_id IN (".
	implode(",", $day_ids) . ") ORDER BY FIELD (day_id," .
	implode(",", $day_ids).")";
$db_day_data = $wpdb->get_results($query);

//Place the day_itinerary data in a separate array, arr_iti_data
// Structure of arr_iti_data: { [day_id#1] -> iti_data, [day_id#2] -> iti_data, ... }
// day_id is used as the array keys

$arr_iti_data = array();

foreach($db_day_data as $day_data){
	$curr_day_id = $day_data->day_id;
	$arr_iti_data[$curr_day_id] = json_decode($day_data->day_itinerary, true);
}

if(!empty($swapped_places)){
	//Do the replacements over day_ids
	foreach($swapped_places as $s_day_id=>$swap_entries){
		foreach($swap_entries as $orig_day_id=>$swap_day_id){
			$curr_iti_data = $arr_iti_data[$s_day_id];
			//find out which index the entry to swap is at
			$prov_i = -1;
			$place_i = -1;
			foreach($curr_iti_data as $prov_index=>$prov_data){
				foreach($prov_data['places'] as $place_index=>$place_data){
					if($place_data['id'] == $orig_day_id){
						$prov_i = $prov_index;
						$place_i = $place_index;
						break;
					}
				}
				//Breaks out from outer for loop
				if($prov_i >= 0){
					break;
				}
			}

			if($prov_i >= 0 && $place_i >= 0){
				//If the index to swap was found, do the swap here
				$arr_iti_data[$s_day_id][$prov_i]['places'][$place_i]['id'] = $swap_day_id;
				$arr_iti_data[$s_day_id][$prov_i]['places'][$place_i]['label'] = "";
			}
		}
	}
}

runMpdfExport($arr_iti_data);

exit;

//==============================================================
//==============================================================
?>