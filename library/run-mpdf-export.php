<?php
// function runMpdfExport($arr_iti_data)
// Generates the itinerary based on $arr_iti_data
// Structure of arr_iti_data: { [day_id#1] -> iti_data, [day_id#2] -> iti_data, ... }
// day_id is used as the array keys
#require('../../../../mpdf/mpdf.php');
#require_once('../itibuilder-commons.php');
require(get_theme_root().'/../../mpdf/mpdf.php');
require_once(get_theme_root().'/hhp-bones/itibuilder-commons.php');

//Function to estimate the number of lines the string would occupy
//in the half-container
//Content (excluding headers) can only have max 9 lines in the box
//If more than that, we need to bring the info over to the right side box
//Each line can have a max of 24 English chars. A chinese char can be estimated 
//to have a width of 1.5 - 2 chars (~14 char per line)
function compute_num_lines($in_str){
	//Count the number of new lines first, then see if each new line will form 
	//multiple lines in the view
	$order   = array("\r\n", "\n\r", "\n", "\r");
	$in_str_clean = str_replace($order, PHP_EOL, $in_str);
	$newlines = explode(PHP_EOL, $in_str_clean);

	$view_lines = 0;

	//For each new line, count the number of 'view-lines' (num of lines in display)
	foreach($newlines as $line){
		$view_lines += count_view_lines($line);
	}

	return $view_lines;
}

//Compute the number of lines the input string will occupy in the container
//$in_str must be a single line of text (without EOL)
function count_view_lines($line){
	
	if(!is_string($line) || empty($line)){
		//error_log("In count_view_lines: returning 0");
		return 1;
	}
	
	//If encoding is ASCII, means its all in English chars,
	//else, if there are chinese chars, it is usally UTF-8
	$line_enc = mb_detect_encoding($line, 'ASCII', true);
	if($line_enc == "ASCII"){
		//All are english chars
		$view_lines = ceil(strlen($line)/24.0);
	}
	else {
		//Assume all chinese characters
		$view_lines = ceil(mb_strlen($line)/14.0);
	}

	//error_log("In count_view_lines: returning view_lines = ". $view_lines);
	return $view_lines;
}

//Splits a line into 2 parts. The point to split is defined
//by split_at_line , e.g. split_at_line = 2, The string will be split
//after 2 lines 
function halfcontainer_splitline($line, $split_at_line){
	//If encoding is ASCII, means its all in English chars,
	//else, if there are chinese chars, it is usally UTF-8
	$line_enc = mb_detect_encoding($line, 'ASCII', true);
	if($line_enc == "ASCII"){
		$split_offset = 24 * $split_at_line;
	}
	else {
		$split_offset = 14 * $split_at_line;
	}

	return str_split($line, $split_offset);
}


function prettyprint_day_itinerary($day_iti){
	$ret_str = "";
	foreach($day_iti as $prov_data){
		foreach($prov_data['places'] as $place_data){
			$ret_str .= '<p>Prov' . $prov_data['province'] . ' - [' . $place_data['id'] . '] '.
				$place_data['label'] . '</p>';
		}
	}
	return $ret_str;
}



function show_sample_page_a(){
	$htmlcode = '<div class="print-page">
			<p class="page-day-display text-right">Day 1, Taipei</p>
			<div class="place-header-container rounded-box text-center">
				<!-- Jiǔ fèn -->
				<p class="place-title">九份</p>
				<p class="place-title">Jiu Fen</p>
			</div>
			<div class="print-feat-img">
				<img src="' . $_SERVER['DOCUMENT_ROOT'] . '/wp-content/uploads/2013/09/jiu-fen.jpg" 
					style="height: 5in; width:auto;"/>
			</div>

			<div class="half-container half-c-left rounded-box gray-gradient box-padding">
				<p>Address:</p>
				<p lang="zh-TW">台灣新北市瑞芳區九份老街</p>
				<br>
				<p>Operating hours:</p>
				<p>9 am ~ 10 pm</p>
			</div>
			<div class="half-container half-c-right rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod 
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
					veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea 
					commodo consequat.
				</p>
			</div>
		</div>';

	return $htmlcode;
}

function show_sample_page_b(){
	$htmlcode = '<div class="print-page">
			<p class="page-day-display text-right">Day 1, Taipei</p>
			<div class="place-header-container rounded-box text-center">
				<h1>九份 – Jiǔ fèn (Jiu Fen)</h1>
			</div>

			<div class="page-margin-div"></div>

			<div class="half-container half-c-left rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
			<div class="half-container half-c-right rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>

			<div style="clear: both"></div>
			<br><br>

			<div class="half-container half-c-left rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
			<div class="half-container half-c-right rounded-box gray-gradient box-padding">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
			</div>
		</div>';

	return $htmlcode;
}

function runMpdfExport($arr_iti_data){

	//Mapping table for todo-type default image
	//Possible todo-types are: 'Sightseeing', 'Shopping', 'Fun and relax', 
	//  'Eating', 'Accommodation', 'Useful Tip'
	$todo_type_default_img = array(
		'Sightseeing' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-sightsee.png',
		'Shopping' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-shopping.png',
		'Fun and relax' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-funrelax.png',
		'Eating' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-food.png',
		'Accommodation' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-accom-2.png',
		'Useful Tip' => get_theme_root().'/hhp-bones/library/images/pdfexport/exp-icon-tips.png',
		);

	$share_with_us_img = get_theme_root().'/hhp-bones/library/images/pdfexport/share-with-us.png';

	//Build the province map (for easy search of province name using id)
	 $gt_args = array(
	    'orderby' => 'id', 
	    'order'   => 'ASC',
	); 
	$terms = get_terms("Location", $gt_args);
	$province_map = array();
	foreach($terms as $term){
	    $province_map[intval($term->term_id)] = $term->name;
	}


	//Show a debug page
	$show_debug = false;
	if ($show_debug){
		$html .= '<div class="print-page">
					<p>arr_iti_data:</p>';

		$day_num = 1;
		foreach($arr_iti_data as $iti_data){
			$html .= '<p>Day '. $day_num . '</p>'. prettyprint_day_itinerary($iti_data);
			$day_num++;
		}
		$html .= '</div>';
	}


	//Show the cover page
	$html .= '<div class="print-page" 
				style="background: transparent url(\''.get_theme_root().'/hhp-bones/library/images/itibuilder/iti-cover-page.jpg\') no-repeat fixed center; background-size: cover;">
				<p id="cover-iti-date"></p>
				</div>';

	//Show the useful info page
	$html .= '<div class="print-page" 
				style="background: transparent url(\''.get_theme_root().'/hhp-bones/library/images/itibuilder/useful-info-page.jpg\') no-repeat fixed center; background-size: cover;">				
				<p>&nbsp;</p>
				</div>';
				
	//Show the place page-A
	//$html .= show_sample_page_a();
	//Show the place page-B
	//$html .= show_sample_page_b();


	$day_num = 1;
	foreach($arr_iti_data as $day_id=>$iti_data){

		foreach($iti_data as $prov_data){			
			foreach($prov_data['places'] as $place_data){
				$page_a_content = "";
				//First show the day number and province
				$currprov_name = $province_map[ intval($prov_data['province']) ];
				$page_a_content .= '<p class="page-day-display text-right">Day '. $day_num .', '. $currprov_name.'</p>';
				
				//Next we show the place title
				$place_name = get_the_title($place_data['id']);
				$name_parts = split_place_name($place_name);
				$page_a_content .= '<div class="place-header-container rounded-box">
					<p class="place-title">'. $name_parts[0] .'</p>
					<p class="place-title">'. $name_parts[2] .'</p>
					</div>';

				//Get the feature image
				$image_full = wp_get_attachment_url(
						get_post_thumbnail_id($place_data['id']));
				$page_a_content .= '<div class="print-feat-img">
					<img src="'. $image_full .'" style="height: 5in; width:auto;"/></div>';

				//Add the Left side info container
				$order   = array("\r\n", "\n\r", "\n", "\r");
	            $replace = '<br />';
	            // Processes \r\n's first so they aren't converted twice.
	            $b_address = get_post_meta( $place_data['id'], 'place-address-1', true );
	            $b_address = str_replace($order, $replace, $b_address);
	            
				$page_a_content .= '<div class="half-container half-c-left rounded-box gray-gradient box-padding">';
				
				//Each container can have max 9 lines of data
				$c1_lines_left = 9;
				//Add the Pin Yin 
				$page_a_content .= '<h3 style="margin-bottom: 0">Name (Pin Yin)</h3>
					<p class="text-full-justify">'.$name_parts[1].'</p>';
				$c1_lines_left -= compute_num_lines($name_parts[1]);

				//Add the address
				$page_a_content .= '<h3 style="margin-bottom: 0">Address (地址)</h3>
					<p class="text-full-justify">'.$b_address.'</p>';
				$c1_lines_left -= compute_num_lines($b_address);
	

				//Add the Auxillary data (opening hours/contact number)
				$b_auxdata = get_post_meta( $place_data['id'], 'place-operating-hours-1', true );
				if(!empty($b_auxdata)) {
	            	$page_a_content .= '<h3 style="margin-bottom: 0">Operating hours</h3>';
	        	}
	        	else {
	        		$b_auxdata = get_post_meta( $place_data['id'], 'place-phone-1', true );
	        		if(!empty($b_auxdata)) {
		            	$page_a_content .= '<h3 style="margin-bottom: 0">Contact number</h3>';
	        		}
	        	}

	        	//Check for overflow:
	        	//Content (excluding headers) can only have max 9 lines in the box
	        	//If more than that, we need to bring the info over to the right side box
	        	//Each line can have a max of 24 English chars. A chinese char can be estimated 
	        	//to have a width of 2 chars
	        	$has_overflow = false;
	        	$b_auxdata_html = '<p class="text-full-justify">';
	        	$b_overflowdata = '';

	        	if(!empty($b_auxdata)){
	        		$order   = array("\r\n", "\n\r", "\n", "\r");
					$b_auxdata = str_replace($order, PHP_EOL, $b_auxdata);
					$b_newlines = explode(PHP_EOL, $b_auxdata);
					
					foreach($b_newlines as $b_line){
						//Get the num of lines the current b_line will occupy in the view
						$b_numviewlines = count_view_lines($b_line);
						//error_log("count_view_lines returns: ".$b_numviewlines);
						if($c1_lines_left >= $b_numviewlines){
							$b_auxdata_html .= $b_line.'<br />';
							$c1_lines_left -= $b_numviewlines;
						}
						else if($c1_lines_left > 0 && $c1_lines_left < $b_numviewlines){
							//We need to split the overflow lines
							$split_line = halfcontainer_splitline($b_line, $c1_lines_left);
							$b_auxdata_html .= $split_line[0];
							$b_overflowdata .= $split_line[1].'<br />';
							$c1_lines_left = 0;
							$has_overflow = true;
						}
						else {
							//Place in the overflow
							$b_overflowdata .= $b_line.'<br />';
							$has_overflow = true;

						}
					}

					//Add the aux data
		        	$b_auxdata_html = rtrim($b_auxdata_html, '<br />');
		        	$b_auxdata_html .= '</p>';
		        	$page_a_content .= $b_auxdata_html;
	        	}
	        	
				//Close the left side container
				$page_a_content .= '</div>';
				

				//Add the Right side info container
				$num_todo = get_post_meta( $place_data['id'], 'place-num-todo', true );
				$todo_index = 1;

				$page_a_content .= '<div class="half-container half-c-right rounded-box gray-gradient box-padding">';

				if($has_overflow){
					//Put the overflow data on the right panel
					$page_a_content .= '<p class="text-full-justify">'.$b_overflowdata.'</p>';
				}
				else if(isset($num_todo) && $num_todo > 0){
					//No overflow, and there is a to-do item to display
					$todo_type = get_post_meta( $place_data['id'], 'place-todo-type-1', true );
					$todo_img_url = get_post_meta( $place_data['id'], 'place-todo-img-1', true );
					$todo_text = get_post_meta( $place_data['id'], 'place-todo-text-1', true );
					$order   = array("\r\n", "\n\r", "\n", "\r");
                    $replace = '<br />';
                    $todo_text = str_replace($order, $replace, $todo_text);

					//If there is no todo_img_url, we need to show the default pic based on the todo_type
					if(empty($todo_img_url)){
						if(isset($todo_type_default_img[$todo_type])){
							$todo_img_url = $todo_type_default_img[$todo_type];
						}
						else {
							$todo_img_url = "images/logo-gray-thumb.png";
						}
					}

					$page_a_content .= '<div class="half-container-img" style="background: transparent url(\''.$todo_img_url.'\') no-repeat fixed center;"></div>
						<p class="text-full-justify">'.$todo_text.'</p>';

					$todo_index++;
				}
				else {
					//No overflow and no "things to do" info, show the "Share with us" panel
					$page_a_content .= '<a href="https://www.facebook.com/happyholidayplanner/" target="_blank">
							<img class="half-container-img-full" src="'.$share_with_us_img.'" />
						</a>';
				}

				//Close the right side container
				$page_a_content .= '</div>';
				//Finally add the page to the html code
				$html .= '<div class="print-page">'. $page_a_content .'</div>';

				//Check if there are more 'Things to do' for this place
				while(isset($num_todo) && $num_todo >= $todo_index){
					$page_b_content = '<p class="page-day-display text-right">Day '. $day_num .', '. $currprov_name.'</p>';
					$page_b_content .= '<br>'; 
					$iter_num = 1;
					while($todo_index <= $num_todo){
						$todo_type = get_post_meta( $place_data['id'], 'place-todo-type-'.$todo_index, true );
						$todo_img_url = get_post_meta( $place_data['id'], 'place-todo-img-'.$todo_index, true );
						$todo_text = get_post_meta( $place_data['id'], 'place-todo-text-'.$todo_index, true );
						$order   = array("\r\n", "\n\r", "\n", "\r");
                    	$replace = '<br />';
                    	$todo_text = str_replace($order, $replace, $todo_text);

						//If iter_num is an odd number its on the left side (half-c-left) 
						// for even number its on the right side (half-c-right)
						if($iter_num%2 != 0){
							$page_b_content .= '<div class="half-container half-c-left rounded-box gray-gradient box-padding">';
						}
						else {
							$page_b_content .= '<div class="half-container half-c-right rounded-box gray-gradient box-padding">';	
						}
						

						//If there is no todo_img_url, we need to show the default pic based on the todo_type
						if(empty($todo_img_url)){
							if(isset($todo_type_default_img[$todo_type])){
								$todo_img_url = $todo_type_default_img[$todo_type];
							}
							else {
								$todo_img_url = "images/logo-gray-thumb.png";
							}
						}

						$page_b_content .= 	'<div class="half-container-img" style="background: transparent url(\''.$todo_img_url.'\') no-repeat fixed center;" />
							<p class="text-full-justify">'.$todo_text.'</p>
						</div>';
						
						//Print max 4 todos on the 2nd page
						$todo_index++;
						if($iter_num == 4){	
							break;
						}
						else {
							$iter_num++;
						}
						
					}
					//Finally add the page to the html code
					$html .= '<div class="print-page">'. $page_b_content .'</div>';
				}
			}
		}

		$day_num++;
	}

	//==============================================================
	//==============================================================
	//==============================================================

	
	$mpdf=new mPDF('-aCJK','A4'); //set 'useAdobeCJK' to false
	$mpdf->SetDisplayMode('fullwidth');
	//$mpdf->debugfonts = true;
	//$mpdf->autoLangToFont = true;

	// LOAD a stylesheet
	$stylesheet = file_get_contents(get_theme_root().'/hhp-bones/library/css/print.css');

	$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

	$mpdf->WriteHTML($html);
	$mpdf->Output();
	//$mpdf->Output('hhp_itinerary.pdf', 'D');
}

?>