<?php
/*
Template Name: Login Page Layout 
*/
?>

<?php get_header(); ?>

	<div id="content">

		<div id="inner-content" class="wrap clearfix">

			<div id="main" class="ninecol first clearfix" role="main">
				<div class="boxed">
					<div id="fb-root"></div>
					<script>
					  window.fbAsyncInit = function() {
					  FB.init({
					    appId      : '573561169383977',
					    status     : true, // check login status
					    cookie     : true, // enable cookies to allow the server to access the session
					    xfbml      : true  // parse XFBML
					  });

					  // Here we subscribe to the auth.authResponseChange JavaScript event. This event is fired
					  // for any authentication related change, such as login, logout or session refresh. This means that
					  // whenever someone who was previously logged out tries to log in again, the correct case below 
					  // will be handled. 
					  FB.Event.subscribe('auth.authResponseChange', function(response) {
					    // Here we specify what we do with the response anytime this event occurs. 
					    if (response.status === 'connected') {
					      // The response object is returned with a status field that lets the app know the current
					      // login status of the person. In this case, we're handling the situation where they 
					      // have logged in to the app.
					      testAPI();
					    } else if (response.status === 'not_authorized') {
					      // In this case, the person is logged into Facebook, but not into the app, so we call
					      // FB.login() to prompt them to do so. 
					      // In real-life usage, you wouldn't want to immediately prompt someone to login 
					      // like this, for two reasons:
					      // (1) JavaScript created popup windows are blocked by most browsers unless they 
					      // result from direct interaction from people using the app (such as a mouse click)
					      // (2) it is a bad experience to be continually prompted to login upon page load.
					      FB.login();
					    } else {
					      // In this case, the person is not logged into Facebook, so we call the login() 
					      // function to prompt them to do so. Note that at this stage there is no indication
					      // of whether they are logged into the app. If they aren't then they'll see the Login
					      // dialog right after they log in to Facebook. 
					      // The same caveats as above apply to the FB.login() call here.
					      FB.login();
					    }
					  });
					  };

					  // Load the SDK asynchronously
					  (function(d){
					   var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
					   if (d.getElementById(id)) {return;}
					   js = d.createElement('script'); js.id = id; js.async = true;
					   js.src = "//connect.facebook.net/en_US/all.js";
					   ref.parentNode.insertBefore(js, ref);
					  }(document));

					  // Here we run a very simple test of the Graph API after login is successful. 
					  // This testAPI() function is only called in those cases. 
					  function testAPI() {
					    console.log('Welcome!  Fetching your information.... ');
					    FB.api('/me', function(response) {
					      console.log('Good to see you, ' + response.name + '.');
					      console.log('Email, ' + response.email + '.');
					      console.log('Username, ' + response.username + '.');
					    });
					  }
					</script>

					<!--
					  Below we include the Login Button social plugin. This button uses the JavaScript SDK to
					  present a graphical Login button that triggers the FB.login() function when clicked. -->

					<fb:login-button show-faces="true" width="200" max-rows="1"></fb:login-button>
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
					<h1 class="blue-text"><?php the_title(); ?></h1>
					<section class="entry-content content-padding clearfix" itemprop="articleBody">
						<?php the_content(); ?>
					</section>

					<?php endwhile; else : ?>

					<article id="post-not-found" class="hentry clearfix">
							<header class="article-header">
								<h1><?php _e( 'Oops, Post Not Found!', 'bonestheme' ); ?></h1>
						</header>
							<section class="entry-content content-padding">
								<p><?php _e( 'Uh Oh. Something is missing. Try double checking things.', 'bonestheme' ); ?></p>
						</section>
						<footer class="article-footer">
								<p><?php _e( 'This is the error message in the page-custom.php template.', 'bonestheme' ); ?></p>
						</footer>
					</article>

					<?php endif; ?>
				</div>
			</div>

			<?php get_sidebar(); ?>

		</div>

	</div>


<?php get_footer(); ?>